<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Phinx\Seed\AbstractSeed;

class NinaObjectsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtures/NinaObjects.json'), true);
        $ninaObjects = $this->table('nina_objects');
        $ninaObjects->insert($data)->save();
    }
}
