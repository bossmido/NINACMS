<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Phinx\Migration\AbstractMigration;

class CreateNinaObjectsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('nina_objects');
        $table->addColumn('root_id', 'integer', ['signed' => false]);
        $table->addColumn('parent_id', 'integer', ['signed' => false, 'null' => true]);
        $table->addColumn('order', 'integer', ['signed' => false, 'null' => true]);
        $table->addColumn('category', 'string', ['length' => 40]);
        $table->addColumn('class', 'string', ['length' => 80, 'null' => true]);
        $table->addColumn('label', 'string', ['length' => 150, 'null' => true]);
        $table->addColumn('fields_values', 'text', ['null' => true]);
        $table->addColumn('styles', 'text', ['null' => true]);
        $table->addColumn('user_id', 'string', ['length' => 45]);
        $table->addColumn('group_id', 'string', ['length' => 45]);
        $table->addColumn('rights', 'string', ['length' => 3, 'default' => '740']);

        $table->create();
    }
}
