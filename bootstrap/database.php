<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();
$capsule->addConnection([
    'driver' => NINA_DB_TYPE,
    'host' => NINA_DB_HOST,
    'database' => NINA_DB_NAME . '_' . NINA_ENVIRONMENT,
    'username' => NINA_DB_USER,
    'password' => NINA_DB_PASSWORD,
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => NINA_DB_PREFIX,
]);

$capsule->setEventDispatcher($dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();
