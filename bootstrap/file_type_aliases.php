<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

$aliases = require_once NINA_ROOT_PATH . '/config/file_type_aliases.php';

foreach ($aliases as $alias => $values) {
    $aliasesFromShortcuts = [];

    foreach ($values as $valueIndex => $value) {
        // On a trouvé un symbole d'héritage
        if (strpos($value, '@') === 0) {
            $inheritanceAlias = str_replace('@', '', $value);
            $aliasesFromShortcuts = array_merge($aliasesFromShortcuts, $aliases[$inheritanceAlias]);

            // On supprime les symboles d'héritages du tableau d'extensions
            unset($aliases[$alias][$valueIndex]);
        }
    }

    $aliases[$alias] = array_merge($aliases[$alias], $aliasesFromShortcuts);
}

$container->make('config')->set('nina.file_type_aliases', $aliases);
