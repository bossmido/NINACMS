<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/illuminate/support/helpers.php';
require_once __DIR__ . '/../nina/helpers.php';
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/whoops.php';

date_default_timezone_set(NINA_TIMEZONE);

require_once __DIR__ . '/container.php';
require_once __DIR__ . '/services.php';
require_once __DIR__ . '/database.php';
require_once __DIR__ . '/router.php';
require_once __DIR__ . '/file_type_aliases.php';
