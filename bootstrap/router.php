<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Router;

// Création du routeur
$router = new Router($dispatcher, $container);
$container->instance('router', $router);

// Dans « /foo/{id}/bar », id doit correspondre à un entier
$router->pattern('id', '[0-9]+');
// Et sera automatiquement converti en entier
$router->bind('id', function ($v) {
    return (int)$v;
});

$container->singleton(Registrar::class, function () use ($router) {
    return $router;
});

// Inclusion des routes
$router->group([
    'namespace' => 'App\Controllers',
    'middleware' => SubstituteBindings::class,
    'prefix' => defined('NINA_BASE_WEB_PATH') ? NINA_BASE_WEB_PATH : '',
], function () use ($router) {
    require_once NINA_ROOT_PATH . '/config/routes.php';
});
