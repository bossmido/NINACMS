<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

// Récupération des services définis
$services = require_once NINA_ROOT_PATH . '/config/services.php';

foreach ($services['bind'] as $abstract => $concrete) {
    $container->bind($abstract, $concrete);
}

foreach ($services['singleton'] as $abstract => $concrete) {
    $container->singleton($abstract, $concrete);
}
