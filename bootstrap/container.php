<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;

// Création et configuration du container
$container = new Container();
$container->instance('container', $container);

$container['config'] = new Repository(require_once NINA_ROOT_PATH . '/config/container.php');
$container['config']->set('nina.validator', require_once NINA_ROOT_PATH . '/config/validator.php');

// Création du dispatcher
$dispatcher = new Dispatcher($container);
$container->instance('dispatcher', $dispatcher);

Container::setInstance($container);
