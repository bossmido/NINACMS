<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\PrettyPageHandler;

$whoops = new \Whoops\Run();
$isProduction = NINA_ENVIRONMENT === 'prod';

if (Whoops\Util\Misc::isAjaxRequest()) {
    $handler = new JsonResponseHandler();
    $handler->addTraceToOutput(! $isProduction);
    $handler->discoverPublicProperties(!$isProduction);
} elseif (Whoops\Util\Misc::isCommandLine()) {
    $handler = new PlainTextHandler();
    $handler->addTraceToOutput(! $isProduction);
} else {
    $handler = new PrettyPageHandler();

    if ($isProduction) {
        // TODO: Faire un Handler customisé pour Nina ?
        $handler = new PlainTextHandler();
        $handler->addTraceToOutput(false);
    }
}

$whoops->pushHandler($handler);
$whoops->register();
