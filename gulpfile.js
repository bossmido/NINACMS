/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

const gulp = require('gulp');
const runSequence = require('run-sequence');
const args = require("minimist")(process.argv.slice(2));
const gutil = require('gulp-util');
const rename = require('gulp-rename');
const debug = require('gulp-debug');

const sass = require('gulp-sass');
const cssnext = require("gulp-cssnext");
const csso = require("gulp-csso");

const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

const isProduction = process.env.NODE_ENV === 'production';

const paths = {
  components: {
    output: 'public/assets/components/',
    admin: {
      scss: 'app/Components/*/admin/styles/**/*.scss',
      js: 'app/Components/*/admin/scripts/**/*.js',
      images: 'app/Components/*/admin/images/**/*.*',
    },
    public: {
      scss: 'app/Components/*/styles/**/*.scss',
      js: 'app/Components/*/scripts/**/*.js',
      images: 'app/Components/*/images/**/*.*',
    },
  }
};

const handlers = {
  scss (input, output = paths.components.output) {
    return gulp.src(input)
      .pipe(debug())
      .pipe(sass().on('error', sass.logError))
      .pipe(cssnext({sourcemap: !isProduction}))
      .pipe(isProduction ? csso() : gutil.noop())
      .pipe(gulp.dest(output))
  },
  js (input, output = paths.components.output) {
    return gulp.src(input)
      .pipe(debug())
      .pipe(babel())
      .pipe(uglify())
      .on('error', function (e) {
        console.error(e);
        this.emit('end');
      })
      .pipe(gulp.dest(output))
  },
  images (input, output = paths.components.output) {
    return gulp.src(input)
      .pipe(debug())
      .pipe(gulp.dest(output))
  }
};

/*
 * Définition des tâches
 */

gulp.task('default', function (cb) {
  const taskName = args.task;

  if (taskName) {
    runSequence(
      Array.isArray(taskName) ? taskName : [taskName],
      cb
    );
  } else {
    runSequence(
      'components:admin',
      'components:public',
      cb
    )
  }
});

gulp.task('watch', function (cb) {
  gulp.watch(paths.components.admin.scss, ['components:admin:scss']);
  gulp.watch(paths.components.admin.js, ['components:admin:js']);
  gulp.watch(paths.components.admin.images, ['components:admin:images']);

  gulp.watch(paths.components.public.scss, ['components:public:scss']);
  gulp.watch(paths.components.public.js, ['components:public:js']);
  gulp.watch(paths.components.public.images, ['components:public:images']);
});

/*
 * Tâches admin
 */

gulp.task('components:admin', function (cb) {
  runSequence(
    'components:admin:scss',
    'components:admin:js',
    'components:admin:images',
    cb
  )
});

gulp.task('components:admin:scss', function () {
  return handlers.scss(paths.components.admin.scss);
});

gulp.task('components:admin:js', function () {
  return handlers.js(paths.components.admin.js);
});

gulp.task('components:admin:images', function () {
  return handlers.images(paths.components.admin.images);
});

/*
 * Tâches publiques
 */

gulp.task('components:public', function (cb) {
  runSequence(
    'components:public:scss',
    'components:public:js',
    'components:public:images',
    cb
  );
});

gulp.task('components:public:scss', function () {
  return handlers.scss(paths.components.admin.scss);
});

gulp.task('components:public:js', function () {
  return handlers.js(paths.components.public.js);
});

gulp.task('components:public:images', function () {
  return handlers.images(paths.components.public.images);
});
