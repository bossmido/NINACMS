<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App;

/**
 * Class ComponentGroup
 *
 * @package App
 */
class ComponentGroup
{
    const ESSENTIALS = 'essentials';
    const MISCELLANEOUS = 'miscellaneous';
    const DISPLAY = 'display';
    const DISPLAY_COLUMNS = self::DISPLAY . ':columns';
    const DISPLAY_COLUMNS_TEST = self::DISPLAY_COLUMNS . ':test';
    const ARTICLES = 'articles';

    public static function getBasicTree()
    {
        return [
            // self::ESSENTIALS,
            self::MISCELLANEOUS,
            self::DISPLAY => [
                self::DISPLAY_COLUMNS => [
                    self::DISPLAY_COLUMNS_TEST
                ],
            ],
            self::ARTICLES,
        ];
    }
}
