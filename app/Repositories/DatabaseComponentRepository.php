<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repositories;

use App\Category;
use App\Entities\DatabaseNinaObject;
use Nina\Context;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Exceptions\ComponentNotFoundException;

/**
 * Class DatabaseComponentRepository
 *
 * @package App\Repositories
 */
class DatabaseComponentRepository implements ComponentRepository
{
    /**
     * {@inheritdoc}
     */
    public function create($rootId, $parentId, $class)
    {
        /** @var Context $context */

        // $context = Container::getInstance()->make('context');

        return DatabaseNinaObject::create([
            'root_id' => $rootId,
            'parent_id' => $parentId,
            'order' => 0,
            'class' => $class,
            'label' => $class,
            'fields_values' => new \ArrayObject(),
            'category' => Category::COMPONENT,
            'user_id' => '1', // TODO: ne pas écrire cette valeur en dur
            'group_id' => 'test', // TODO: ne pas écrire cette valeur en dur
            'rights' => '740',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function save(NinaObject $ninaObject)
    {
        $ninaObject->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(NinaObject $ninaObject)
    {
        $ninaObject->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function replicate(NinaObject $ninaObject, array $except = null)
    {
        return $ninaObject->replicate($except);
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return DatabaseNinaObject
            ::where('category', Category::COMPONENT)
            ->where('id', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdOrFail($id)
    {
        $component = $this->findById($id);

        if (!$component) {
            throw new ComponentNotFoundException($id);
        }

        return $component;
    }

    /**
     * {@inheritdoc}
     */
    public function findNextSiblings(NinaObject $component, $order)
    {
        return DatabaseNinaObject
            ::where('category', Category::COMPONENT)
            ->where([
                ['parent_id', '=', $component->getParentId()],
                ['id', '<>', $component->getId()],
                ['order', '>=', $order],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection($rootId)
    {
        return DatabaseNinaObject
            ::where('category', Category::COMPONENT)
            ->where(
                function ($q) use ($rootId) {
                    return $q
                        ->where('id', $rootId)
                        ->orWhere('root_id', $rootId);
                }
            )
            ->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectChildren($parentId)
    {
        return DatabaseNinaObject
            ::where('category', Category::COMPONENT)
            ->where('parent_id', $parentId)
            ->get();
    }
}
