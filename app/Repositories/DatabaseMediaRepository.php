<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repositories;

use App\Category;
use App\Entities\DatabaseNinaObject;
use Nina\Context;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Exceptions\FileNotFoundException;

/**
 * Class DatabaseMediaRepository
 *
 * @package App\Repositories
 */
class DatabaseMediaRepository implements MediaRepository
{
    /**
     * {@inheritdoc}
     */
    public function initialize($libraryId, $parentId, $label)
    {
        /** @var Context $context */

        // $context = Container::getInstance()->make('context');

        return new DatabaseNinaObject(
            [
                'root_id' => $libraryId,
                'parent_id' => $parentId,
                'label' => $label,
                'fields_values' => ['__contained_by' => []],
                'category' => Category::MEDIA,
                'user_id' => '1', // TODO: ne pas écrire cette valeur en dur
                'group_id' => 'test', // TODO: ne pas écrire cette valeur en dur
                'rights' => '740',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function replicate(NinaObject $ninaObject, array $except = null)
    {
        return $ninaObject->replicate($except);
    }

    /**
     * {@inheritdoc}
     */
    public function save(NinaObject $ninaObject)
    {
        return $ninaObject->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(NinaObject $ninaObject)
    {
        return $ninaObject->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function deleteWhereParentId($parentId)
    {
        return DatabaseNinaObject
            ::where('category', Category::MEDIA)
            ->where('parent_id', $parentId)
            ->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return DatabaseNinaObject
            ::where('category', Category::MEDIA)
            ->where('id', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdOrFail($id)
    {
        $media = $this->findById($id);

        if (!$media) {
            throw new FileNotFoundException($id);
        }

        return $media;
    }

    /**
     * {@inheritdoc}
     */
    public function findByParentId($parentId)
    {
        return DatabaseNinaObject
            ::where('category', Category::MEDIA)
            ->where('parent_id', $parentId)
            ->get()
            ->all();
    }

    /**
     * {@inheritdoc}
     */
    public function findByParentIdAndLabel($parentId, $label)
    {
        return DatabaseNinaObject
            ::where('category', Category::MEDIA)
            ->where('parent_id', $parentId)
            ->whereRaw('BINARY label = ?', [$label])
            ->get()
            ->all();
    }
}
