<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repositories;

use App\Category;
use App\Entities\DatabaseNinaObject;
use Nina\Context;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\FolderNotFoundAtLibraryRootException;
use Nina\Exceptions\FolderNotFoundException;
use Nina\Exceptions\UploadFolderNotFoundException;

/**
 * Class DatabaseMediasContainerRepository
 *
 * @package App\Repositories
 */
class DatabaseMediasContainerRepository implements MediasContainerRepository
{
    /**
     * {@inheritdoc}
     */
    public function create($libraryId, $parentId, $label)
    {
        /** @var Context $context */

        //        $context = Container::getInstance()->make('context');

        return DatabaseNinaObject::create(
            [
                'root_id' => $libraryId,
                'parent_id' => $parentId,
                'label' => $label,
                'category' => Category::MEDIAS_CONTAINER,
                'user_id' => '1', // TODO: ne pas écrire cette valeur en dur
                'group_id' => 'test', // TODO: ne pas écrire cette valeur en dur
                'rights' => '740',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function replicate(NinaObject $ninaObject, array $expect = [])
    {
        return $ninaObject->replicate($expect);
    }

    /**
     * {@inheritdoc}
     */
    public function save(NinaObject $ninaObject)
    {
        return $ninaObject->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(NinaObject $ninaObject)
    {
        return $ninaObject->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return DatabaseNinaObject
            ::whereIn('category', [Category::MEDIAS_CONTAINER, Category::MEDIAS_CONTAINER_FOR_UPLOADS])
            ->where('id', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdOrFail($id)
    {
        $mediasContainer = $this->findById($id);

        if (!$mediasContainer) {
            throw new FolderNotFoundException($id);
        }

        return $mediasContainer;
    }

    /**
     * {@inheritdoc}
     */
    public function findByLibraryId($libraryId)
    {
        return DatabaseNinaObject
            ::whereIn('category', [Category::MEDIAS_CONTAINER, Category::MEDIAS_CONTAINER_FOR_UPLOADS])
            ->where('root_id', $libraryId)
            ->where('parent_id', $libraryId)
            ->get()
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByLibraryIdOrFail($libraryId)
    {
        $mediasContainer = $this->findByLibraryId($libraryId);

        if (!$mediasContainer) {
            throw new FolderNotFoundAtLibraryRootException();
        }

        return $mediasContainer;
    }

    /**
     * {@inheritdoc}
     */
    public function findForUploadsByRootId($rootId)
    {
        return DatabaseNinaObject
            ::where('category', Category::MEDIAS_CONTAINER_FOR_UPLOADS)
            ->where('root_id', $rootId)
            ->get()
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findForUploadsByRootIdOrFail($rootId)
    {
        $mediasContainerForUploads = $this->findForUploadsByRootId($rootId);

        if (!$mediasContainerForUploads) {
            throw new UploadFolderNotFoundException();
        }

        return $mediasContainerForUploads;
    }

    /**
     * {@inheritdoc}
     */
    public function findByParentId($parentId)
    {
        return DatabaseNinaObject
            ::whereIn('category', [Category::MEDIAS_CONTAINER, Category::MEDIAS_CONTAINER_FOR_UPLOADS])
            ->where('parent_id', $parentId)
            ->get()
            ->all();
    }

    /**
     * {@inheritdoc}
     */
    public function findByParentIdAndLabel($parentId, $label)
    {
        return DatabaseNinaObject
            ::whereIn('category', [Category::MEDIAS_CONTAINER, Category::MEDIAS_CONTAINER_FOR_UPLOADS])
            ->where('parent_id', $parentId)
            ->where('label', $label)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findParentsOf(NinaObject $mediasContainer)
    {
        /** @var NinaObject $mediasContainer */
        /** @var NinaObject[] $ancestors */
        $ancestors = [];

        while ($mediasContainer->getRootId() !== $mediasContainer->getParentId()) {
            $mediasContainer = $this->findByIdOrFail($mediasContainer->getParentId());
            $ancestors[] = $mediasContainer;
        }

        return $ancestors;
    }

    /**
     * {@inheritdoc}
     */
    public function findChildrenOf($mediasContainer)
    {
        return $this->findByParentId($mediasContainer->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function findAllChildrenOf($mediasContainer)
    {
        $children = $this->findChildrenOf($mediasContainer);

        foreach ($children as $child) {
            $newChildren = $this->findAllChildrenOf($child);

            if (count($newChildren) > 0) {
                $children = array_merge($children, $newChildren);
            }
        }

        return $children;
    }
}
