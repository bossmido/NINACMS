<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repositories;

use App\Category;
use App\Entities\DatabaseNinaObject;
use Nina\Contracts\Repositories\LibraryRepository;
use Nina\Exceptions\LibraryNotFoundException;

/**
 * Class DatabaseLibraryRepository
 *
 * @package App\Repositories
 */
class DatabaseLibraryRepository implements LibraryRepository
{
    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return DatabaseNinaObject
            ::where('class', 'Library')
            ->where('category', Category::COMPONENT)
            ->where('id', $id)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByRootId($rootId)
    {
        return DatabaseNinaObject
            ::where('class', 'Library')
            ->where('category', Category::COMPONENT)
            ->where('root_id', $rootId)
            ->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByRootIdOrFail($rootId)
    {
        $library = $this->findByRootId($rootId);

        if (!$library) {
            throw new LibraryNotFoundException($rootId);
        }

        return $library;
    }
}
