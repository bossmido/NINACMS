<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers;

use Illuminate\Container\Container;
use Illuminate\Routing\Controller as BaseController;
use Kocal\Validator\Validator;

/**
 * Class AppController
 * @package App\Controllers
 */
class AppController extends BaseController
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct()
    {
        $this->container = Container::getInstance()->make('container');
    }

    /**
     * @param       $data
     * @param       $rules
     * @param array $messages
     * @param array $customAttributes
     *
     * @return Validator
     */
    public function validate(array &$data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $data = autoCastArrayValuesRecursively($data);

        /** @var Validator $validator */
        $validator = $this->container->make('validator');
        $validator->setRules($rules);
        $validator->validate($data, $messages, $customAttributes);

        return $validator;
    }
}
