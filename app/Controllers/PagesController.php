<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\Request;
use Nina\Context;
use Nina\Exceptions\ValidationException;

/**
 * Class PagesController
 *
 * @package App\Controllers
 */
class PagesController extends AppController
{
    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var Context
     */
    private $context;

    public function __construct(UrlGenerator $urlGenerator, Context $context)
    {
        parent::__construct();
        $this->urlGenerator = $urlGenerator;
        $this->context = $context;
    }

    public function adminIndex(Request $request)
    {
        $q = autoCastArrayValuesRecursively($request->only('root_id'));
        $validator = $this->validate($q, ['root_id' => 'integer|required|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return renderPage(
            'admin/index',
            [
                'options' => [
                    'locale' => $this->context->getLocale(),
                    'root_id' => $q['root_id'],
                    'webroot_url' => $this->urlGenerator->to('/'),
                    'api_url' => $this->urlGenerator->to('/api'),
                    'assets_url' => $this->urlGenerator->to('/assets'),
                    'components_assets_url' => $this->urlGenerator->to('/assets/components'),
                    'nina_mediatheque_url' => $this->urlGenerator->to(NINA_MEDIATHEQUE_URL),
                    'maximum_file_upload_size' => getMaximumFileUploadSize(),
                ],
            ]
        );
    }
}
