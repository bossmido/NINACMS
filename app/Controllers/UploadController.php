<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers;

use Illuminate\Http\Request;
use Nina\Component\ComponentProvider;
use Nina\Contracts\Repositories\LibraryRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\ValidationException;
use Nina\Services\NinaUploaderService;

/**
 * Class UploadController
 *
 * @package App\Controllers
 */
class UploadController extends AppController
{
    /**
     * @var ComponentProvider
     */
    private $componentProvider;

    /**
     * @var LibraryRepository
     */
    private $libraryRepository;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var MediasContainerRepository
     */
    private $mediasContainerRepository;

    public function __construct(
        ComponentProvider $componentProvider,
        LibraryRepository $libraryRepository,
        MediaRepository $mediaRepository,
        MediasContainerRepository $mediasContainerRepository
    )
    {
        $this->componentProvider = $componentProvider;
        $this->libraryRepository = $libraryRepository;
        $this->mediaRepository = $mediaRepository;
        $this->mediasContainerRepository = $mediasContainerRepository;
        parent::__construct();
    }

    public function file(Request $request)
    {
        // Validation
        $q = $request->only(['root_id', 'destination_id', 'file']);
        $validator = $this->validate(
            $q,
            [
                'root_id' => 'required|integer|min:1',
                'destination_id' => 'required|integer|min:1',
                'file' => 'required|file|max:' . (getMaximumFileUploadSize() / 1024),
            ]
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // On récupère le medias container de destination
        $destination = $this->mediasContainerRepository->findByIdOrFail($q['destination_id']);

        // On gère l'upload
        $uploader = NinaUploaderService::open($q['file']);
        $uploader->upload($destination);

        return json($uploader->state());
    }

    public function fileForComponent(Request $request)
    {
        // Validation
        $q = $request->only(['root_id', 'componentClass', 'componentId', 'fieldId', 'file']);
        $validator = $this->validate(
            $q,
            [
                'root_id' => 'required|integer|min:1',
                'componentClass' => 'required',
                'componentId' => 'required|integer|min:1',
                'fieldId' => 'required',
            ]
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // On récupère les règles de validation du fichier
        $componentInstance = $this->componentProvider->loadFromDatabase($q['componentId']);
        $rules = get($componentInstance->getFieldsFileRules()[$q['fieldId']]);

        // On récupère le dossier des envois
        $library = $this->libraryRepository->findByRootId($q['root_id']);
        $destination = $this->mediasContainerRepository->findForUploadsByRootIdOrFail($library->getId());

        // On gère l'upload
        $uploader = NinaUploaderService::open($q['file']);
        $uploader->validate($rules);
        $uploader->upload($destination);

        return json($uploader->state());
    }
}
