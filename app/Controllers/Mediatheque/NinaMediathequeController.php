<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers\Mediatheque;

use App\Controllers\AppController;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Http\Request;
use Nina\Contracts\Controllers\MediathequeController;
use Nina\Contracts\Repositories\LibraryRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\ValidationException;
use Nina\Services\NinaMediathequeService;
use Nina\Traits\MediathequeTrait;

/**
 * Class NinaMediathequeController
 *
 * @package App\Controllers\Mediatheque
 */
class NinaMediathequeController extends AppController implements MediathequeController
{
    use MediathequeTrait;

    /**
     * @var string
     */
    private $name = 'nina';

    /**
     * @var string
     */
    private $label = 'NinaCMS';

    /**
     * @var bool
     */
    private $isEnabled = true;

    /**
     * @var LibraryRepository
     */
    private $libraryRepository;

    /**
     * @var MediasContainerRepository
     */
    private $mediasContainerRepository;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var NinaMediathequeService
     */
    private $ninaMediatheque;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $files;

    public function __construct(
        LibraryRepository $libraryRepository,
        MediasContainerRepository $mediasContainerRepository,
        MediaRepository $mediaRepository,
        NinaMediathequeService $ninaMediatheque
    )
    {
        parent::__construct();
        $this->libraryRepository = $libraryRepository;
        $this->mediasContainerRepository = $mediasContainerRepository;
        $this->mediaRepository = $mediaRepository;
        $this->ninaMediatheque = $ninaMediatheque;
        $this->filesystem = $this->container->make(FilesystemFactory::class)->disk('mediatheque_nina');
        $this->files = $this->container->make('files');
    }

    /**
     * {@inheritdoc}
     */
    public function createMediasContainer(Request $request)
    {
        $q = $request->only(['library_id', 'parent_id', 'name']);

        $validator = $this->validate($q, [
            'library_id' => 'required|integer|min:1',
            'parent_id' => 'required|integer|min:1',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->ninaMediatheque->createMediasContainer($q['parent_id'], $q['name']);

        return json([
            'medias_container' => extractNinaObjectData($mediasContainer),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function renameMediasContainer(Request $request)
    {
        $q = $request->only(['id', 'newName']);

        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'newName' => 'required',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['id']);
        $this->ninaMediatheque->renameMediasContainer($mediasContainer, $q['newName']);

        return json([
            'medias_container' => extractNinaObjectData($mediasContainer),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function readMediasContainer(Request $request)
    {
        $q = $request->only(['id', 'library_id']);

        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'library_id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['id']);
        list($mediasContainers, $medias) = $this->ninaMediatheque->readMediasContainer($mediasContainer);

        return json([
            'current_medias_container' => extractNinaObjectData($mediasContainer),
            'medias_containers' => array_map('extractNinaObjectData', $mediasContainers),
            'medias' => array_map('extractNinaObjectData', $medias),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function copyMediaContainers(Request $request)
    {
        $q = $request->only(['id', 'destination_id']);

        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'destination_id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['id']);
        $mediasContainerDestination = $this->mediasContainerRepository->findByIdOrFail($q['destination_id']);

        $mediasContainer = $this->ninaMediatheque->copyMediaContainers($mediasContainer, $mediasContainerDestination);

        return json([
            'medias_container' => extractNinaObjectData($mediasContainer),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function moveMediaContainers(Request $request)
    {
        $q = $request->only(['id', 'destination_id']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'destination_id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['id']);
        $mediasContainerDestination = $this->mediasContainerRepository->findByIdOrFail($q['destination_id']);

        $mediasContainer = $this->ninaMediatheque->moveMediaContainers($mediasContainer, $mediasContainerDestination);

        return json([
            'medias_container' => extractNinaObjectData($mediasContainer),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMediasContainer(Request $request)
    {
        $q = $request->only(['library_id', 'id']);
        $validator = $this->validate($q, [
            'library_id' => 'required|integer|min:1',
            'id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['id']);
        $deletedIds = $this->ninaMediatheque->deleteMediasContainer($mediasContainer);

        return json([
            'deleted_medias_ids' => $deletedIds['deleted_medias_ids'],
            'deleted_medias_containers_ids' => $deletedIds['deleted_medias_containers_ids'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function renameMedia(Request $request)
    {
        $q = $request->only(['id', 'newName']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'newName' => 'required',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $media = $this->mediaRepository->findByIdOrFail($q['id']);
        $media = $this->ninaMediatheque->renameMedia($media, $q['newName']);

        return json([
            'media' => extractNinaObjectData($media),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function copyMedia(Request $request)
    {
        $q = $request->only(['id', 'destination_id']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'destination_id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $media = $this->mediaRepository->findByIdOrFail($q['id']);
        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['destination_id']);
        $this->ninaMediatheque->copyMedia($media, $mediasContainer);

        return json([
            'media' => extractNinaObjectData($media),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function moveMedia(Request $request)
    {
        $q = $request->only(['id', 'destination_id']);
        $validator = $this->validate($q, [
                'id' => 'required|integer|min:1',
                'destination_id' => 'required|integer|min:1',
            ]
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $media = $this->mediaRepository->findByIdOrFail($q['id']);
        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($q['destination_id']);
        $this->ninaMediatheque->moveMedia($media, $mediasContainer);

        return json([
            'media' => extractNinaObjectData($media),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMedia(Request $request)
    {
        $q = $request->only(['id']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $media = $this->mediaRepository->findByIdOrFail($q['id']);
        $this->ninaMediatheque->deleteMedia($media);

        return json();
    }

    /**
     * {@inheritdoc}
     */
    public function cropImage(Request $request)
    {
        $q = $request->only('image_id', 'crop');
        $this->validate(
            $q,
            [
                'image_id' => 'required|integer|min:1',
                'crop' => 'required|array',
            ]
        );

        $image = $this->mediaRepository->findByIdOrFail($q['image_id']);
        $images = $this->ninaMediatheque->cropImage($image, $q['crop']);

        return json(
            [
                'mother_image' => extractNinaObjectData($images['mother_image']),
                'daughter_image' => extractNinaObjectData($images['daughter_image']),
            ]
        );
    }

    /**
     * @param array $properties
     *
     * @return array
     * @throws \Exception
     */
    public function getAdditionalProperties(array $properties)
    {
        /** @var Request $request */
        $request = $this->container->make(Request::class);

        // On check le root_id
        $q = $request->only('root_id');
        $validator = $this->validate($q, ['root_id' => 'required|integer|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // On récupère la Library
        $library = $this->libraryRepository->findByRootId($q['root_id']);

        if (!$library) {
            throw new \Exception("Impossible de récupérer la librairie associée à l'application.");
        }

        // On récupère le premier mediaContainers de la Library
        $medias_container = $this->mediasContainerRepository->findByLibraryIdOrFail($library->getId());

        $properties['explorer']['root_id'] = $medias_container->getId();
        $properties['explorer']['current_id'] = $medias_container->getId();

        return $properties;
    }
}
