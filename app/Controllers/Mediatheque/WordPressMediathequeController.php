<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers\Mediatheque;

use App\Controllers\AppController;
use Illuminate\Http\Request;
use Nina\Contracts\Controllers\MediathequeController;
use Nina\Traits\MediathequeTrait;

/**
 * Class WordPressMediathequeController
 *
 * @package App\Controllers\Mediatheque
 */
class WordPressMediathequeController extends AppController implements MediathequeController
{
    use MediathequeTrait;

    /**
     * @var string
     */
    private $name = 'wordpress';

    /**
     * @var string
     */
    private $label = 'WordPress';

    /**
     * @var bool
     */
    private $isEnabled = true;

    /**
     * {@inheritdoc}
     */
    public function createMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function renameMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function readMediasContainer(Request $request)
    {
        return json([
            'scope' => 'wordpress',
            'current_medias_container' => new \ArrayObject(),
            'medias_containers' => [],
            'medias' => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function copyMediaContainers(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function moveMediaContainers(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function renameMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function copyMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function moveMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function cropImage(Request $request)
    {
    }
}
