<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers\Mediatheque;

use App\Controllers\AppController;
use Illuminate\Http\Request;
use Nina\Contracts\Controllers\MediathequeController;
use Nina\Traits\MediathequeTrait;

/**
 * Class JoomlaMediathequeController
 *
 * @package App\Controllers\Mediatheque
 */
class JoomlaMediathequeController extends AppController implements MediathequeController
{
    use MediathequeTrait;

    /**
     * @var string
     */
    private $name = 'joomla';

    /**
     * @var string
     */
    private $label = 'Joomla';

    /**
     * @var bool
     */
    private $isEnabled = false;

    /**
     * {@inheritdoc}
     */
    public function createMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function renameMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function readMediasContainer(Request $request)
    {
        return json([
            'scope' => 'joomla',
            'medias_containers' => [],
            'medias' => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function copyMediaContainers(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function moveMediaContainers(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMediasContainer(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function renameMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function copyMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function moveMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMedia(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function cropImage(Request $request)
    {
    }
}
