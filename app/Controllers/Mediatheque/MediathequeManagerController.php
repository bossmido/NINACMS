<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers\Mediatheque;

use App\Controllers\AppController;
use Nina\Contracts\Controllers\MediathequeController;

/**
 * Class MediathequeController
 *
 * @package App\Controllers\Mediatheque
 */
class MediathequeManagerController extends AppController
{
    /**
     * @var array
     */
    private $mediathequeControllers = [
        NinaMediathequeController::class,
        JoomlaMediathequeController::class,
        WordPressMediathequeController::class,
    ];

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $mediatheques = [];

        foreach ($this->mediathequeControllers as $mediathequeController) {
            /** @var MediathequeController $mediathequeController */
            $mediathequeController = $this->container->make($mediathequeController);

            if ($mediathequeController->isEnabled()) {
                $mediatheque = [
                    'name' => $mediathequeController->getName(),
                    'label' => $mediathequeController->getLabel(),
                    'bus' => null,
                    'explorer' => [
                        'root_id' => null,
                        'current_id' => null,
                        'current_medias_container' => new \ArrayObject(),
                        'prev_ids' => [],
                        'next_ids' => [],
                        'files' => [
                            'medias' => [], // peut être filtré
                            'medias_containers' => [], // peut être filtré
                            'original_medias' => [],
                            'original_medias_containers' => [],
                        ],
                        'filters' => [],
                        'medias_selections' => new \ArrayObject(),
                        'medias_selection' => null,
                    ],
                    'clipboard' => [
                        'empty' => true,
                        'action' => null, // 'copy' ou 'cut'
                        'file' => new \ArrayObject(),
                        'file_type' => null, // 'media' ou 'medias_container'
                    ],
                ];

                $mediatheques[] = $mediathequeController->getAdditionalProperties($mediatheque);
            }
        }

        return json([
            'mediatheques' => $mediatheques,
        ]);
    }
}
