<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Nina\Component\ComponentProvider;
use Nina\Context;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Exceptions\ValidationException;
use Nina\Services\ComponentService;

/**
 * Class ComponentsController
 *
 * @package App\Controllers
 */
class ComponentsController extends AppController
{
    /**
     * @var ComponentService
     */
    private $componentService;

    /**
     * @var ComponentProvider
     */
    private $componentProvider;

    /**
     * @var ComponentRepository
     */
    private $componentRepository;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    public function __construct()
    {
        parent::__construct();

        $this->componentService = $this->container->make('component_service');
        $this->componentProvider = $this->container->make('component_provider');
        $this->componentRepository = $this->container->make('component_repository');
        $this->mediaRepository = $this->container->make('medias_repository');
    }

    /**
     * @param $root_id
     *
     * @return string
     */
    public function renderCollection($root_id)
    {
        $this->container->make('context')->disableEditMode();

        return $this->componentService->renderCollection($root_id);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getCollection(Request $request)
    {
        $q = $request->only('root_id');
        $validator = $this->validate($q, ['root_id' => 'integer|required|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $collection = $this->componentService->getCollection($q['root_id']);

        return json($collection);
    }

    /**
     * @return JsonResponse
     */
    public function getAllMetadata()
    {
        return json([
            'metadata' => $this->componentService->getAllComponentsMetadata(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function renderEditionForm(Request $request)
    {
        $q = $request->only('id');
        $validator = $this->validate($q, ['id' => 'integer|required|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->componentService->renderEditionForm($q['id']);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function render(Request $request)
    {
        $q = $request->only('id');
        $validator = $this->validate($q, ['id' => 'integer|required|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->componentService->render($q['id'], ['tidy' => false]);
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $q = $request->only('data');
        $validator = $this->validate($q, [
            'data.parent_id' => 'required|integer|min:1',
            'data.class' => 'required|string|nina_component_exists',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentService->create($q['data']);

        return [
            'html' => $component->render(['tidy' => false]),
            'component' => extractNinaObjectData($component->getModel()),
        ];
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function saveStyles(Request $request)
    {
        $q = $request->only(['data']);
        $validator = $this->validate($q, [
            'data' => 'required|array',
            'data.*.id' => 'required|integer|min:1|distinct',
            'data.*.styles' => 'required|array',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->componentService->saveStyles($q['data']);

        return json();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function saveFieldsValues(Request $request)
    {
        $q = $request->only(['id', 'fieldsValues', 'mediasToUnlink']);
        $validator = $this->validate($q, [
            'id' => 'integer|required|min:1',
            'fieldsValues' => 'array',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentProvider->loadFromDatabase($q['id']);
        $this->componentService->saveFieldsValues($component, $q['fieldsValues'], get($q['mediasToUnlink'], []));
        $component->loadChildrenRecursively();

        return json(['html' => $component->render(['tidy' => false])]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function move(Request $request)
    {
        $q = $request->only(['id', 'parent_id', 'order']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'parent_id' => 'required|integer|min:1',
            'order' => 'required|integer|min:0',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentProvider->loadFromDatabase($q['id']);
        $this->componentService->move($component, $q['parent_id'], $q['order']);

        return json();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function duplicate(Request $request)
    {
        $q = $request->only(['id', 'root_id', 'parent_id', 'order']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'root_id' => 'required|integer|min:1',
            'parent_id' => 'required|integer|min:1',
            'order' => 'required|integer|min:0',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentProvider->loadFromDatabase($q['id']);
        $component = $this->componentService->duplicate($component, $q['root_id'], $q['parent_id'], $q['order']);

        return json(['html' => $component->render(['tidy' => false])]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function delete(Request $request)
    {
        $q = $request->only(['id']);
        $validator = $this->validate($q, ['id' => 'required|integer|min:1',]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentProvider->loadFromDatabase($q['id']);
        $this->componentService->delete($component);

        return json();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    private function paste(Request $request)
    {
        $q = $request->only(['id', 'parent_id', 'after_id', 'before_id']);
        $validator = $this->validate($q, [
            'id' => 'required|integer|min:1',
            'parent_id' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component = $this->componentProvider->loadFromDatabase($q['id']);
        $component = $this->componentService->paste($component, $q['parent_id']);
        $component->afterConstruct();

        return json(['html' => $component->render(['tidy' => false])]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function pasteBefore(Request $request)
    {
        // Poudre de perlimpinpin
        return $this->paste($request);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function pasteAfter(Request $request)
    {
        // Poudre de perlimpinpin
        return $this->paste($request);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changeOrders(Request $request)
    {
        $q = $request->only(['data']);
        $validator = $this->validate($q, [
            'data' => 'required|array',
            'data.*.id' => 'required|integer|min:1|distinct',
            'data.*.order' => 'required|integer|min:0|distinct',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        foreach ($q['data'] as $data) {
            $component = $this->componentProvider->loadFromDatabase($data['id']);
            $this->componentService->changeOrder($component, $data['order']);
        }

        return json();
    }
}
