<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controllers;

use App\ComponentGroup;
use App\Components\Library\LibraryComponent;
use Exception;
use Illuminate\Http\Request;
use Nina\Contracts\Repositories\LibraryRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\ValidationException;
use Nina\Services\ComponentsGroupsTreeBuilder;

/**
 * Class LibraryController
 *
 * @package App\Controllers
 */
class LibraryController extends AppController
{
    /** @var LibraryRepository */
    private $libraryRepository;

    /** @var MediasContainerRepository */
    private $mediasContainerRepository;

    public function __construct(
        LibraryRepository $libraryRepository,
        MediasContainerRepository $mediasContainerRepository
    ) {
        parent::__construct();
        $this->libraryRepository = $libraryRepository;
        $this->mediasContainerRepository = $mediasContainerRepository;
    }

    /**
     * @param $library_id
     *
     * @return LibraryComponent
     * @throws \Exception
     */
    private function getLibraryComponent($library_id)
    {
        $library = $this->libraryRepository->findById($library_id);

        if (!$library) {
            throw new \Exception("L'id $library_id n'est pas celui d'un composant Library.");
        }

        return new LibraryComponent($library);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function getLibraryId(Request $request)
    {
        // On check si $q['root_id'] est valide
        $q = $request->only('root_id');
        $validator = $this->validate($q, ['root_id' => 'required|integer|min:1']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // On récupère la Library
        $library = $this->libraryRepository->findByRootId($q['root_id']);

        if (!$library) {
            throw new Exception("Impossible de récupérer la librairie associée à l'application.");
        }

        return json([
            'library_id' => $library->getId(),
        ]);
    }

    /**
     * @param int $library_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComponents($library_id)
    {
        $libraryComponent = $this->getLibraryComponent($library_id);

        return json([
            'json' => $libraryComponent->getComponentsProperties(),
        ]);
    }

    public function getComponentsGroupsTree()
    {
        /** @var ComponentsGroupsTreeBuilder $componentsGroupsTreeBuilder */
        $componentsGroupsTreeBuilder = $this->container->make(ComponentsGroupsTreeBuilder::class);

        return $componentsGroupsTreeBuilder->build();
    }
}
