<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use App\Category;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Nina\Component\BaseComponent;
use Nina\Component\ComponentReflector;
use Nina\Context;
use Nina\Contracts\Entities\NinaObject;

/**
 * Class DatabaseNinaObject
 *
 * @package App\Entities
 * @property int    id
 * @property int    root_id
 * @property int    parent_id
 * @property int    order
 * @property string category
 * @property string label
 * @property string class
 * @property array  fields_values
 * @property array  styles
 * @property string $rights
 * @property string $user_id
 * @property string $group_id
 */
class DatabaseNinaObject extends Model implements NinaObject
{
    protected $table = 'objects';

    public $timestamps = false;

    protected $casts = [
        'id' => 'int',
        'root_id' => 'int',
        'parent_id' => 'int',
        'order' => 'int',
        'fields_values' => 'array',
        'styles' => 'array',
    ];

    protected $fillable = [
        'root_id',
        'parent_id',
        'category',
        'class',
        'label',
        'fields_values',
        'user_id',
        'group_id',
        'rights',
    ];

    /**
     * @var BaseComponent
     */
    private $component = null;

    /**
     * {@inheritdoc}
     */
    public function detachMediaOfComponent(NinaObject $media, $fieldName)
    {
        assert($this->getCategory() === Category::COMPONENT);
        assert($media->getCategory() === Category::MEDIA);

        $fieldsValues = $this->getFieldsValues();

        foreach (get($fieldsValues['__reflection']['fields'], []) as $fieldId => $field) {
            if (!fieldCanContainsMedias($field)) {
                continue;
            }

            $field = get($fieldsValues[$fieldId], null);

            if (!$field) {
                continue;
            }

            $fieldsValues[$fieldId] = array_diff($fieldsValues[$fieldId], [$media->getId()]);
        }

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function attachComponentToMedia(NinaObject $component, $fieldName)
    {
        assert($this->getCategory() === Category::MEDIA);
        assert($component->getCategory() === Category::COMPONENT);

        $fieldsValues = $this->getFieldsValues();

        $containedBy = get($fieldsValues['__contained_by'][$component->getId()], []);
        $containedBy[] = $fieldName;
        $fieldsValues['__contained_by'][$component->getId()] = array_unique($containedBy);

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function detachComponentOfMedia(NinaObject $component, $fieldName)
    {
        assert($this->getCategory() === Category::MEDIA);
        assert($component->getCategory() === Category::COMPONENT);

        $fieldsValues = $this->getFieldsValues();

        $fields = get($fieldsValues['__contained_by'][$component->getId()], []);
        $fieldsValues['__contained_by'][$component->getId()] = array_diff($fields, [$fieldName]);

        if (count($fieldsValues['__contained_by'][$component->getId()]) === 0) {
            unset($fieldsValues['__contained_by'][$component->getId()]);
        }

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function cleanImage()
    {
        $fieldsValues = $this->getFieldsValues();

        if (isset($fieldsValues['__daughters_images_id'])) {
            unset($fieldsValues['__daughters_images_id']);
        }

        if (isset($fieldsValues['__contained_by'])) {
            unset($fieldsValues['__contained_by']);
        }

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function attachDaughterImageToMotherImage(NinaObject $daughterImage)
    {
        assert($this->isMotherImage());

        $fieldsValues = $this->getFieldsValues();
        $daughtersImagesId = get($fieldsValues['__daughters_images_id'], []);
        $daughtersImagesId[] = $daughterImage->getId();
        $daughtersImagesId = array_unique($daughtersImagesId);
        $fieldsValues['__daughters_images_id'] = $daughtersImagesId;

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function detachDaughterImageFromMotherImage(NinaObject $daughterImage)
    {
        assert($this->isMotherImage());

        $fieldsValues = $this->getFieldsValues();
        $daughtersImagesId = $fieldsValues['__daughters_images_id'];
        $daughtersImagesId = array_diff($daughtersImagesId, [$daughterImage->getId()]);
        $fieldsValues['__daughters_images_id'] = $daughtersImagesId;

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function attachMotherImageToDaughterImage(NinaObject $motherImage)
    {
        assert($motherImage->isMotherImage());

        $fieldsValues = $this->getFieldsValues();
        $fieldsValues['__mother_image_id'] = $motherImage->getId();

        $this->setFieldsValues($fieldsValues);
    }

    /**
     * {@inheritdoc}
     */
    public function isImage()
    {
        return $this->isMotherImage() || $this->isDaughterImage();
    }

    /**
     * {@inheritdoc}
     */
    public function isMotherImage()
    {
        $fieldsValues = $this->getFieldsValues();

        return !isset($fieldsValues['__mother_image_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function isDaughterImage()
    {
        return !$this->isMotherImage();
    }

    /**
     * {@inheritdoc}
     */
    public function isContainedNowhere()
    {
        $fieldsValues = $this->getFieldsValues();

        return empty($fieldsValues['__contained_by']);
    }

    /**
     * {@inheritdoc}
     */
    public function setComponent(BaseComponent $baseComponent)
    {
        return $this->component = $baseComponent;
    }

    /**
     * {@inheritdoc}
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getRootId()
    {
        return $this->root_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setRootId($root_id)
    {
        $this->root_id = $root_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * {@inheritdoc}
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritdoc}
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * {@inheritdoc}
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * {@inheritdoc}
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldsValues()
    {
        return $this->fields_values;
    }

    /**
     * {@inheritdoc}
     */
    public function setFieldsValues($fieldsValues)
    {
        if ($this->component instanceof BaseComponent) {
            $fieldsValues['__reflection'] = ComponentReflector::reflect($this->component);
        }

        $this->fields_values = $fieldsValues;
    }

    /**
     * {@inheritdoc}
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * {@inheritdoc}
     */
    public function setStyles($styles)
    {
        $this->styles = $styles;
    }

    /**
     * {@inheritdoc}
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * {@inheritdoc}
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * Override la méthode newQuery, en appliquant le filtre de permissions pour chaque requête.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery()
    {
        $query = parent::newQuery();

        $query->where(function ($query) {
            /** @var Context $context */
            $context = Container::getInstance()->make('context');

            $query
                ->where('user_id', $context->getUser())
                ->orWhere(function ($query) use ($context) {
                    $query
                        ->whereIn('group_id', $context->getGroups())
                        ->where(new Expression('SUBSTRING(rights, 2, 1)'), '>=', 4)
                        ->orWhere(new Expression('SUBSTRING(rights, 3, 1)'), '>=', 4);
                });
        });

        return $query;
    }
}
