<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Article2;

use App\ComponentGroup;
use Nina\Component\BaseComponent;
use Nina\Field\FieldDate;
use Nina\Field\FieldForComponentProperties;
use Nina\Field\FieldImage;
use Nina\Field\FieldRichTextTinyMce;
use Nina\Field\FieldText;

/**
 * Class Article2Component
 *
 * @package App\Components\Article2
 */
class Article2Component extends BaseComponent
{
    protected $group = ComponentGroup::ARTICLES;

    protected $contains = [];

    protected $name = 'Article';

    protected $icon = 'article.png';

    protected $fieldsRules = [
        'title' => 'required',
        'chapeau' => 'required',
        'content' => 'required',
        'created_at' => 'required|date',
    ];

    public function afterConstruct()
    {
        $this->fields = [
            'principal' => [
                'name' => 'Principal',
                'fields' => [
                    (new FieldText('title', 'Titre'))
                        ->addTitleTagProperty()
                        ->addCssClassesProperty()
                        ->addVisibilityProperties(),
                    (new FieldText('chapeau', 'Chapeau'))->addCommonProperties(),
                    (new FieldRichTextTinyMce('content', 'Le contenu'))->addCommonProperties(),
                    (new FieldDate('created_at', 'Créé le'))->addCommonProperties(),
                ],
            ],
            'illustration_image' => [
                'name' => "Image d'illustration",
                'fields' => [
                    (new FieldImage('illustration_image', '', ['max' => 1]))
                        ->addCssClassesProperty()
                        ->addVisibilityProperties(),
                ],
            ],
            'infinite_images' => [
                'name' => "Une infinité d'images",
                'fields' => [
                    (new FieldImage('other_images'))
                        ->addVisibilityProperties(),
                ],
            ],
            'component_properties' => [
                'name' => 'Propriétés du composant',
                'fields' => [
                    new FieldForComponentProperties(),
                ],
            ],
        ];
    }
}
