<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Page;

use App\ComponentGroup;
use Nina\Component\BaseComponent;

class PageComponent extends BaseComponent
{
    protected $group = ComponentGroup::ESSENTIALS;

    protected $name = 'Page';

    protected $contains = ['Columns'];

    protected $adminStyles = ['component.css'];
}
