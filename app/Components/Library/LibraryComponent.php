<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Library;

use Nina\Component\BaseComponent;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;

class LibraryComponent extends BaseComponent
{
    /** @var MediaRepository */
    private $mediaRepository;

    /** @var MediasContainerRepository */
    private $mediaContainersRepository;

    public function __construct(NinaObject $componentModel = null, $afterConstruct = true)
    {
        parent::__construct($componentModel, $afterConstruct);
        $this->mediaRepository = $this->container->make('medias_repository');
        $this->mediaContainersRepository = $this->container->make('medias_containers_repository');
    }

    /**
     * @return array
     */
    public function getComponentsProperties()
    {
        $this->loadChildrenRecursively();
        $componentsProperties = [];

        foreach ($this->children as $child) {
            $componentsProperties[] = [
                'id' => $child->model->getId(),
                'label' => $child->model->getLabel(),
                'class' => $child->getShortClass(),
                'metadata' => $child->getMetadata(false),
            ];
        }

        return $componentsProperties;
    }
}
