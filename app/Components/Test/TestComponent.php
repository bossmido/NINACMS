<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Test;

use Nina\Component\BaseComponent;
use Nina\Field\FieldCheckbox;
use Nina\Field\FieldCheckboxes;
use Nina\Field\FieldFile;
use Nina\Field\FieldForComponentProperties;
use Nina\Field\FieldImage;
use Nina\Field\FieldLinks;
use Nina\Field\FieldList;
use Nina\Field\FieldRichTextTinyMce;
use Nina\Field\FieldSeparator;

class TestComponent extends BaseComponent
{
    protected $name = 'Composant de test';

    protected $fieldsRules = [
        'test_contenu' => 'required',
        'test_contenu_2' => 'required',

        'test_links' => 'array|between:1,3', // between:1,3 => min:1|max:3
        'test_links.*.url' => 'required',
        'test_links.*.label' => 'required',

        'test_links_2' => 'array|min:3',
        'test_links_2.*.url' => 'required',
        'test_links_2.*.label' => 'required',

        // test_fichier n'est qu'au final un tableau d'ID de médias
        'test_fichier' => 'array',
    ];

    protected $fieldsFileRules = [
        'test_fichier' => 'file|nina_mimes:image,html|max:1536',
        'test_fichier_2' => 'file|nina_mimes:html|max:5120',
        'test_fichier_3' => 'file|max:1536',
    ];

    public function afterConstruct()
    {
        $this->fields = [
            'FieldRichTextTinyMce' => [
                'name' => 'FieldRichTextTinyMce',
                'fields' => [
                    (new FieldRichTextTinyMce('test_contenu', 'Contenu'))->addCommonProperties(),
                    (new FieldRichTextTinyMce('test_contenu_2', 'Mon autre contenu'))->addCommonProperties(),
                ],
            ],
            'FieldImage' => [
                'name' => 'FieldImage',
                'fields' => [
                    (new FieldImage('test_image', 'Image'))->addCommonProperties()->removeTagProperty(),
                ],
            ],
            'FieldFile' => [
                'name' => 'FieldFile',
                'fields' => [
                    new FieldFile('test_fichier', 'Envoyer un fichier', ['image', 'html'], 1536),
                ],
            ],
            'FieldFile2' => [
                'name' => 'FieldFile 2',
                'fields' => [
                    new FieldFile('test_fichier_2', 'Envoyer un fichier', ['html'], 5120),
                ],
            ],
            'FieldFile3' => [
                'name' => 'FieldFile 3',
                'fields' => [
                    new FieldFile('test_fichier_3', 'Envoyer un fichier', '*', 5120),
                ],
            ],
            'FieldLinks' => [
                'name' => 'FieldLinks',
                'fields' => [
                    (new FieldLinks('test_links', 'Une liste de liens (1 à 3)', ['min' => 1, 'max' => 3]))
                        ->addCssClassesProperty()->addVisibilityProperties(),
                    new FieldSeparator(),
                    new FieldLinks('test_links_2', 'Une liste de liens (3 et plus)', [
                        'min' => 3,
                        'infinite' => true,
                    ]),
                ],
            ],
            'FieldList' => [
                'name' => 'FieldList',
                'fields' => [
                    new FieldList('test_list', 'Liste', [
                        'placeholder' => 'Choisissez une valeur',
                        'list' => [
                            'value1' => ['text' => 'Valeur 1'],
                            'value2' => ['text' => 'Valeur 2 (défaut)', 'selectedByDefault' => true],
                            'value3' => ['text' => 'Valeur 3'],
                        ],
                    ]),
                ],
            ],
            'FieldCheckboxes' => [
                'name' => 'FieldCheckboxes',
                'fields' => [
                    new FieldCheckbox('test_checkbox_1', 'Case 1'),
                    new FieldCheckbox('test_checkbox_2', 'Case 2'),
                    new FieldCheckboxes('test_checkboxes', 'Liste de checkbox', [
                        ['id' => '1', 'label' => 'Valeur 1'],
                        ['id' => '2', 'label' => 'Valeur 2'],
                        ['id' => '3', 'label' => 'Valeur 3'],
                    ]),
                ],
            ],
            'component_properties' => [
                'name' => 'Propriétés du composant',
                'fields' => [
                    new FieldForComponentProperties(),
                ],
            ],
        ];
    }
}
