<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Columns;

use App\ComponentGroup;
use Nina\Component\BaseComponent;
use Nina\Field\FieldForComponentProperties;

class ColumnsComponent extends BaseComponent
{
    protected $group = ComponentGroup::DISPLAY_COLUMNS;

    protected $name = 'Colonnes';

    protected $contains = ['Column'];

    protected $icon = 'columns.png';

    protected $adminStyles = ['component.css'];

    public function afterConstruct()
    {
        $this->fields = [
            'component_properties' => [
                'name' => 'Propriétés du composant',
                'fields' => [
                    (new FieldForComponentProperties())->removeTagProperty(),
                ],
            ],
        ];

        $label = $this->model->getLabel();
        $icons = [
            'Bloc 1 colonne' => 'columns1.png',
            'Bloc 2 colonnes' => 'columns2.png',
            'Bloc 3 colonnes' => 'columns3.png',
            'Bloc 4 colonnes' => 'columns4.png',
        ];

        if (isset($icons[$label])) {
            $this->icon = $icons[$label];
        }
    }
}
