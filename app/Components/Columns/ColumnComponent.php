<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Columns;

use Nina\Component\BaseComponent;
use Nina\Field\FieldForComponentProperties;

class ColumnComponent extends BaseComponent
{
    protected $name = 'Colonne';

    protected $contains = ['Columns', 'Article', 'Test'];

    public function afterConstruct()
    {
        $this->fields = [
            'component_properties' => [
                'name' => 'Propriétés du composant',
                'fields' => [
                    (new FieldForComponentProperties())->removeTagProperty()->removeVisibilityProperties(),
                ],
            ],
        ];
    }
}
