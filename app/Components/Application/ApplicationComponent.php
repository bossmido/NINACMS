<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App\Components\Application;

use App\ComponentGroup;
use Nina\Component\BaseComponent;

/**
 * Class ApplicationComponent
 *
 * @package App\Components\Application
 */
class ApplicationComponent extends BaseComponent
{
    protected $contains = ['Library', 'Page'];

    protected $group = ComponentGroup::ESSENTIALS;
}
