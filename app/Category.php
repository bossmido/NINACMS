<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace App;

/**
 * Class Category
 *
 * @package App
 */
class Category
{
    const COMPONENT = 'component';
    const MEDIAS_CONTAINER = 'medias_container';
    const MEDIAS_CONTAINER_FOR_UPLOADS = 'medias_container_for_uploads';
    const MEDIA = 'media';
}
