<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

require_once __DIR__ . '/../bootstrap/bootstrap.php';

use Illuminate\Http\Request;

$request = Request::capture();
$container->instance(Request::class, $request);

$response = $router->dispatch($request);

return $response->send();
