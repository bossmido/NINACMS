<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * ninaCMS content object.
 *
 * @author Philippe Boireaud <philippe.boireaud@gmail.com>
 */
namespace Nina;

class ContentObject
{
    protected $fields;

    function __construct($fields = null)
    {
        $this->setContent($fields);
    }

    public function setContent($fields)
    {
        $this->fields = ($fields ? $fields : []);
    }

    public function set($key, $value)
    {
        if (! isset($this->fields[$key])) {
            $this->fields[$key] = [];
        }
        $this->fields[$key]['value'] = $value;
    }

    public function get($key)
    {
        if (! $key) {
            return false;
        }
        if ($this->fields[$key]) {
            return $this->fields[$key]['value'];
        }
    }

    public function setAttribute($key, $attr, $value)
    {
        if (! isset($this->fields[$key])) {
            $this->fields[$key] = [];
        }
        $this->fields[$key][$attr] = $value;
    }

    public function getAttribute($key, $attr)
    {
        if (! $key) {
            return false;
        }
        if (isset($this->fields[$key])) {
            return $this->fields[$key][$attr];
        }
    }

    public function listAttributes($key)
    {
        if (! $key) {
            return false;
        }
        $ret = [];
        foreach ((array)$this->fields[$key] as $k => $v) {
            if ($k == 'value') {
                continue;
            }
            $ret[$k] = $v;
        }

        return $ret;
    }

    public function getContent()
    {
        return $this->fields;
    }

    public function flush()
    {
        $this->fields = [];
    }
}

