<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * ninaCMS collection object.
 *
 * @author Philippe Boireaud <philippe.boireaud@gmail.com>
 */

namespace Nina;

use Illuminate\Container\Container;
use Nina\Component\BaseComponent;
use Nina\Component\ComponentProvider;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Exceptions\ComponentCanNotContainsComponentException;

class Collection
{
    /**
     * @var BaseComponent[]
     */
    protected $objects;

    protected $parents;

    protected $root_id;

    protected $templates = [];

    protected $templates_path = [];

    /**
     * @var ComponentRepository
     */
    private $componentRepository;

    /**
     * @var ComponentProvider
     */
    private $componentProvider;

    function __construct(
        ComponentRepository $componentRepository,
        ComponentProvider $componentProvider
    ) {
        $this->container = Container::getInstance();
        $this->componentRepository = $componentRepository;
        $this->componentProvider = $componentProvider;
    }

    public function displayCollection($root_id, $param = [], $display_param = [])
    {
        $ret = [
            'html' => '',
            'styles' => [],
        ];

        $this->root_id = $root_id;

        // TODO: Undefined index: filter
        if ($param['depth'] === 0) {
            $param['filter'] .= ' AND id=' . $root_id;
        }

        $this->fetchObjects($root_id, $param);

        if (empty($this->objects)) {
            return $ret;
        }

        $this->makeParentsAndChildrenRelationship();
        $root = $this->getContent($param);

        $ret['html'] = $root->render(['tidy' => false]);
        $ret['styles'] = $this->fetchStyles();

        return $ret;
    }

    private function fetchObjects($root_id, $param = [])
    {
        $components = $this->componentRepository->getCollection($root_id);
        $componentsArray = [];

        if ($components) {
            foreach ($components as $i => $component) {
                if ($component->class === 'Library') {
                    if ($i === 0) {
                        $this->objects = [];

                        return;
                    }

                    continue;
                }

                $componentFqcn = $this->componentProvider->get($component->class . 'Component');
                /** @var BaseComponent $componentInstance */
                $componentInstance = new $componentFqcn($component);
                $componentsArray[$componentInstance->getModel()->getId()] = $componentInstance;
            }
        }

        return $this->objects = $componentsArray;
    }

    /**
     * @return array
     */
    private function fetchStyles()
    {
        $ret = [];

        foreach ($this->objects as $object) {
            $shortClass = $object->getShortClass();

            if (!isset($ret[$shortClass]) && !empty($styles = $object->getStyles())) {
                $ret[$shortClass] = $styles;
            }
        }

        return $ret;
    }

    private function makeParentsAndChildrenRelationship()
    {
        foreach ($this->objects as $component) {
            /** @var BaseComponent $component */
            $model = $component->getModel();
            $this->parents[$model->getParentId()][$model->getOrder() . '_' . $model->getId()] = $model->getId();
        }
    }

    /**
     * @param $param
     *
     * @return BaseComponent
     */
    private function getContent(array $param)
    {
        $filter = '';
        $root = $this->objects[$this->root_id];

        return $this->_getContent($root, $filter);
    }

    /**
     * @param               $filter
     * @param BaseComponent $root
     *
     * @return BaseComponent
     * @throws ComponentCanNotContainsComponentException
     */
    private function _getContent($root, $filter)
    {
        $children = get($this->parents[$root->getModel()->getId()]);

        if (is_array($children)) {
            ksort($children);

            foreach ($children as $order => $id) {
                $children = $this->_getContent($this->objects[$id], $filter);

                if (!$root->canContains($children->getShortClass())) {
                    throw new ComponentCanNotContainsComponentException($root, $children);
                }

                $root->addChild($children);
            }
        }

        return $root;
    }

    /**
     * @param BaseComponent $root
     * @param array         $param
     *
     * @return bool
     */
    public function display($root, $param = [])
    {
        return $root->render($param);
    }

    public function setRootId($root_id)
    {
        $this->root_id = $root_id;
    }
}

