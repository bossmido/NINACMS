<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina;

/**
 * Class NinaException
 *
 * @package Nina
 */
class NinaException extends \Exception
{
    /**
     * NinaException constructor.
     */
    public function __construct()
    {
        parent::__construct($this->buildMessage());
    }

    /**
     * Construit le message d'erreur.
     *
     * @return array|null|string
     */
    public function buildMessage()
    {
        return __('exceptions.' . get_class($this), $this->replaces());
    }

    /**
     * Retourne les propriétées publiques de l'exception.
     *
     * @return array
     */
    protected function replaces()
    {
        $publicProperties = get_object_vars($this);

        return array_filter($publicProperties, function($publicProperty) {
            return !is_array($publicProperty) && !is_object($publicProperty);
        });
    }
}

