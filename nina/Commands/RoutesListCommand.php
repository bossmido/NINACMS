<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Commands;

use Illuminate\Console\Command;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;

/**
 * Class RoutesListCommand
 *
 * @package Nina\Commands
 */
class RoutesListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'routes:list';

    /**
     * @var string
     */
    protected $description = "Liste les routes de l'application";

    /**
     * Exécute la commande.
     */
    public function handle()
    {
        /** @var Router $router */
        $router = $this->getLaravel()->make('router');

        $routes = [];

        foreach ($router->getRoutes() as $route) {
            /** @var Route $route */
            $routes [] = [
                'method' => implode(', ', $route->methods()),
                'uri' => $route->uri(),
                'action' => $route->getActionName(),
            ];
        }

        $this->table(['Méthode', 'URI', 'Action'], $routes);
    }
}
