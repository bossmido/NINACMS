<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Template\Twig;

use Illuminate\Container\Container;
use Illuminate\Contracts\Routing\UrlGenerator;
use Nina\Exceptions\FieldNotFoundInComponentException;
use Nina\Field\BaseField;
use Nina\Services\FieldRendererService;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

/**
 * Class TwigNinaExtension
 *
 * @package Nina\Template\Twig
 */
class TwigNinaExtension extends Twig_Extension
{

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * TwigNinaExtension constructor.
     */
    public function __construct()
    {
        $container = Container::getInstance();
        $this->urlGenerator = $container->make(UrlGenerator::class);
    }

    /**
     * Retourne le nom de l'extension.
     *
     * @return string
     */
    public function getName()
    {
        return 'TwigNinaExtension';
    }

    /**
     * Retourne les filtres.
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            'ninaHtmlAttributes' => new Twig_SimpleFilter('ninaHtmlAttributes', [$this, 'ninaHtmlAttributes']),
            'ninaStyles' => new Twig_SimpleFilter('ninaStyles', [$this, 'ninaStyles']),
            'ninaColWidthPercToBootstrapCol' => new Twig_SimpleFilter(
                'ninaColWidthPercToBootstrapCol',
                [$this, 'ninaColWidthPercToBootstrapCol']
            ),
        ];
    }

    /**
     * Retourne les fonctions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            'url' => new Twig_SimpleFunction('url', [$this, 'url']),
            'renderField' => new Twig_SimpleFunction(
                'renderField',
                [$this, 'renderField'],
                [
                    'needs_context' => true,
                    'is_safe' => ['html'],
                ]
            ),
        ];
    }

    /**
     * Parse un tableau d'attributs et en retourne une chaîne de caractères pour être utilisé dans un template Twig.
     *
     * @param array|string $attributes
     *
     * @return string
     */
    public function ninaHtmlAttributes($attributes)
    {
        $ret = '';

        if (!is_array($attributes)) {
            return '';
        }

        foreach ($attributes as $attribute => $value) {
            if (in_array($attribute, ['style', 'before', 'after'])) {
                continue;
            }

            if (is_array($value)) {
                $ret .= ' ' . $attribute . '="' . implode(', ', $value) . '"';
            } else if ($attribute === 'format') {
                $ret .= ' pattern="' . $value . '"';
            } else if ($attribute === 'control') {
                // Todo: Faire un truc ici
            } else if ($attribute === 'mandatory') {
                if ($value == true) {
                    $ret .= ' required="required"';
                }
            } else if ($value === true) {
                $ret .= ' ' . $attribute;
            } else {
                $ret .= ' ' . $attribute . '="' . $value . '"';
            }
        }

        return $ret;
    }

    /**
     * Parse un tableau de styles et en retourne une chaîne de caractères pour être utilisé dans un template Twig.
     *
     * @param array|null $styles
     *
     * @return string
     */
    public function ninaStyles($styles)
    {
        if (!is_array($styles)) {
            return '';
        }

        $ret = '';

        foreach ($styles as $prop => $value) {
            $ret .= " $prop: $value;";
        }

        return $ret;
    }

    /**
     * Converti une largeur entre 0 - 100%, en un nom de classe pour Bootstrap.
     *
     * @param string|integer $width
     *
     * @return string
     */
    public function ninaColWidthPercToBootstrapCol($width)
    {
        $width = (int)str_replace('%', '', $width);
        $width = max($width, 0);
        $width = min($width, 100);

        return 'col-md-' . ceil($width * 12 / 100);
    }

    /**
     * Génère une adresse URL.
     *
     * @param string $path   Chemin
     * @param array  $extra  Paramètres optionnels
     * @param null   $secure Force ou non le https
     *
     * @return string
     */
    public function url($path, $extra = [], $secure = null)
    {
        return $this->urlGenerator->to($path, $extra, $secure);
    }

    /**
     * Effectue le rendu d'un Field.
     *
     * @param array  $context  Contexte courant Twig
     * @param string $fieldId  L'ID du Field
     * @param string $fragment Le nom du Fragment à rendre
     *
     * @return string
     * @throws FieldNotFoundInComponentException
     */
    public function renderField($context, $fieldId, $fragment = 'Text')
    {
        /** @var \stdClass $ninaObject */
        $ninaObject = $context['ninaObject'];

        /** @var BaseField $field */
        $field = $ninaObject->flattenFields[$fieldId];

        if ($field === null) {
            throw new FieldNotFoundInComponentException($fieldId, $ninaObject->shortClass);
        }

        return FieldRendererService::render($field, $fragment, $ninaObject);
    }
}
