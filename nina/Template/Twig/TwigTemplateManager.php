<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Template\Twig;

use Illuminate\Container\Container;
use Illuminate\Contracts\Routing\UrlGenerator;
use Nina\Context;
use Nina\Contracts\TemplateManager;
use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Extensions_Extension_Intl;
use Twig_Loader_Filesystem;

/**
 * Class TwigTemplateManager
 *
 * @package Nina\Template\Twig
 */
class TwigTemplateManager implements TemplateManager
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * TwigTemplateManager constructor.
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
        $this->urlGenerator = Container::getInstance()->make(UrlGenerator::class);
    }

    public function process($template, array $data = [], array $param = [])
    {
        $templates_path = [
            NINA_ROOT_PATH,
            NINA_TEMPLATES_PATH,
            NINA_TEMPLATES_PATH . DS . 'fragments',
            NINA_PATH,
        ];

        if (isset($param['templates_path'])) {
            $templates_path = array_merge($templates_path, $param['templates_path']);
        }

        $loader = new Twig_Loader_Filesystem($templates_path);
        $loader_param = [];

        if (NINA_ENVIRONMENT === 'prod') {
            $loader_param['cache'] = NINA_CACHE_PATH;
        } else {
            $loader_param['debug'] = true;
        }

        $twig = new Twig_Environment($loader, $loader_param);

        if (NINA_ENVIRONMENT !== 'prod') {
            $twig->addExtension(new Twig_Extension_Debug());
        }

        $twig->addExtension(new Twig_Extensions_Extension_Intl());
        $twig->addExtension(new TwigNinaExtension());

        $data['BaseComponent'] = 'BaseComponent.html.twig';
        $data['BaseFragment'] = 'BaseFragment.html.twig';
        $data['ninaMediathequeUrl'] = $this->urlGenerator->to(NINA_MEDIATHEQUE_URL);
        $data['ninaLocale'] = $this->context->getLocale();
        $data['ninaEditMode'] = $this->context->isEditModeEnabled();

        return $twig->render($template, $data);
    }
}
