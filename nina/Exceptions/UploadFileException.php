<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Illuminate\Http\UploadedFile;
use Nina\NinaException;

/**
 * Class UploadFileException
 *
 * @package Nina\Exceptions
 */
class UploadFileException extends NinaException
{
    /**
     * Le nom du fichier uploadé.
     * @var string
     */
    public $filename;

    /**
     * UploadFileException constructor.
     *
     * @param \Illuminate\Http\UploadedFile $uploadedFile
     */
    public function __construct(UploadedFile $uploadedFile)
    {
        $this->filename = $uploadedFile->getClientOriginalName();
        parent::__construct();
    }
}
