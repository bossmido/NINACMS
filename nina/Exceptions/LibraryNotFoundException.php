<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class LibraryNotFoundException
 *
 * @package Nina\Exceptions
 */
class LibraryNotFoundException extends NinaException
{
    /**
     * L'identifiant racine par lequel on a tenté de récupérer la Library.
     * @var mixed
     */
    public $rootId;

    /**
     * LibraryNotFoundException constructor.
     *
     * @param mixed $rootId
     */
    public function __construct($rootId)
    {
        $this->rootId = $rootId;
        parent::__construct();
    }
}
