<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class ComponentNotFoundInProviderException
 *
 * @package Nina\Exceptions
 */
class ComponentNotFoundInProviderException extends NinaException
{
    /**
     * Nom du composant.
     * @var string
     */
    public $componentName;

    /**
     * ComponentNotFoundInProviderException constructor.
     *
     * @param $componentName
     */
    public function __construct($componentName)
    {
        $this->componentName = $componentName;
        parent::__construct();
    }
}
