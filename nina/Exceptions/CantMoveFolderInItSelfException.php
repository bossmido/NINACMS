<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class CantMoveFolderInItSelfException
 *
 * @package Nina\Exceptions
 */
class CantMoveFolderInItSelfException extends NinaException
{
}
