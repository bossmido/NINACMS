<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Kocal\Validator\Validator;
use Nina\NinaException;

/**
 * Class ValidationException
 *
 * @package Nina\Exceptions
 */
class ValidationException extends NinaException
{
    /**
     * Les erreurs de validation.
     * @var array
     */
    public $validationErrors = [];

    /**
     * ValidationException constructor.
     *
     * @param \Kocal\Validator\Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validationErrors = $validator->errors()->all();
        parent::__construct();
    }
}
