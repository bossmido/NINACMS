<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class FileNotFoundException
 *
 * @package Nina\Exceptions
 */
class FileNotFoundException extends NinaException
{
    /**
     * Identifiant du fichier (média).
     * @var mixed
     */
    public $id;

    /**
     * FileNotFoundException constructor.
     *
     * @param mixed $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        parent::__construct();
    }
}
