<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class ComponentAlreadyRegisteredException
 *
 * @package Nina\Exceptions
 */
class ComponentAlreadyRegisteredException extends NinaException
{
    /**
     * Le nom du composant déjà enregistré.
     * @var string
     */
    public $componentName;

    /**
     * ComponentAlreadyRegisteredException constructor.
     *
     * @param $componentName string
     */
    public function __construct($componentName)
    {
        $this->$componentName = $componentName;
        parent::__construct();
    }
}
