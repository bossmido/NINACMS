<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class FileIsNotAllowedToBeUploadedException
 *
 * @package Nina\Exceptions
 */
class FileIsNotAllowedToBeUploadedException extends NinaException
{
    /**
     * Nom du fichier.
     * @var string
     */
    public $filename;

    /**
     * FileIsNotAllowedToBeUploadedException constructor.
     *
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
        parent::__construct();
    }
}
