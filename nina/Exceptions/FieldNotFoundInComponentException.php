<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class FieldNotFoundInComponentException
 *
 * @package Nina\Exceptions
 */
class FieldNotFoundInComponentException extends NinaException
{
    /**
     * Nom du champ.
     * @var string
     */
    public $fieldName;

    /**
     * Nom du composant.
     * @var string
     */
    public $componentName;

    /**
     * FieldNotFoundInComponentException constructor.
     *
     * @param string $fieldName
     * @param string $componentName
     */
    public function __construct($fieldName, $componentName)
    {
        $this->fieldName = $fieldName;
        $this->componentName = $componentName;
        parent::__construct();
    }
}
