<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class ComponentNotFoundException
 *
 * @package Nina\Exceptions
 */
class ComponentNotFoundException extends NinaException
{
    /**
     * Identifiant du composant.
     * @var mixed
     */
    public $id;

    /**
     * ComponentNotFoundException constructor.
     *
     * @param mixed $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        parent::__construct();
    }
}
