<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\NinaException;

/**
 * Class FolderNotFoundException
 *
 * @package Nina\Exceptions
 */
class FolderNotFoundException extends NinaException
{
    /**
     * L'identifiant du dossier (conteneur de médias)
     * @var mixed
     */
    public $id;

    /**
     * FolderNotFoundException constructor.
     *
     * @param mixed $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        parent::__construct();
    }
}
