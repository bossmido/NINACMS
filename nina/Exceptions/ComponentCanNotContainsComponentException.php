<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

use Nina\Component\BaseComponent;
use Nina\NinaException;

/**
 * Class ComponentCanNotContainsComponentException
 *
 * @package Nina\Exceptions
 */
class ComponentCanNotContainsComponentException extends NinaException
{
    /**
     * Nom du composant parent.
     *
     * @var string
     */
    public $parentComponentName;

    /**
     * Nom du composant enfant.
     *
     * @var string
     */
    public $childComponentName;

    /**
     * ComponentCanNotContainsComponentException constructor.
     *
     * @param \Nina\Component\BaseComponent $parentComponent
     * @param \Nina\Component\BaseComponent $childComponent
     */
    public function __construct(BaseComponent $parentComponent, BaseComponent $childComponent)
    {
        $this->parentComponentName = $parentComponent->getShortClass();
        $this->childComponentName = $childComponent->getShortClass();
        parent::__construct();
    }
}
