<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Traits;

use Nina\Field\BaseField;
use Nina\Field\FieldCheckbox;
use Nina\Field\FieldList;
use Nina\Field\FieldText;

/**
 * Trait PropertiesTrait
 *
 * @package nina\Traits
 */
trait PropertiesTrait
{
    /**
     * @var array[string]BaseField
     */
    public $properties = [];

    /**
     * Doit retourner un identifiant pour préfixé l'id des propriétés.
     */
    public abstract function getIdForProperty();

    /**
     * @param string    $name
     * @param BaseField $field
     *
     * @return $this
     */
    public function addProperty($name, BaseField $field)
    {
        $this->properties[$name] = $field;

        return $this;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function removeProperty($name)
    {
        if (isset($this->properties[$name])) {
            unset($this->properties[$name]);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addCommonProperties()
    {
        $this->addTagProperty();
        $this->addCssClassesProperty();
        $this->addVisibilityProperties();

        return $this;
    }

    /**
     * Ne doit pas être utilisée en même temps que `PropertiesTrait::addTitleTagProperty()`.
     *
     * @see PropertiesTrait::addTitleTagProperty()
     *
     * @return $this
     */
    public function addTagProperty()
    {
        $field = new FieldList($this->getIdForProperty() . '_tag', 'Tag HTML', [
            'defaultValue' => 'div',
            'list' => [
                'div' => ['text' => '<div>', 'selectedByDefault' => true],
                'section' => ['text' => '<section>'],
                'span' => ['text' => '<span>'],
                'article' => ['text' => '<article>'],
            ],
        ]);

        return $this->addProperty('tag', $field);
    }

    /**
     * Ne doit pas être utilisée en même temps que `PropertiesTrait::addTagProperty()`.
     *
     * @see PropertiesTrait::addTagProperty()
     *
     * @return $this
     */
    public function addTitleTagProperty()
    {
        $field = new FieldList($this->getIdForProperty() . '_tag', 'Tag HTML', [
            'defaultValue' => 'h2',
            'list' => [
                'h1' => ['text' => 'Titre de niveau 1 (<h1>)'],
                'h2' => ['text' => 'Titre de niveau 2 (<h2>)'],
                'h3' => ['text' => 'Titre de niveau 3 (<h3>)'],
                'h4' => ['text' => 'Titre de niveau 4 (<h4>)'],
                'h5' => ['text' => 'Titre de niveau 5 (<h5>)'],
                'h6' => ['text' => 'Titre de niveau 6 (<h6>)'],
            ],
        ]);

        return $this->addProperty('tag', $field);
    }

    /**
     * @return $this
     */
    public function removeTagProperty()
    {
        return $this->removeProperty('tag');
    }

    /**
     * @return $this
     */
    public function addCssClassesProperty()
    {
        $field = new FieldText($this->getIdForProperty() . '_css_classes', 'Classes CSS');

        return $this->addProperty('css_classes', $field);
    }

    /**
     * @return $this
     */
    public function removeCssClassesProperty()
    {
        return $this->removeProperty('css_classes');
    }

    /**
     * @return $this
     */
    public function addVisibilityProperties()
    {
        $this->addProperty('visibility_hidden_on_computer', new FieldCheckbox(
            $this->getIdForProperty() . '_visibility_hidden_on_computer',
            'Caché sur ordinateur',
            ['bigLabel' => 'Visibilité']
        ));

        $this->addProperty('visibility_hidden_on_tablet', new FieldCheckbox(
            $this->getIdForProperty() . '_visibility_hidden_on_tablet',
            'Caché sur tablette'
        ));

        $this->addProperty('visibility_hidden_on_smartphone', new FieldCheckbox(
            $this->getIdForProperty() . '_visibility_hidden_on_smartphone',
            'Caché sur smartphone'
        ));

        return $this;
    }

    /**
     * @return $this
     */
    public function removeVisibilityProperties()
    {
        $this->removeProperty('visibility_hidden_on_computer');
        $this->removeProperty('visibility_hidden_on_tablet');
        $this->removeProperty('visibility_hidden_on_smartphone');

        return $this;
    }
}
