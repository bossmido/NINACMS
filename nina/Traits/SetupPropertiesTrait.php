<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Traits;

use Nina\Field\BaseField;

/**
 * Trait SetupPropertiesTrait
 *
 * @package Nina\Traits
 */
trait SetupPropertiesTrait
{
    /**
     * @var string
     */
    private $cssNinaNamespace = 'nina';

    /**
     * @return mixed
     */
    protected abstract function getProperties();

    /**
     * @return mixed
     */
    protected abstract function prepareProperties();

    /**
     * @param array $templateData
     */
    protected function handleProperties(array &$templateData)
    {
        $this->handleTagProperty($templateData['tag']);
        $this->handleCssClassesProperty($templateData['attrClass']);
        $this->handleVisibilityProperty($templateData['attrClass']);

        $templateData['attrClass'] = implode(' ', $templateData['attrClass']);
    }

    /**
     * @param string $tag
     */
    private function handleTagProperty(&$tag)
    {
        $property = get($this->getProperties()['tag']);

        if ($property instanceof BaseField) {
            $tag = $property->value ? $property->value : 'div';
        }
    }

    /**
     * @param array $attrClass
     */
    private function handleCssClassesProperty(array &$attrClass)
    {
        $property = get($this->getProperties()['css_classes']);

        if ($property instanceof BaseField) {
            $attrClass[] = $property->value;
        }
    }

    /**
     * @param array $attrClass
     */
    private function handleVisibilityProperty(array &$attrClass)
    {
        $properties = $this->getProperties();
        $prefix = 'visibility_hidden_on_';
        $devices = ['computer', 'tablet', 'smartphone'];

        foreach ($devices as $device) {
            if (isset($properties[$prefix . $device])
                && $properties[$prefix . $device] instanceof BaseField
                && $properties[$prefix . $device]->value !== null
            ) {
                $attrClass[] = $this->cssNinaNamespace . '-hidden-on-' . $device;
            }
        }
    }
}
