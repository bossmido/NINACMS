<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Traits;

trait MediathequeTrait
{
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return true
     */
    public function isEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param array $array
     *
     * @return array
     */
    public function getAdditionalProperties(array $array)
    {
        return $array;
    }
}
