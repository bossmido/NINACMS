<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use App\Category;
use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Kocal\Validator\Validator;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\FilenameCantBeEmptyException;
use Nina\Exceptions\FilenameCantBeOnlyComposedWithSpacesOrDotsException;
use Nina\Exceptions\FilenameContainsIllegalCharacterException;

function __($key, array $replace = [], $locale = null, $fallback = true)
{
    /** @var \Illuminate\Translation\Translator $translator */
    $translator = Container::getInstance()->make('translator');

    return $translator->get($key, $replace, $locale, $fallback);
}

/**
 * @param string $template
 * @param array  $data
 * @param array  $params
 *
 * @return string
 */
function render($template, array $data = [], array $params = [])
{
    $container = Container::getInstance();

    /** @var \Nina\Contracts\TemplateManager $templateManager */
    $templateManager = $container->make('template_manager');

    $html = $templateManager->process($template, $data, $params);

    // Tidy est désactivé si NINA_USE_TIDY === false, où si $params['tidy'] est spécifié et vaut `false`
    if (NINA_USE_TIDY && get($params['tidy'], true)) {
        return tidy_repair_string(
            $html,
            [
                'clean' => true,
                'char-encoding' => 'utf8',
                'indent' => true,
                'merge-divs' => false,
                'wrap' => 120,
            ]
        );
    }

    return $html;
}

/**
 * @param string $page
 * @param array  $data
 * @param array  $params
 *
 * @return string
 */
function renderPage($page, $data = [], $params = [])
{
    return render('pages' . DS . $page . '.html.twig', $data, $params);
}

/**
 * @param $data
 *
 * @return JsonResponse
 */
function json($data = null)
{
    return new JsonResponse($data);
}

/**
 * Transforme une chaîne de caractères en sa version CamelCase.
 *
 * @param       $str
 * @param array $noStrip
 *
 * @return mixed|string
 */
function camelCase($str, array $noStrip = [])
{
    // non-alpha and non-numeric characters become spaces
    $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
    $str = trim($str);

    // uppercase the first character of each word
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    $str = lcfirst($str);

    return $str;
}

/**
 * Get value or a default value from an array.
 *
 * @param      $var
 * @param null $default
 *
 * @return null|mixed
 */
function get(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

/**
 * @param array $array
 *
 * @return array
 */
function autoCastArrayValuesRecursively(array $array)
{
    foreach ($array as $n => $value) {
        if (is_object($value)) {
            continue;
        }
        if (is_array($value)) {
            $array[$n] = autoCastArrayValuesRecursively($value);
        } elseif ($value === "true") {
            $array[$n] = true;
        } elseif ($value === "false") {
            $array[$n] = false;
        } elseif ($value === true || $value === false) {
            continue;
        } elseif ($value === (string)(int)$value) {
            $array[$n] = (int)$value;
        } elseif (is_numeric($value) && ($floatValue = floatval($value)) && intval($floatValue) != $floatValue) {
            $array[$n] = $floatValue;
        } else {
            $array[$n] = $value;
        }
    }

    return $array;
}

/**
 * @param NinaObject $ninaObject
 *
 * @return array
 */
function extractNinaObjectData(NinaObject $ninaObject)
{
    /** @var MediaRepository $mediaRepository */
    $mediaRepository = Container::getInstance()->make('medias_repository');

    $data = $ninaObject->getFieldsValues();

    if (isset($data['__mother_image_id'])) {
        $motherImage = $mediaRepository->findByIdOrFail($data['__mother_image_id']);
        $data['__mother_image'] = extractNinaObjectData($motherImage);
    }

    return [
        'id' => $ninaObject->getId(),
        'parent_id' => $ninaObject->getParentId(),
        'category' => $ninaObject->getCategory(),
        'label' => $ninaObject->getLabel(),
        'data' => $data,
    ];
}

/**
 * @param $str
 *
 * @return mixed
 */
function removeMultiplesSlashes($str)
{
    return preg_replace('#/{2,}#', '/', $str);
}

/**
 * @param \Nina\Field\BaseField|array $field
 *
 * @return bool
 */
function fieldCanContainsMedias($field)
{
    $class = $field instanceof \Nina\Field\BaseField ? $field->class : $field['class'];

    return in_array($class, ['FieldFile', 'FieldImage']);
}

/**
 * @param NinaObject $ninaObject
 *
 * @return array
 */
function getFileTolerantFields(NinaObject $ninaObject)
{
    $fields = get($ninaObject->getFieldsValues()['__reflection']['fields'], []);

    return array_filter($fields, function ($field) {
        return fieldCanContainsMedias($field);
    });
}

/**
 * @param array $allowedExtensions
 *
 * @return array
 */
function formalizeAllowedExtensions(array $allowedExtensions)
{
    $aliases = Container::getInstance()->make('config')->get('nina.file_type_aliases');
    $withDot = [];
    $withoutDot = [];

    foreach ($allowedExtensions as $allowedExtension) {
        if (isset($aliases[$allowedExtension])) {
            foreach ($aliases[$allowedExtension] as $allowedAlias) {
                $withDot[] = '.' . $allowedAlias;
                $withoutDot[] = $allowedAlias;
            }
        } else {
            $withDot[] = '.' . $allowedExtension;
            $withoutDot[] = $allowedExtension;
        }
    }

    return [
        'with_dot' => $withDot,
        'without_dot' => $withoutDot,
    ];
}

/**
 * @param string $filename
 *
 * @throws Exception
 */
function checkIfFilenameIsValid($filename)
{
    static $notAllowedChars = ['/', '\\', '<', '>', '"', '|', '*', '?', ':'];

    // Le nom est vide
    if (mb_strlen($filename) === 0) {
        throw new FilenameCantBeEmptyException();
    }

    // Le nom est uniquement composé d'espaces, ou de points
    if (preg_match('#^\s+$#', $filename) || preg_match('#^[.]+$#', $filename)) {
        throw new FilenameCantBeOnlyComposedWithSpacesOrDotsException();
    }

    // Le nom contient un caractère non autorisé
    foreach ($notAllowedChars as $notAllowedChar) {
        if (strpos($filename, $notAllowedChar) !== false) {
            throw new FilenameContainsIllegalCharacterException($notAllowedChar);
        }
    }
}

/**
 * @param     $filename
 * @param     $othersExistingFilename
 * @param int $iterationCount
 *
 * @return string
 */
function generateUniqueFileName($filename, $othersExistingFilename, $iterationCount = 0)
{
    // Regex pour un nom de fichier qui contient son nom, peut-être son nombre de copie et peut-être son extension
    static $filenameRegex = '#^(.*)(?: \(([0-9]+)\))?(\.\w+)?$#U'; // le flag U est important !!

    // Si le nom est disponible, alors on le retourne directement
    if (!in_array($filename, $othersExistingFilename)) {
        return $filename;
    } else {
        $extension = null;

        // Si ça match, on récupère le nom, le nombre de copie, et l'extension
        if (preg_match($filenameRegex, $filename, $matches)) {
            $filename = $matches[1];
            $iterationCount = isset($matches[2]) ? +$matches[2] : $iterationCount;
            $extension = isset($matches[3]) ? $matches[3] : null;
        }

        $iterationCount += 1;
        $filename = sprintf('%s (%d)%s', $filename, $iterationCount, $extension);

        return generateUniqueFileName($filename, $othersExistingFilename, $iterationCount);
    }
}

/**
 * @param NinaObject               $mediasContainer
 * @param                          $category
 *
 * @return array
 */
function getChildrenNames(NinaObject $mediasContainer, $category)
{
    $container = Container::getInstance();
    $repository = null;

    switch ($category) {
        case Category::MEDIAS_CONTAINER:
        case Category::MEDIAS_CONTAINER_FOR_UPLOADS:
            /** @var MediasContainerRepository $repository */
            $repository = $container->make('medias_containers_repository');
            break;

        case Category::MEDIA:
            /** @var MediaRepository $repository */
            $repository = $container->make('medias_repository');
            break;

        default:
            throw new RuntimeException();
    }

    $children = $repository->findByParentId($mediasContainer->getId());

    return array_map(
        function ($m) {
            /** @var $m NinaObject */
            return $m->getLabel();
        },
        $children
    );
}

/**
 * @param NinaObject $ninaObject
 *
 * @return string[]
 */
function getSiblingsNamesAndAlsoMine(NinaObject $ninaObject)
{
    $container = Container::getInstance();

    /** @var MediaRepository $repository */
    $repository = $container->make(
        $ninaObject->getCategory() === Category::MEDIA
            ? 'medias_repository'
            : 'medias_containers_repository'
    );

    $siblings = $repository->findByParentId($ninaObject->getParentId());

    return array_map(
        function ($m) {
            /** @var $m NinaObject */
            return $m->getLabel();
        },
        $siblings
    );
}

/**
 * Transforme la notation du php.ini pour les nombres (ex : « 2M ») en un entier (ex : « 2*1024*1024 »).
 * Retourne des octets.
 *
 * @param string|int $size
 *
 * @return int
 */
function convertPHPSizeToBytes($size)
{
    if (is_numeric($size)) {
        return $size;
    }

    $suffix = substr($size, -1);
    $value = substr($size, 0, -1);

    switch (strtoupper($suffix)) {
        case 'P':
            $value *= 1024;
        case 'T':
            $value *= 1024;
        case 'G':
            $value *= 1024;
        case 'M':
            $value *= 1024;
        case 'K':
            $value *= 1024;
            break;
    }

    return $value;
}

/**
 * Retourne la taille maximale qu'un fichier doit peser pour être uploadé correctement.
 *
 * @return int
 */
function getMaximumFileUploadSize()
{
    return min(
        convertPHPSizeToBytes(ini_get('post_max_size')),
        convertPHPSizeToBytes(ini_get('upload_max_filesize'))
    );
}

/**
 * @param NinaObject $a
 * @param NinaObject $b
 *
 * @return int
 */
function sortByLabel(NinaObject $a, NinaObject $b)
{
    return strnatcmp($a->getLabel(), $b->getLabel());
}

/**
 * @param array $data
 * @param array $rules
 * @param array $messages
 * @param array $customAttributes
 *
 * @return \Kocal\Validator\Validator
 */
function validate(array &$data, array $rules, array $messages = [], array $customAttributes = [])
{
    $data = autoCastArrayValuesRecursively($data);

    /** @var Validator $validator */
    $validator = Container::getInstance()->make('validator');
    $validator->setRules($rules);
    $validator->validate($data, $messages, $customAttributes);

    return $validator;
}
