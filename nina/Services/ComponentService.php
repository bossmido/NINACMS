<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use Illuminate\Container\Container;
use Nina\Collection;
use Nina\Component\BaseComponent;
use Nina\Component\ComponentProvider;
use Nina\Context;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Exceptions\ValidationException;

/**
 * Class ComponentService
 *
 * @package Nina\Services
 */
class ComponentService
{
    /**
     * @var \Illuminate\Container\Container
     */
    private $container;

    /**
     * @var ComponentProvider
     */
    private $componentProvider;

    /**
     * @var ComponentRepository
     */
    private $componentRepository;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * ComponentService constructor.
     */
    public function __construct()
    {
        $this->container = Container::getInstance();
        $this->componentProvider = $this->container->make(ComponentProvider::class);
        $this->componentRepository = $this->container->make('component_repository');
        $this->mediaRepository = $this->container->make('medias_repository');
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function renderCollection($id)
    {
        /** @var \Nina\Context $context */
        $context = $this->container->make(Context::class);

        $context->setEditMode(false);
        $collection = $this->getCollection($id);

        return renderPage('public/index', [
            'content' => $collection['html'],
            'styles' => $collection['styles'],
        ]);
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getCollection($id)
    {
        /** @var Collection $collection */
        $collection = $this->container->make(Collection::class);

        return $collection->displayCollection($id, [
            'depth' => 4,
            'no_cache' => true,
            'php_include_content' => true,
        ]);
    }

    /**
     * @return array
     */
    public function getAllComponentsMetadata()
    {
        $components = $this->componentProvider->all();
        $metadata = [];

        foreach ($components as $component) {
            /** @var BaseComponent $componentInstance */
            $componentInstance = new $component(null, false);
            $metadata[$componentInstance->getShortClass()] = $componentInstance->getMetadata();
        }

        return $metadata;
    }

    /**
     * @param       $componentId
     * @param array $params
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($componentId, array $params = [])
    {
        $componentInstance = $this->componentProvider->loadFromDatabase($componentId);
        $componentInstance->afterConstruct();
        $componentInstance->loadChildrenRecursively();

        return json([
            'html' => $componentInstance->render($params),
        ]);
    }

    /**
     * @param $componentId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderEditionForm($componentId)
    {
        $componentInstance = $this->componentProvider->loadFromDatabase($componentId);
        $componentInstance->afterConstruct();

        $renderingTemplate = 'admin/NinaComponentEditionForm.html.twig';
        $renderingData = [
            'ninaObject' => [
                'id' => $componentId,
                'class' => $componentInstance->getClass(),
                'medias' => $componentInstance->getMedias(),
                'fields' => $componentInstance->getFields(),
                'fieldsValues' => $componentInstance->getModel()->getFieldsValues(),
            ],
        ];
        $renderingOptions = ['tidy' => false];

        return json([
            'html' => render($renderingTemplate, $renderingData, $renderingOptions),
        ]);
    }

    /**
     * @param array $payload
     *
     * @return BaseComponent
     */
    public function create(array $payload)
    {
        $parentComponent = $this->componentProvider->loadFromDatabase($payload['parent_id']);
        $parentComponentModel = $parentComponent->getModel();

        $componentModel = $this->componentRepository->create(
            $parentComponentModel->getRootId(),
            $parentComponentModel->getId(),
            $payload['class']
        );

        $componentFqcn = $this->componentProvider->get($componentModel->getClass() . 'Component');
        $component = new $componentFqcn($componentModel);

        if (isset($payload['styles'])) {
            $componentModel->setStyles($payload['styles']);
        }

        return $component;
    }

    /**
     * @param \Nina\Component\BaseComponent $component
     * @param array                         $style
     */
    public function saveStyle(BaseComponent $component, array $style)
    {
        $component->getModel()->setStyles($style);
        $this->componentRepository->save($component->getModel());
    }

    /**
     * @param array $payload
     */
    public function saveStyles(array $payload)
    {
        foreach ($payload as $componentPayload) {
            $component = $this->componentProvider->loadFromDatabase($componentPayload['id']);
            $this->saveStyle($component, $componentPayload['styles']);
        }
    }

    /**
     * @param \Nina\Component\BaseComponent $component
     * @param array                         $fieldsValues
     * @param array                         $allMediasIdsToUnlink
     *
     * @throws \Nina\Exceptions\ValidationException
     */
    public function saveFieldsValues(
        BaseComponent $component,
        array $fieldsValues = [],
        array $allMediasIdsToUnlink = []
    ) {
        $validator = validate($fieldsValues, $component->getFieldsRules());

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $component->getModel()->setFieldsValues($fieldsValues);
        $component->save();
        $fieldsValues = $component->getModel()->getFieldsValues();

        foreach ($allMediasIdsToUnlink as $fieldName => $mediasIdsToUnlink) {
            $this->unlinkMedias($component, $fieldName, $mediasIdsToUnlink);
        }

        foreach (getFileTolerantFields($component->getModel()) as $fieldName => $field) {
            $this->linkMedias($component, $fieldName, get($fieldsValues[$fieldName], []));
        }
    }

    /**
     * Déplace un composant dans la mémoire, tout en mettant à jour l'ordre
     * de ses voisins le suivant.
     *
     * @param BaseComponent $component
     * @param               $newParentId
     * @param               $newOrder
     */
    public function move(BaseComponent $component, $newParentId, $newOrder)
    {
        $component->move($newParentId, $newOrder);
        $component->save();
        $this->moveNextSiblingsComponents($component, $newOrder, +1);
    }

    /**
     * @param BaseComponent $component
     * @param               $newRootId
     * @param               $newParentId
     * @param               $newOrder
     *
     * @return BaseComponent
     */
    public function duplicate(BaseComponent $component, $newRootId, $newParentId, $newOrder)
    {
        $component->afterConstruct();
        $component->loadChildrenRecursively();

        $callback = function ($duplicatedComponent) {
            /** @var BaseComponent $duplicatedComponent */

            $fieldsValues = $duplicatedComponent->getModel()->getFieldsValues();

            foreach (getFileTolerantFields($duplicatedComponent->getModel()) as $fieldName => $field) {
                $this->linkMedias($duplicatedComponent, $fieldName, get($fieldsValues[$fieldName], []));
            }
        };

        $component = $component->duplicate($callback, $newRootId, $newParentId, $newOrder);
        $this->moveNextSiblingsComponents($component, $newOrder, +1);

        return $component;
    }

    /**
     * @param \Nina\Component\BaseComponent $component
     */
    public function delete(BaseComponent $component)
    {
        $component->loadChildrenRecursively();

        $onBeforeDelete = function ($componentToDelete) {
            /** @var BaseComponent $componentToDelete */
            $fieldsValues = $componentToDelete->getModel()->getFieldsValues();

            foreach (getFileTolerantFields($componentToDelete->getModel()) as $fieldName => $field) {
                $this->unlinkMedias($componentToDelete, $fieldName, get($fieldsValues[$fieldName], []));
            }
        };

        $this->moveNextSiblingsComponents($component, $component->getModel()->getOrder(), -1);
        $component->delete($onBeforeDelete, true);
    }

    /**
     * @param BaseComponent $component
     * @param               $newParentId
     *
     * @return BaseComponent
     */
    public function paste(BaseComponent $component, $newParentId)
    {
        $parentComponent = $this->componentRepository->findByIdOrFail($newParentId);

        $component = $this->duplicate(
            $component,
            $parentComponent->getRootId(),
            $parentComponent->getId(),
            $component->getModel()->getOrder() + 1
        );

        $component->save();

        return $component;
    }

    /**
     * @param BaseComponent $component
     * @param mixed         $newOrder
     */
    public function changeOrder(BaseComponent $component, $newOrder)
    {
        $component->getModel()->setOrder($newOrder);
        $this->componentRepository->save($component->getModel());
    }

    /**
     * @param BaseComponent $component
     * @param               $fieldName
     * @param               $mediasIdsToUnlink
     */
    private function unlinkMedias(BaseComponent $component, $fieldName, $mediasIdsToUnlink)
    {
        foreach ($mediasIdsToUnlink as $mediaIdToUnlink) {
            $this->unlinkMedia($component, $fieldName, $mediaIdToUnlink);
        };
    }

    /**
     * @param BaseComponent $component
     * @param               $fieldName
     * @param               $mediaIdToUnlink
     */
    private function unlinkMedia(BaseComponent $component, $fieldName, $mediaIdToUnlink)
    {
        $media = $this->mediaRepository->findById($mediaIdToUnlink);

        if (is_null($media)) {
            return;
        }

        $media->detachComponentOfMedia($component->getModel(), $fieldName);

        // Si après détachement d'un composant, le media n'est attaché à personne, on le supprime
        if ($media->isDaughterImage() && $media->isContainedNowhere()) {
            /** @var NinaMediathequeService $ninaMediatheque */
            $ninaMediatheque = $this->container->make(NinaMediathequeService::class);
            $ninaMediatheque->deleteMedia($media);

            return;
        }

        $this->mediaRepository->save($media);
    }

    /**
     * @param \Nina\Component\BaseComponent $component
     * @param  string                       $fieldName
     * @param array                         $mediasIds
     */
    private function linkMedias(BaseComponent $component, $fieldName, array $mediasIds)
    {
        foreach ($mediasIds as $mediaId) {
            $this->linkMedia($component, $fieldName, $mediaId);
        }
    }

    /**
     * @param BaseComponent $component
     * @param string        $fieldName
     * @param integer       $mediaId
     */
    private function linkMedia(BaseComponent $component, $fieldName, $mediaId)
    {
        $media = $this->mediaRepository->findByIdOrFail($mediaId);
        $media->attachComponentToMedia($component->getModel(), $fieldName);
        $this->mediaRepository->save($media);
    }

    /**
     * @param BaseComponent $component
     * @param               $order
     * @param               $offset
     */
    private function moveNextSiblingsComponents(BaseComponent $component, $order, $offset)
    {
        $nextSiblings = $this->componentRepository->findNextSiblings($component->getModel(), $order);

        foreach ($nextSiblings as $nextSibling) {
            $nextSibling->setOrder($nextSibling->getOrder() + $offset);
            $this->componentRepository->save($nextSibling);
        }
    }
}
