<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use Intervention\Image\ImageManager;

/**
 * Class ImageFileManipulator
 *
 * @package Nina\Services
 */
class ImageFileManipulatorService
{
    /**
     * Chemin de l'image à manipuler.
     *
     * @var string
     */
    private $path;

    /**
     * Permettra d'ouvrir l'image et de la manipuler.
     *
     * @var \Intervention\Image\Image
     */
    private $imageManager;

    /**
     * L'instance de l'image.
     *
     * @var \Intervention\Image\Image
     */
    private $image;

    /**
     * ImageFileManipulator constructor.
     *
     * @param string                           $path
     * @param \Intervention\Image\ImageManager $imageManager
     */
    private function __construct($path, ImageManager $imageManager)
    {
        $this->path = $path;
        $this->imageManager = $imageManager;
        $this->image = $this->imageManager->make($path);
    }

    /**
     * Crée une nouvelle instance de la classe, tout en ouvrant l'image.
     *
     * @param string $path
     * @return \Nina\Services\ImageFileManipulatorService
     */
    public static function open($path)
    {
        $imageManager = new ImageManager([
            'driver' => NINA_INTERVENTION_IMAGE_DRIVER,
        ]);

        return new self($path, $imageManager);
    }

    /**
     * Applique les différentes manipulations graphiques.
     *
     * @param array $manipulations
     *
     * @return $this
     */
    public function manipulate(array $manipulations)
    {
        $m = $manipulations;

        return $this
            ->crop($m['width'], $m['height'], $m['x'], $m['y'])
            ->rotate($m['rotate'])
            ->flip($m['scaleX'] === -1, $m['scaleY'] === -1)
            ->resize($m['display_width'], $m['display_height']);
    }

    /**
     * Redimensionne l'image.
     *
     * @param int $width
     * @param int $height
     *
     * @return $this
     */
    private function resize($width, $height)
    {
        $this->image->resize($width, $height);

        return $this;
    }

    /**
     * Inverse ou non le sens vertical et/ou horizontal.
     *
     * @param boolean $vertical
     * @param boolean $horizontal
     *
     * @return $this
     */
    private function flip($vertical, $horizontal)
    {
        if ($vertical) {
            $this->image->flip('v');
        }

        if ($horizontal) {
            $this->image->flip('h');
        }

        return $this;
    }

    /**
     * Effectue une rotation.
     *
     * @param float $angle
     *
     * @return $this
     */
    private function rotate($angle)
    {
        $this->image->rotate($angle, '#ffffff');

        return $this;
    }

    /**
     * Découpe l'image.
     *
     * @param integer $width
     * @param integer $height
     * @param integer $x
     * @param integer $y
     *
     * @return $this
     */
    private function crop($width, $height, $x, $y)
    {
        $this->image->crop(round($width), round($height), round($x), round($y));

        return $this;
    }

    /**
     * Sauvegarde l'image.
     *
     * @param $path
     *
     * @return \Intervention\Image\Image
     */
    public function save($path)
    {
        return $this->image->save($path);
    }
}
