<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use App\Entities\DatabaseNinaObject;
use Illuminate\Container\Container;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediaRepository;

class MediathequeImageHandler
{
    /**
     * @var NinaObject
     */
    private $motherImage;

    /**
     * @var NinaObject
     */
    private $daughterImage;

    /**
     * @var string
     */
    private $pathPrefix = '';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $files;

    /**
     * MediathequeImageHandler constructor.
     *
     * @param NinaObject $image
     */
    private function __construct($image)
    {
        $this->container = Container::getInstance();
        $this->mediaRepository = $this->container->make('medias_repository');
        $this->filesystem = $this->container->make(FilesystemFactory::class)->disk('mediatheque_nina');
        $this->files = $this->container->make('files');

        assert($this->filesystem instanceof \Illuminate\Filesystem\FilesystemAdapter);
        $this->pathPrefix = $this->filesystem->getDriver()->getAdapter()->getPathPrefix();

        $this->loadImages($image);
    }

    /**
     * @param NinaObject $image
     */
    private function loadImages($image)
    {
        if ($image->isMotherImage()) {
            $this->motherImage = $image;
            $this->daughterImage = $this->giveBirth();
        } else {
            $this->daughterImage = $image;
            $this->motherImage = $this->findMother();
        }
    }

    /**
     * @return DatabaseNinaObject
     */
    private function giveBirth()
    {
        // Duplication de l'image mère en image fille
        $daughterImage = $this->mediaRepository->replicate($this->motherImage);
        $daughterImage->cleanImage();
        $this->mediaRepository->save($daughterImage);

        // Liaison
        $this->motherImage->attachDaughterImageToMotherImage($daughterImage);
        $daughterImage->attachMotherImageToDaughterImage($this->motherImage);

        // Génération d'un nouveau path
        $fieldsValues = $daughterImage->getFieldsValues();
        $newNameForImage = $this->generateNewNameForImage($daughterImage->getLabel());
        $newPathForImage = $this->generateNewPathForImage($fieldsValues['path'], $newNameForImage);

        // Copie de l'image
        $this->filesystem->copy($fieldsValues['path'], $newPathForImage);
        $daughterImage->setLabel($newNameForImage);
        $fieldsValues['path'] = $newPathForImage;
        $daughterImage->setFieldsValues($fieldsValues);

        // Sauvegarde
        $this->mediaRepository->save($daughterImage);
        $this->mediaRepository->save($this->motherImage);

        return $daughterImage;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function generateNewNameForImage($name)
    {
        $fileName = $this->files->name($name);
        $fileExtension = $this->files->extension($name);

        return sprintf(
            '%s-%s.%s',
            trim($fileName, DS),
            md5(uniqid('', true)),
            $fileExtension
        );
    }

    /**
     * @param string $path
     * @param string $fileName
     *
     * @return string
     */
    private function generateNewPathForImage($path, $fileName)
    {
        $fileDirectory = $this->files->dirname($path);

        return sprintf(
            '/%s/%s',
            trim($fileDirectory, '/'),
            $fileName
        );
    }

    /**
     * @return DatabaseNinaObject
     */
    private function findMother()
    {
        return $this->mediaRepository->findByIdOrFail(
            $this->daughterImage->getFieldsValues()['__mother_image_id']
        );
    }

    /**
     * @param NinaObject $image
     *
     * @return MediathequeImageHandler
     */
    public static function handle($image)
    {
        return new self($image);
    }

    /**
     * @param array $manipulations
     */
    public function manipulate(array $manipulations)
    {
        $fieldsValues = $this->daughterImage->getFieldsValues();
        $fieldsValues['__crop'] = $manipulations;
        $fieldsValues['__updated_at'] = time();
        $originalImagePath = $this->motherImage->getFieldsValues()['path'];

        ImageFileManipulatorService
            ::open($this->pathPrefix . $originalImagePath)
            ->manipulate($manipulations)
            ->save($this->pathPrefix . $fieldsValues['path']);

        $this->daughterImage->setFieldsValues($fieldsValues);
        $this->mediaRepository->save($this->daughterImage);
    }

    /**
     * @return NinaObject
     */
    public function getMotherImage()
    {
        return $this->motherImage;
    }

    /**
     * @return NinaObject
     */
    public function getDaughterImage()
    {
        return $this->daughterImage;
    }
}
