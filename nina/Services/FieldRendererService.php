<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use Illuminate\Container\Container;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Field\BaseField;
use Nina\Traits\SetupPropertiesTrait;

/**
 * Class FieldRenderer
 *
 * @package Nina\Services
 */
class FieldRendererService
{
    use SetupPropertiesTrait;

    /**
     * @var \Nina\Field\BaseField
     */
    private $field;

    /**
     * @var string
     */
    private $fragment;

    /**
     * @var \stdClass
     */
    private $ninaObject;

    /**
     * @param \Nina\Field\BaseField $field
     * @param string                $fragment
     * @param \stdClass             $ninaObject
     *
     * @return string
     */
    public static function render(BaseField $field, $fragment, \stdClass $ninaObject)
    {
        return (new self($field, $fragment, $ninaObject))->renderField();
    }

    /**
     * FieldRenderer constructor.
     *
     * @param \Nina\Field\BaseField $field
     * @param string                $fragment
     * @param \stdClass             $ninaObject
     */
    private function __construct(BaseField $field, $fragment, \stdClass $ninaObject)
    {
        $this->container = Container::getInstance();
        $this->field = $field;
        $this->fragment = $fragment;
        $this->ninaObject = $ninaObject;
        $this->prepareField();
        $this->prepareProperties();
    }

    /**
     * @return string
     */
    private function renderField()
    {
        // Si le champ principal n'a pas de valeur, ça sert à rien de vouloir l'afficher ¯\_(ツ)_/¯
        if (!$this->field->value) {
            return;
        }

        $template = "fragments/Fragment{$this->fragment}.html.twig";
        $templateData = [
            'ninaObject' => $this->ninaObject,
            'field' => $this->field,
            'value' => $this->field->value,
            'medias' => $this->findMedias(),
            'tag' => 'div',
            'attrClass' => [],
        ];

        $this->handleProperties($templateData);

        return $this->container->make('template_manager')->process($template, $templateData);
    }

    private function findMedias()
    {
        $medias = [];

        if (fieldCanContainsMedias($this->field)) {
            /** @var MediaRepository $mediaRepository */
            $mediaRepository = $this->container->make('medias_repository');

            foreach (get($this->field->value, []) as $mediaId) {
                $medias[] = extractNinaObjectData($mediaRepository->findByIdOrFail($mediaId));
            }
        }

        return $medias;
    }

    /**
     * @param BaseField|null $field
     */
    private function prepareField(BaseField $field = null)
    {
        if (is_null($field)) {
            $field = $this->field;
        }

        $field->value = get($this->ninaObject->fieldsValues[$field->id], $field->defaultValue);
    }

    /**
     * {@inheritdoc}
     */
    protected function getProperties()
    {
        return $this->field->properties;
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareProperties()
    {
        foreach ($this->getProperties() as $property) {
            $this->prepareField($property);
        }
    }
}
