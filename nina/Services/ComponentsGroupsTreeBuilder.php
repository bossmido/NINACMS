<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use App\ComponentGroup;

/**
 * Class ComponentsGroupsTreeBuilder
 *
 * @package Nina\Services
 */
class ComponentsGroupsTreeBuilder
{
    private $rootId = '__ROOT__';

    public function build($basicTree = null, &$parentBranch = null)
    {
        $tree = [];

        if ($basicTree === null) {
            $basicTree = [
                $this->rootId => ComponentGroup::getBasicTree(),
            ];
        }

        foreach ($basicTree as $k => $v) {
            $id = is_string($v) ? $v : $k;

            $branch = [
                // Identifiant unique
                'id' => $id,
                // Nom traduit du groupe
                'name' => __('components_groups.' . $id),
                // Segments de chemin pour accéder à ce groupe. Si taille == 1, alors il est situé à la racine
                'path' => '',
                // Les groupes enfants
                'children' => [],
            ];

            if ($parentBranch !== null) {
                $branch['path'] = $parentBranch['path'];
            }

            if ($branch['id'] !== $this->rootId) {
                $branch['path'] .= '/' . $branch['name'];
            }

            if (is_array($v)) {
                $branch['children'] = $this->build($v, $branch);
            }

            $tree[] = $branch;
        }

        return $tree;
    }
}
