<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use App\Category;
use Illuminate\Container\Container;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Mediatheque;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Exceptions\CantCopyFolderInItSelfException;
use Nina\Exceptions\CantCopyUploadsFolderException;
use Nina\Exceptions\CantCreateFolderException;
use Nina\Exceptions\CantDeleteUploadsFolderException;
use Nina\Exceptions\CantMoveFolderInItSelfException;
use Nina\Exceptions\CantMoveUploadsFolderException;
use Nina\Exceptions\CantRenameFileException;
use Nina\Exceptions\CantRenameFolderException;
use Nina\Exceptions\CopyFileException;
use Nina\Exceptions\CopyFolderException;
use Nina\Exceptions\DeleteFileException;
use Nina\Exceptions\DeleteFolderException;
use Nina\Exceptions\FolderNameIsAlreadyTaken;
use Nina\Exceptions\MoveFileException;
use Nina\Exceptions\MoveFolderException;

class NinaMediathequeService implements Mediatheque
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var ComponentRepository
     */
    private $componentRepository;

    /**
     * @var MediasContainerRepository
     */
    private $mediasContainerRepository;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $files;

    /**
     * @var string
     */
    private $pathPrefix;

    public function __construct(
        ComponentRepository $componentRepository,
        MediasContainerRepository $mediasContainerRepository,
        MediaRepository $mediaRepository
    ) {
        $this->container = Container::getInstance();
        $this->componentRepository = $componentRepository;
        $this->mediasContainerRepository = $mediasContainerRepository;
        $this->mediaRepository = $mediaRepository;
        $this->filesystem = $this->container->make(FilesystemFactory::class)->disk('mediatheque_nina');
        $this->files = $this->container->make('files');

        assert($this->filesystem instanceof \Illuminate\Filesystem\FilesystemAdapter);
        $this->pathPrefix = $this->filesystem->getDriver()->getAdapter()->getPathPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function createMediasContainer($parentId, $name)
    {
        $this->checkIfFilenameIsValid($name);
        $this->checkIfMediasContainerNameIsUnique($name, $parentId);

        // Récupération du medias container parent
        $parentMediasContainer = $this->mediasContainerRepository->findByIdOrFail($parentId);
        $parentFieldsValues = $parentMediasContainer->getFieldsValues();

        // Création du nouveau medias container
        $mediasContainer = $this->mediasContainerRepository->create(
            $parentMediasContainer->getRootId(),
            $parentId,
            $name
        );
        $fieldsValues = $mediasContainer->getFieldsValues();
        $fieldsValues['path'] = $parentFieldsValues['path'] . DS . $mediasContainer->getLabel();
        $mediasContainer->setFieldsValues($fieldsValues);

        // Création du dossier dans le filesystem
        if (!$this->filesystem->makeDirectory($fieldsValues['path'])) {
            throw new CantCreateFolderException();
        }

        // Sauvegarde dans la base de données
        $this->mediasContainerRepository->save($mediasContainer);

        return $mediasContainer;
    }

    /**
     * @param $name
     */
    private function checkIfFilenameIsValid($name)
    {
        return checkIfFilenameIsValid($name);
    }

    /**
     * @param $name
     * @param $parentId
     *
     * @throws \Nina\Exceptions\FolderNameIsAlreadyTaken
     */
    private function checkIfMediasContainerNameIsUnique($name, $parentId)
    {
        $mediasContainers = $this->mediasContainerRepository->findByParentIdAndLabel($parentId, $name);

        if ($mediasContainers) {
            throw new FolderNameIsAlreadyTaken();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function renameMediasContainer(NinaObject $mediasContainer, $newName)
    {
        if ($mediasContainer->getLabel() === $newName) {
            return;
        }

        // Récupération du medias container parent + vérification sur le nouveau nom
        $parentMediasContainer = $this->mediasContainerRepository->findByIdOrFail($mediasContainer->getParentId());
        $this->checkIfFilenameIsValid($newName);
        $this->checkIfMediasContainerNameIsUnique($newName, $parentMediasContainer->getId());

        // Création du nouveau chemin
        $fieldsValues = $mediasContainer->getFieldsValues();
        $parentFieldsValues = $parentMediasContainer->getFieldsValues();
        $mediasContainer->setLabel($newName);
        $oldPath = $fieldsValues['path'];
        $newPath = $parentFieldsValues['path'] . DS . $mediasContainer->getLabel();
        $fieldsValues['path'] = $newPath;

        // Déplacement sur le disque
        if (!$this->files->moveDirectory($this->pathPrefix . DS . $oldPath, $this->pathPrefix . DS . $newPath)) {
            throw new CantRenameFolderException();
        }

        // Déplacement dans la base de données
        $mediasContainer->setFieldsValues($fieldsValues);
        $this->moveMediasContainerInDatabase($mediasContainer);
        $this->mediasContainerRepository->save($mediasContainer);
    }

    /**
     * @param NinaObject $parentMediasContainer
     */
    private function moveMediasContainerInDatabase(NinaObject &$parentMediasContainer)
    {
        $this->copyOrMoveMediasContainerInDatabase($parentMediasContainer, false);
    }

    /**
     * @param NinaObject $parentMediasContainer
     * @param bool       $copy
     */
    private function copyOrMoveMediasContainerInDatabase(NinaObject &$parentMediasContainer, $copy)
    {
        $medias = $this->mediaRepository->findByParentId($parentMediasContainer->getId());
        $mediasContainers = $this->mediasContainerRepository->findByParentId($parentMediasContainer->getId());

        if ($copy) {
            $parentMediasContainer = $this->mediasContainerRepository->replicate($parentMediasContainer);
        }

        $parentMediasContainerFieldsValues = $parentMediasContainer->getFieldsValues();
        $this->mediasContainerRepository->save($parentMediasContainer);

        $path = $parentMediasContainerFieldsValues['path'];

        foreach ($medias as $media) {
            if ($copy) {
                $media = $this->mediaRepository->replicate($media);
            }

            $mediaFieldsValues = $media->getFieldsValues();
            $mediaFieldsValues['path'] = $path . DS . $media->getLabel();

            $media->setFieldsValues($mediaFieldsValues);
            $media->setParentId($parentMediasContainer->getId());
            $this->mediaRepository->save($media);
        }

        foreach ($mediasContainers as $mediasContainer) {
            $mediasContainerFieldsValues = $mediasContainer->getFieldsValues();
            $mediasContainerFieldsValues['path'] = $path . DS . $mediasContainer->getLabel();

            $mediasContainer->setFieldsValues($mediasContainerFieldsValues);
            $this->copyOrMoveMediasContainerInDatabase($mediasContainer, $copy);
            $mediasContainer->setParentId($parentMediasContainer->getId());

            $this->mediasContainerRepository->save($mediasContainer);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function readMediasContainer(NinaObject $mediasContainer)
    {
        $id = $mediasContainer->getId();

        $mediasContainers = $this->mediasContainerRepository->findByParentId($id);
        $medias = $this->mediaRepository->findByParentId($id);
        $medias = array_filter(
            $medias,
            function ($media) {
                /** @var NinaObject $media */
                return !$media->isDaughterImage();
            }
        );

        usort($mediasContainers, 'sortByLabel');
        usort($medias, 'sortByLabel');

        return [$mediasContainers, $medias];
    }

    /**
     * {@inheritdoc}
     */
    public function copyMediaContainers(NinaObject $mediasContainer, NinaObject $mediasContainerDestination)
    {
        // Vérifications
        if ($mediasContainer->getCategory() === Category::MEDIAS_CONTAINER_FOR_UPLOADS) {
            throw new CantCopyUploadsFolderException();
        }

        if ($mediasContainer->getId() === $mediasContainerDestination->getId()) {
            throw new CantCopyFolderInItSelfException();
        }

        $this->failIfAttemptToCopyFolderInItsSubFolder($mediasContainer, $mediasContainerDestination);

        // Récupération des fieldsValues
        $fieldsValues = $mediasContainer->getFieldsValues();
        $fieldsValuesDestination = $mediasContainerDestination->getFieldsValues();

        // Génération du nouveau nom + nouveau chemin
        $mediasContainer->setLabel(
            generateUniqueFileName($mediasContainer->getLabel(), getSiblingsNamesAndAlsoMine($mediasContainer))
        );

        $destinationPath = $fieldsValuesDestination['path'] . DS . $mediasContainer->getLabel();

        // Copie sur le disque
        if (!$this->files->copyDirectory(
            $this->pathPrefix . DS . $fieldsValues['path'],
            $this->pathPrefix . DS . $destinationPath
        )
        ) {
            throw new CopyFolderException();
        }

        // Copie dans la base de données
        $fieldsValues['path'] = $destinationPath;
        $mediasContainer->setFieldsValues($fieldsValues);
        $this->copyMediasContainerInDatabase($mediasContainer);
        $mediasContainer->setParentId($mediasContainerDestination->getId());

        $this->mediasContainerRepository->save($mediasContainer);

        return $mediasContainer;
    }

    /**
     * @param NinaObject $mediasContainer
     * @param NinaObject $mediasContainerDestination
     *
     * @throws \Nina\Exceptions\CantCopyFolderInItSelfException
     */
    private function failIfAttemptToCopyFolderInItsSubFolder(
        NinaObject $mediasContainer,
        NinaObject $mediasContainerDestination
    ) {
        $this->failIfAttemptToCopyOrMoveFolderInItsSubFolder($mediasContainer, $mediasContainerDestination);
    }

    /**
     * @param NinaObject $mediasContainer
     * @param NinaObject $mediasContainerDestination
     * @param bool       $copy
     *
     * @throws \Nina\Exceptions\CantCopyFolderInItSelfException
     * @throws \Nina\Exceptions\CantMoveFolderInItSelfException
     */
    private function failIfAttemptToCopyOrMoveFolderInItsSubFolder(
        NinaObject $mediasContainer,
        NinaObject $mediasContainerDestination,
        $copy = true
    ) {
        $mediasContainerDestinationParents = $this->mediasContainerRepository->findParentsOf(
            $mediasContainerDestination
        );

        array_pop($mediasContainerDestinationParents); // supprime le medias container racine

        foreach ($mediasContainerDestinationParents as $mediasContainerDestinationParent) {
            if ($mediasContainer->getId() === $mediasContainerDestinationParent->getId()) {
                if ($copy) {
                    throw new CantCopyFolderInItSelfException();
                }

                throw new CantMoveFolderInItSelfException();
            }
        }
    }

    /**
     * @param NinaObject $parentMediasContainer
     */
    private function copyMediasContainerInDatabase(NinaObject &$parentMediasContainer)
    {
        $this->copyOrMoveMediasContainerInDatabase($parentMediasContainer, true);
    }

    /**
     * {@inheritdoc}
     */
    public function moveMediaContainers(NinaObject $mediasContainer, NinaObject $mediasContainerDestination)
    {
        // Vérifications
        if ($mediasContainer->getCategory() === Category::MEDIAS_CONTAINER_FOR_UPLOADS) {
            throw new CantMoveUploadsFolderException();
        }

        if ($mediasContainer->getId() === $mediasContainerDestination->getId()) {
            throw new CantMoveFolderInItSelfException();
        }

        $this->failIfAttemptToMoveFolderInItsSubFolder($mediasContainer, $mediasContainerDestination);

        // Récupération des fieldsValues
        $fieldsValues = $mediasContainer->getFieldsValues();
        $fieldsValuesDestination = $mediasContainerDestination->getFieldsValues();

        // Génération du nouveau nom + nouveau chemin
        $mediasContainer->setLabel(
            generateUniqueFileName(
                $mediasContainer->getLabel(),
                getChildrenNames($mediasContainer, Category::MEDIAS_CONTAINER)
            )
        );
        $destinationPath = $fieldsValuesDestination['path'] . DS . $mediasContainer->getLabel();

        // Déplacement sur le disque
        if (!$this->files->moveDirectory(
            $this->pathPrefix . DS . $fieldsValues['path'],
            $this->pathPrefix . DS . $destinationPath
        )
        ) {
            throw new MoveFolderException();
        }

        // Déplacement dans la base de données
        $fieldsValues['path'] = $destinationPath;
        $mediasContainer->setFieldsValues($fieldsValues);
        $this->moveMediasContainerInDatabase($mediasContainer);
        $mediasContainer->setParentId($mediasContainerDestination->getId());

        $this->mediasContainerRepository->save($mediasContainer);

        return $mediasContainer;
    }

    /**
     * @param NinaObject $mediasContainer
     * @param NinaObject $mediasContainerDestination
     */
    private function failIfAttemptToMoveFolderInItsSubFolder(
        NinaObject $mediasContainer,
        NinaObject $mediasContainerDestination
    ) {
        $this->failIfAttemptToCopyOrMoveFolderInItsSubFolder($mediasContainer, $mediasContainerDestination, false);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMediasContainer(NinaObject $mediasContainer)
    {
        $deletedMediasContainersIds = [];
        $deletedMediasIds = [];

        // Vérifications
        if ($mediasContainer->getCategory() === Category::MEDIAS_CONTAINER_FOR_UPLOADS) {
            throw new CantDeleteUploadsFolderException();
        }

        $path = $mediasContainer->getFieldsValues()['path'];

        // Récupération des medias container à supprimer
        $mediasContainersToDelete = $this->mediasContainerRepository->findAllChildrenOf($mediasContainer);
        $mediasContainersToDelete[] = $mediasContainer;

        // Suppression des medias containers
        foreach ($mediasContainersToDelete as $mediasContainerToDelete) {
            $deletedMediasContainersIds[] = $mediasContainerToDelete->getId();

            // Suppression des médias
            $medias = $this->mediaRepository->findByParentId($mediasContainerToDelete->getId());

            foreach ($medias as $media) {
                $deletedMediasIds[] = $media->getId();
            }

            // Pas besoin de supprimer les médias du disque, puisqu'on va supprimer le dossier "père"
            $this->mediaRepository->deleteWhereParentId($mediasContainerToDelete->getId());
            $this->mediasContainerRepository->delete($mediasContainerToDelete);
        }

        // Suppression du medias container sur le disque
        if (!$this->filesystem->deleteDirectory($path)) {
            throw new DeleteFolderException();
        }

        return [
            'deleted_medias_ids' => $deletedMediasIds,
            'deleted_medias_containers_ids' => $deletedMediasContainersIds,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function renameMedia(NinaObject $media, $newName)
    {
        $mediasContainer = $this->mediasContainerRepository->findByIdOrFail($media->getParentId());

        // Vérifications
        if ($media->getLabel() === $newName) {
            return $media;
        }

        $this->checkIfFilenameIsValid($newName);
        $this->checkIfMediaNameIsUnique($newName, $media->getParentId());

        // Changement du nom et du path
        $fieldsValues = $media->getFieldsValues();
        $fieldsValuesParent = $mediasContainer->getFieldsValues();

        $path = $fieldsValues['path'];
        $fieldsValues['path'] = $fieldsValuesParent['path'] . DS . $newName;

        // Renommage sur le disque
        if (!$this->filesystem->move($path, $fieldsValues['path'])) {
            throw new CantRenameFileException();
        }

        // Renommage et sauvegarde dans la base de données
        $media->setLabel($newName);
        $media->setFieldsValues($fieldsValues);
        $this->mediaRepository->save($media);

        return $media;
    }

    /**
     * @param $name
     * @param $parentId
     *
     * @throws \Nina\Exceptions\FolderNameIsAlreadyTaken
     */
    private function checkIfMediaNameIsUnique($name, $parentId)
    {
        $media = $this->mediaRepository->findByParentIdAndLabel($parentId, $name);

        if ($media) {
            throw new FolderNameIsAlreadyTaken();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function copyMedia(NinaObject $media, NinaObject $mediasContainerDestination)
    {
        $path = $media->getFieldsValues()['path'];
        $parentPath = $mediasContainerDestination->getFieldsValues()['path'];

        // Copie dans la base de données
        $media = $this->mediaRepository->replicate($media);
        $media->setLabel(
            generateUniqueFileName(
                $media->getLabel(),
                getChildrenNames($mediasContainerDestination, Category::MEDIA)
            )
        );

        // Traitements si c'est une image
        if ($media->isImage()) {
            $media->cleanImage();
            $this->mediaRepository->save($media);
        }

        $fieldsValues = $media->getFieldsValues();
        $destinationPath = $parentPath . DS . $media->getLabel();
        $fieldsValues['path'] = $destinationPath;

        // Traitements si c'est une image fille
        if ($media->isDaughterImage()) {
            $motherImage = $this->mediaRepository->findByIdOrFail($fieldsValues['__mother_image_id']);
            $motherImage->attachDaughterImageToMotherImage($media);
            $this->mediaRepository->save($motherImage);
        }

        // Copie sur le disque
        if (!$this->filesystem->copy($path, $destinationPath)) {
            throw new CopyFileException();
        }

        $media->setFieldsValues($fieldsValues);
        $media->setParentId($mediasContainerDestination->getId());
        $this->mediaRepository->save($media);

        return $media;
    }

    /**
     * {@inheritdoc}
     */
    public function moveMedia(NinaObject $media, NinaObject $mediasContainerDestination)
    {
        // Vérifications
        if ($media->getParentId() === $mediasContainerDestination->getId()) {
            return $media;
        }

        $fieldsValues = $media->getFieldsValues();
        $fieldsValuesDestination = $mediasContainerDestination->getFieldsValues();

        // Génération d'un nouveau nom
        $media->setLabel(
            generateUniqueFileName(
                $media->getLabel(),
                getChildrenNames($mediasContainerDestination, Category::MEDIA)
            )
        );

        $pathDestination = $fieldsValuesDestination['path'] . DS . $media->getLabel();

        // Déplacement sur le disque
        if (!$this->filesystem->move($fieldsValues['path'], $pathDestination)) {
            throw new MoveFileException();
        }

        // Déplacement en base de données
        $fieldsValues['path'] = $pathDestination;
        $media->setParentId($mediasContainerDestination->getId());
        $media->setFieldsValues($fieldsValues);
        $this->mediaRepository->save($media);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMedia(NinaObject $media)
    {
        $fieldsValues = $media->getFieldsValues();

        // Suppression de la liaison avec les composants
        foreach (get($fieldsValues['__contained_by'], []) as $componentId => $componentFields) {
            $component = $this->componentRepository->findById($componentId);

            foreach ($componentFields as $componentField) {
                $component->detachMediaOfComponent($media, $componentField);
            }

            $this->componentRepository->save($component);
        }

        // Traitements si c'est une image fille
        if ($media->isDaughterImage()) {
            $motherImage = $this->mediaRepository->findByIdOrFail($fieldsValues['__mother_image_id']);
            $motherImage->detachDaughterImageFromMotherImage($media);
            $this->mediaRepository->save($motherImage);
        }

        // Traitements si c'est une image mère
        if ($media->isMotherImage()) {
            foreach (get($fieldsValues['__daughters_images_id'], []) as $daughterImageId) {
                $daughterImage = $this->mediaRepository->findByIdOrFail($daughterImageId);
                $this->deleteMedia($daughterImage);
            }
        }

        // Suppression du média en base de données
        $this->mediaRepository->delete($media);

        // Suppression du média sur le disque
        if (!$this->filesystem->delete($fieldsValues['path'])) {
            throw new DeleteFileException();
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function cropImage(NinaObject $image, array $manipulations)
    {
        $mediathequeImageHandler = MediathequeImageHandler::handle($image);
        $mediathequeImageHandler->manipulate($manipulations);

        return [
            'daughter_image' => $mediathequeImageHandler->getDaughterImage(),
            'mother_image' => $mediathequeImageHandler->getMotherImage(),
        ];
    }
}
