<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Services;

use App\Category;
use Illuminate\Container\Container;
use Illuminate\Http\UploadedFile;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Exceptions\FileIsNotAllowedToBeUploadedException;
use Nina\Exceptions\UploadFileException;
use Nina\Exceptions\ValidationException;

class NinaUploaderService
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var MediaRepository
     */
    private $mediaRepository;

    /**
     * @var \Symfony\Component\HttpFoundation\File\File
     */
    private $uploadedFile;

    /**
     * @var null|NinaObject
     */
    private $media;

    /**
     * @var array
     */
    private $filesThatAreNotAllowedToBeUploaded = [
        '.htaccess',
        '.htpasswd',
        '.htgroup',
    ];

    /**
     *
     * @param \Illuminate\Http\UploadedFile $file
     *
     * @return \Nina\Services\NinaUploaderService
     */
    public static function open(UploadedFile $file)
    {
        return new self($file);
    }

    /**
     * @param string $rules
     *
     * @throws \Nina\NinaException
     */
    public function validate($rules)
    {
        /** @var \Kocal\Validator\Validator $validator */
        $validator = $this->container->make('validator');
        $validator->setRules(['file' => $rules]);
        $validator->validate(['file' => $this->uploadedFile], [], ['file' => 'fichier']);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * @param NinaObject $destination
     *
     * @throws FileIsNotAllowedToBeUploadedException
     * @throws UploadFileException
     */
    public function upload(NinaObject $destination)
    {
        if (in_array($this->uploadedFile->getClientOriginalName(), $this->filesThatAreNotAllowedToBeUploaded)) {
            throw new FileIsNotAllowedToBeUploadedException($this->uploadedFile->getClientOriginalName());
        }

        // On génère un nom unique
        $mediaName = generateUniqueFileName(
            $this->uploadedFile->getClientOriginalName(),
            getChildrenNames($destination, Category::MEDIA)
        );

        // Enregistrement du fichier sur le disque
        $destinationPath = $destination->getFieldsValues()['path'];
        $uploaded = $this->uploadedFile->storePubliclyAs($destinationPath, $mediaName, ['disk' => 'mediatheque_nina']);

        if ($uploaded === false) {
            throw new UploadFileException($this->uploadedFile);
        }

        // Création du média en base de données
        $this->media = $this->mediaRepository->initialize(
            $destination->getRootId(),
            $destination->getId(),
            $mediaName
        );

        $fieldsValues = $this->media->getFieldsValues();
        $fieldsValues['path'] = $destinationPath . DS . $this->media->getLabel();
        $fieldsValues['mimetype'] = $this->uploadedFile->getMimeType();

        $this->media->setFieldsValues($fieldsValues);

        $this->mediaRepository->save($this->media);
    }

    /**
     * @return array
     */
    public function state()
    {
        return [
            'media' => extractNinaObjectData($this->media),
            'original_filename' => $this->uploadedFile->getClientOriginalName(),
        ];
    }

    /**
     * NinaUploader constructor.
     *
     * @param \Illuminate\Http\UploadedFile $uploadedFile
     */
    private function __construct(UploadedFile $uploadedFile)
    {
        $this->container = Container::getInstance();
        $this->mediaRepository = $this->container->make('medias_repository');
        $this->uploadedFile = $uploadedFile;
    }
}
