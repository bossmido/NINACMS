<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: hugo
 * Date: 14/04/17
 * Time: 09:55
 */

namespace Nina\Field;

class FieldLinks extends BaseField
{
    public $min = -1;

    public $max = -1;

    public $infinite = true;

    public function __construct($id, $label = null, $options = [])
    {
        if (isset($options['min'])) {
            $this->min = $options['min'];
        }

        if (isset($options['max'])) {
            $this->max = $options['max'];
        }

        $this->infinite = $this->max === -1;

        if ($this->infinite) {
            assert($this->min >= $this->max, "FieldsLink, min doit être inférieur à max.");
        }

        parent::__construct($id, $label);
    }
}
