<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: hugo
 * Date: 11/04/17
 * Time: 08:43
 */

namespace Nina\Field;

class FieldLink extends BaseField
{
    public $fieldForLabel;

    public $fieldForOpenInNewTab;

    public function __construct($id, $label = null, array $attributes = [])
    {
        if (! isset($attributes['placeholder'])) {
            $attributes['placeholder'] = 'https://';
        }

        parent::__construct($id, $label, $attributes);

        $this->fieldForLabel = new FieldText($id . '_label', $label . ' (label)');
        $this->fieldForOpenInNewTab =
            new FieldCheckbox($id . '_target_blank', 'Ouvrir ce lien dans une nouvelle fenêtre ?');
    }
}
