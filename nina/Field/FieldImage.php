<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Field;

use Dflydev\ApacheMimeTypes\PhpRepository;

class FieldImage extends BaseField
{
    /**
     * @var int|mixed
     */
    public $max = -1;

    /**
     * @var bool
     */
    public $infinite = true;

    /**
     * @var bool
     */
    public $multiple = true;

    /**
     * @var array
     */
    public $allowedMimetypes;

    public function __construct($id, $label = null, $options = [])
    {
        if (isset($options['max'])) {
            $this->max = $options['max'];
        }

        $this->infinite = $this->max === -1;
        $this->multiple = $this->infinite || ! $this->infinite && $this->max > 1;

        if (! $this->infinite) {
            assert($this->max > 0, "FieldImage, max doit être supérieur à 0.");
        }

        $repository = new PhpRepository();
        $allowedExtensions = formalizeAllowedExtensions(['image']);
        $this->allowedMimetypes = array_map(function ($type) use ($repository) {
            return $repository->findType($type);
        }, $allowedExtensions['without_dot']);

        parent::__construct($id, $label);
    }
}
