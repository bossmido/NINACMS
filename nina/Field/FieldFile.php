<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: hugo
 * Date: 11/04/17
 * Time: 08:43
 */

namespace Nina\Field;

use Dflydev\ApacheMimeTypes\PhpRepository;

class FieldFile extends BaseField
{
    /**
     * @var FieldHidden
     */
    public $hiddenField;

    /**
     * @var array
     */
    public $allowedExtensions;

    /**
     * @var array
     */
    public $allowedMimetypes;

    /**
     * @var int
     */
    public $maxSize;

    /**
     * FieldFile constructor.
     * Si $maxSize vaut -1, alors on récupère la valeur maximale pour l'envoie de fichier
     * grâce à la fonction getMaximumFileUploadSize().
     *
     * @param              $id
     * @param null         $label
     * @param array|string $allowedExtensions Extensions autorisées (c'est cependant le type mime correspondant à
     *                                        l'extensions qui est vérifié).
     *                                        Si '*' ou null, on accepte toutes les extensions.
     * @param int          $maxSize           Taille maximale en Kilo-octets (1024 ko = 1 Mo)
     */
    public function __construct($id, $label = null, $allowedExtensions = '*', $maxSize = -1)
    {
        $attributes = [];

        // Extensions autorisées

        if (is_array($allowedExtensions)) {
            $repository = new PhpRepository();

            $this->allowedExtensions = formalizeAllowedExtensions($allowedExtensions);
            $this->allowedMimetypes = array_map(
                function ($type) use ($repository) {
                    return $repository->findType($type);
                },
                $this->allowedExtensions['without_dot']
            );

            $attributes['accept'] = implode(', ', $this->allowedExtensions['with_dot']);
        } elseif ($allowedExtensions === '*' || $allowedExtensions === null) {
            $this->allowedExtensions['with_dot'] = ['*'];
            $this->allowedMimetypes = ['*'];
        }

        $this->maxSize = ($maxSize === -1 ? getMaximumFileUploadSize() : $maxSize);

        if (! empty($label)) {
            $label .= sprintf(' (Taille maximale : %.2f Mo)', $this->maxSize / 1024);

            if (empty($attributes['accept'])) {
                $label .= ' (Tous types de fichiers acceptés)';
            } else {
                $label .= ' (Fichiers acceptés : ' . $attributes['accept'] . ')';
            }
        }

        parent::__construct($id, $label, $attributes);
    }
}
