<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Field;

use Nina\Traits\PropertiesTrait;

/**
 * Class BaseField
 *
 * @package Nina\Field
 */
class BaseField
{
    use PropertiesTrait;

    /**
     * @var string
     */
    public $id;

    /**
     * @var mixed|null
     */
    public $value;

    /**
     * @var mixed|null
     */
    public $defaultValue;

    /**
     * @var null|string
     */
    public $label;

    /**
     * @var array
     */
    public $attributes;

    /**
     * @var string
     */
    public $class;

    /**
     * @var string
     */
    public $template;


    /**
     * BaseField constructor.
     *
     * @param       $id
     * @param null  $label
     * @param array $attributes
     */
    public function __construct($id, $label = null, $attributes = [])
    {
        $this->id = $id;
        $this->label = is_null($label) ? camel_case($id) : $label;
        $this->attributes = $attributes;

        $this->class = (new \ReflectionClass($this))->getShortName();
        $this->template = "fields/{$this->class}.html.twig";
    }

    /**
     * {@inheritdoc}
     */
    public function getIdForProperty()
    {
        return $this->id;
    }
}
