<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Field;

class FieldSeparator extends BaseField
{
    /**
     * FieldSeparator constructor.
     */
    public function __construct()
    {
        parent::__construct(null);
    }
}
