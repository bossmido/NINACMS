<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */
namespace Nina\Field;

/**
 * Class FieldCheckbox
 *
 * @package Nina\Field
 */
class FieldCheckbox extends BaseField
{
    /**
     * @var string|null
     */
    public $bigLabel = null;

    /**
     * FieldCheckbox constructor.
     *
     * @param       $id
     * @param null  $label
     * @param array $options
     */
    public function __construct($id, $label = null, array $options = [])
    {
        parent::__construct($id, $label);

        if (isset($options['bigLabel'])) {
            $this->bigLabel = $options['bigLabel'];
        }
    }
}
