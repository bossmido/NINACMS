<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: hugo
 * Date: 14/10/16
 * Time: 16:44
 */

namespace Nina\Field;

class FieldList extends BaseField
{
    /**
     * @var string|null
     */
    public $placeholder = 'Choisissez une option';

    /**
     * @var array|null
     */
    public $list = [];

    /**
     * FieldList constructor.
     *
     * @param       $id
     * @param null  $label
     * @param array $options
     */
    public function __construct($id, $label = null, array $options = [])
    {
        parent::__construct($id, $label);

        $this->defaultValue = get($options['defaultValue']);
        $this->placeholder = get($options['placeholder'], $this->placeholder);
        $this->list = get($options['list'], $this->list);
    }
}
