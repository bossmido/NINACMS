<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: hugo
 * Date: 11/04/17
 * Time: 08:44
 */

namespace Nina\Field;

class FieldCheckboxes extends BaseField
{
    /**
     * @var FieldCheckbox[]
     */
    public $checkboxes = [];

    /**
     * FieldCheckboxes constructor.
     *
     * @param       $id
     * @param null  $label
     * @param array $checkboxDefinitions
     */
    public function __construct($id, $label, array $checkboxDefinitions)
    {
        parent::__construct($id, $label, []);

        foreach ($checkboxDefinitions as $checkboxDefinition) {
            $checkboxId = $id . '_' . $checkboxDefinition['id'];
            $checkboxLabel = $checkboxDefinition['label'];

            $this->checkboxes[] = new FieldCheckbox($checkboxId, $checkboxLabel);
        }
    }
}
