<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Interface MediathequeControllerInterface
 *
 * @package Nina\Controllers\Mediatheque
 */
interface MediathequeController
{
    public function getName();

    public function getLabel();

    public function isEnabled();

    public function getAdditionalProperties(array $array);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createMediasContainer(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function renameMediasContainer(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function readMediasContainer(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyMediaContainers(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function moveMediaContainers(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteMediasContainer(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function renameMedia(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyMedia(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function moveMedia(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteMedia(Request $request);

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function cropImage(Request $request);
}
