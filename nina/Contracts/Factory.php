<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts;

/**
 * Interface Factory
 *
 * @package Nina\Contracts
 */
interface Factory
{
    /**
     * @param array $data
     * @return mixed
     */
    public static function make(array $data);
}
