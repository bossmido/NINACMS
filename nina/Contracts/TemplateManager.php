<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts;

use Nina\Context;

/**
 * Interface TemplateManager
 *
 * @package Nina\Contracts
 */
interface TemplateManager
{
    /**
     * TemplateManager constructor.
     *
     * @param Context $context
     */
    public function __construct(Context $context);

    /**
     * @param       $template
     * @param array $data
     * @param array $param
     * @return mixed
     */
    public function process($template, array $data = [], array $param = []);
}
