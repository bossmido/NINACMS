<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Entities;

use Nina\Component\BaseComponent;
use Nina\Contracts\Arrayable;

/**
 * Interface NinaObject
 *
 * @package App\Entities
 */
interface NinaObject extends Arrayable
{
    /**
     * Détache le Media au lié au Component actuel via le Field `$fieldName`.
     *
     * @param self   $media
     * @param string $fieldName
     *
     * @return mixed
     */
    public function detachMediaOfComponent(NinaObject $media, $fieldName);

    /**
     * Attache le Media au Component actuel via le Field `$fieldName`
     *
     * @param self   $component
     * @param string $fieldName
     *
     * @return mixed
     */
    public function attachComponentToMedia(NinaObject $component, $fieldName);

    /**
     * Détache le Component au Media actuel.
     *
     * @param self   $component
     * @param string $fieldName
     *
     * @return mixed
     */
    public function detachComponentOfMedia(NinaObject $component, $fieldName);

    /**
     * Est-ce que cette instance de NinaObject est une image ?
     *
     * @return boolean
     */
    public function isImage();

    /**
     * Est-ce que cette instance de NinaObject est une image mère ?
     *
     * @return boolean
     */
    public function isMotherImage();

    /**
     * Est-ce que cette instance de NinaObject est une image fille ?
     *
     * @return boolean
     */
    public function isDaughterImage();

    /**
     * Enlève les caractéristiques spécifiques d'une image mère, à une image fille.
     * (Une image fille est un clone d'une image, qui devient ensuite sa mère).
     *
     * @return mixed
     */
    public function cleanImage();

    /**
     * Attache une image mère à une image qui sera sa fille.
     *
     * @param NinaObject $motherImage
     *
     * @return mixed
     */
    public function attachMotherImageToDaughterImage(NinaObject $motherImage);

    /**
     * Attache une image fille à une image qui sera sa mère.
     *
     * @param NinaObject $daughterImage
     *
     * @return mixed
     */
    public function attachDaughterImageToMotherImage(NinaObject $daughterImage);

    /**
     * Détache une image fille de sa mère.
     *
     * @param NinaObject $daughterImage
     *
     * @return mixed
     */
    public function detachDaughterImageFromMotherImage(NinaObject $daughterImage);

    /**
     * Retourne vrai ou faux si l'image n'est contenue nul part.
     *
     * @return boolean
     */
    public function isContainedNowhere();

    /**
     * Assigne le Component lié à cette instance.
     *
     * @param BaseComponent $component
     */
    public function setComponent(BaseComponent $component);

    /**
     * Retourne le Component lié à cette instance.
     *
     * @return BaseComponent
     */
    public function getComponent();

    /**
     * Retourne l'id.
     *
     * @return int
     */
    public function getId();

    /**
     * Assigne un nouvel identifiant.
     *
     * @param int $id
     */
    public function setId($id);

    /**
     * Retourne le root_id.
     *
     * @return int
     */
    public function getRootId();

    /**
     * Assigne un nouveau root_id.
     *
     * @param int $root_id
     */
    public function setRootId($root_id);

    /**
     * Retourne le parent_id.
     *
     * @return int
     */
    public function getParentId();

    /**
     * Assigne un nouveau parent_id;
     *
     * @param int $parent_id
     */
    public function setParentId($parent_id);

    /**
     * Retourne l'order.
     *
     * @return int
     */
    public function getOrder();

    /**
     * Change l'order.
     *
     * @param int $order
     */
    public function setOrder($order);

    /**
     * Retourne la Category.
     *
     * @return string
     */
    public function getCategory();

    /**
     * Change la Category.
     *
     * @param string $category
     */
    public function setCategory($category);

    /**
     * Retourne le label.
     *
     * @return string
     */
    public function getLabel();

    /**
     * Change le label.
     *
     * @param string $label
     */
    public function setLabel($label);

    /**
     * Retourne la classe du Component.
     *
     * @return string
     */
    public function getClass();

    /**
     * Change la classe du Component.
     *
     * @param string $class
     */
    public function setClass($class);

    /**
     * Retourne les valeurs des champs.
     *
     * @return array
     */
    public function getFieldsValues();

    /**
     * Change les valeurs des champs.
     *
     * @param array $fieldsValues
     */
    public function setFieldsValues($fieldsValues);

    /**
     * Retourne les styles CSS.
     *
     * @return array
     */
    public function getStyles();

    /**
     * Change les styles CSS.
     *
     * @param array $styles
     */
    public function setStyles($styles);

    /**
     * Retourne les droits (sous la forme des droits Unix).
     *
     * @return mixed
     */
    public function getRights();

    /**
     * Change les droits.
     *
     * @param $rights
     *
     * @return mixed
     */
    public function setRights($rights);

    /**
     * Retourne l'user_id.
     *
     * @return mixed
     */
    public function getUserId();

    /**
     * Change l'user_id.
     *
     * @param $user_id
     *
     * @return mixed
     */
    public function setUserId($user_id);

    /**
     * Retourne le group_id.
     *
     * @return mixed
     */
    public function getGroupId();

    /**
     * Change le group_id.
     *
     * @param $group_id
     *
     * @return mixed
     */
    public function setGroupId($group_id);
}
