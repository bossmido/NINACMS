<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts;

/**
 * Interface Arrayable
 *
 * @package Nina\Contracts
 */
interface Arrayable
{
    /**
     * @return array
     */
    public function toArray();
}
