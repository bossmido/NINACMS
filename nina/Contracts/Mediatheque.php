<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts;

use Nina\Contracts\Entities\NinaObject;

/**
 * Interface Mediatheque
 *
 * @package Nina\Contracts
 */
interface Mediatheque
{
    /**
     * Créer un MediasContainer.
     *
     * @param mixed  $parentId
     * @param string $name
     *
     * @return mixed
     */
    public function createMediasContainer($parentId, $name);

    /**
     * Re-nomme un MediasContainer.
     *
     * @param NinaObject $mediasContainer
     * @param string     $newName
     *
     * @return mixed
     */
    public function renameMediasContainer(NinaObject $mediasContainer, $newName);

    /**
     * Lis le contenu d'un MediasContainer.
     *
     * @param NinaObject $mediasContainer
     *
     * @return mixed
     */
    public function readMediasContainer(NinaObject $mediasContainer);

    /**
     * Copie un MediasContainer dans un autre MediasContainer.
     *
     * @param NinaObject $mediasContainer
     * @param NinaObject $mediasContainerDestination
     *
     * @return mixed
     */
    public function copyMediaContainers(NinaObject $mediasContainer, NinaObject $mediasContainerDestination);

    /**
     * Déplace un MediasContainer dans un autre MediasContainer.
     *
     * @param NinaObject $mediasContainer
     * @param NinaObject $mediasContainerDestination
     *
     * @return NinaObject
     */
    public function moveMediaContainers(NinaObject $mediasContainer, NinaObject $mediasContainerDestination);

    /**
     * Supprime un MediasContainer.
     *
     * @param NinaObject $mediasContainer
     *
     * @return NinaObject
     */
    public function deleteMediasContainer(NinaObject $mediasContainer);

    /**
     * Re-nomme un Media.
     *
     * @param NinaObject $media
     * @param string     $newName
     *
     * @return NinaObject
     */
    public function renameMedia(NinaObject $media, $newName);

    /**
     * Copie un Media dans un MediasContainer.
     *
     * @param NinaObject $media
     * @param NinaObject $mediasContainerDestination
     *
     * @return mixed
     */
    public function copyMedia(NinaObject $media, NinaObject $mediasContainerDestination);

    /**
     * Déplace un Media dans un MediasContainer.
     *
     * @param NinaObject $media
     * @param NinaObject $mediasContainerDestination
     *
     * @return mixed
     */
    public function moveMedia(NinaObject $media, NinaObject $mediasContainerDestination);

    /**
     * Supprime un Media.
     *
     * @param NinaObject $media
     *
     * @return mixed
     */
    public function deleteMedia(NinaObject $media);

    /**
     * Effectue des manipulations graphique sur une image.
     *
     * @param NinaObject $image
     * @param array      $manipulations
     *
     * @return mixed
     */
    public function cropImage(NinaObject $image, array $manipulations);
}
