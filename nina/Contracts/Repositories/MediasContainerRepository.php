<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Repositories;

use Nina\Contracts\Entities\NinaObject;

interface MediasContainerRepository
{
    /**
     * @param $libraryId
     * @param $parentId
     * @param $label
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function create($libraryId, $parentId, $label);

    /**
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     * @param array                               $expect
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function replicate(NinaObject $ninaObject, array $expect = []);

    /**
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return mixed
     */
    public function save(NinaObject $ninaObject);

    /**
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return boolean
     */
    public function delete(NinaObject $ninaObject);

    /**
     * @param $id
     *
     * @return NinaObject
     */
    public function findById($id);

    /**
     * @param $id
     *
     * @return \Nina\Contracts\Entities\NinaObject
     * @throws \Nina\Exceptions\FolderNotFoundException
     */
    public function findByIdOrFail($id);

    /**
     * @param $libraryId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findByLibraryId($libraryId);

    /**
     * @param $libraryId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     * @throws \Nina\Exceptions\FolderNotFoundAtLibraryRootException
     */
    public function findByLibraryIdOrFail($libraryId);

    /**
     * @param $rootId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findForUploadsByRootId($rootId);

    /**
     * @param $rootId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     * @throws \Nina\Exceptions\UploadFolderNotFoundException
     */
    public function findForUploadsByRootIdOrFail($rootId);

    /**
     * @param $parentId
     *
     * @return \Nina\Contracts\Entities\NinaObject[]
     */
    public function findByParentId($parentId);

    /**
     * @param $parentId
     * @param $label
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findByParentIdAndLabel($parentId, $label);

    /**
     * @param NinaObject $mediasContainer
     *
     * @return NinaObject[]
     */
    public function findParentsOf(NinaObject $mediasContainer);

    /**
     * @param NinaObject $mediasContainer
     *
     * @return NinaObject[]
     */
    public function findChildrenOf($mediasContainer);

    /**
     * @param NinaObject $mediasContainer
     *
     * @return NinaObject[]
     */
    public function findAllChildrenOf($mediasContainer);
}
