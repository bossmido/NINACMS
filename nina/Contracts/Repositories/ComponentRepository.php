<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Repositories;

use Nina\Contracts\Entities\NinaObject;
use Nina\Exceptions\ComponentNotFoundException;

interface ComponentRepository
{
    /**
     * @param $rootId
     * @param $parentId
     * @param $class
     *
     * @return NinaObject
     */
    public function create($rootId, $parentId, $class);

    /**
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return mixed
     */
    public function save(NinaObject $ninaObject);

    /**
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return mixed
     */
    public function delete(NinaObject $ninaObject);

    /**
     * Clone le composant en un nouveau, sans les attributs définis dans `$except`.
     *
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     * @param array|null                          $except
     *
     * @return mixed
     */
    public function replicate(NinaObject $ninaObject, array $except = null);

    /**
     * @param $id
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findById($id);

    /**
     * @param $id
     *
     * @return NinaObject
     * @throws ComponentNotFoundException
     */
    public function findByIdOrFail($id);

    /**
     * @param NinaObject $component
     * @param            $order
     * @return NinaObject[]
     */
    public function findNextSiblings(NinaObject $component, $order);

    /**
     * @param $rootId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function getCollection($rootId);

    /**
     * @param $parentId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function getDirectChildren($parentId);
}
