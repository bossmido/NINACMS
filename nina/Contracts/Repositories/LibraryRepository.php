<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Repositories;

use Nina\Contracts\Entities\NinaObject;

interface LibraryRepository
{
    /**
     * @param mixed $id
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findById($id);

    /**
     * @param mixed $rootId
     *
     * @return NinaObject
     */
    public function findByRootId($rootId);

    /**
     * @param mixed $rootId
     *
     * @return \Nina\Contracts\Entities\NinaObject
     * @throws \Nina\Exceptions\LibraryNotFoundException
     */
    public function findByRootIdOrFail($rootId);
}
