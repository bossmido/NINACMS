<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Contracts\Repositories;

use Nina\Contracts\Entities\NinaObject;

interface MediaRepository
{
    /**
     * Initialise un Media.
     *
     * @param mixed $libraryId
     * @param mixed $parentId
     * @param mixed $label
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function initialize($libraryId, $parentId, $label);

    /**
     * Clone le Media en un nouveau Media, en omettant les attributs définis dans `$except`.
     *
     * @param NinaObject $ninaObject
     * @param array|null $except
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function replicate(NinaObject $ninaObject, array $except = null);

    /**
     * Sauvegarde le Media en mémoire.
     *
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return mixed
     */
    public function save(NinaObject $ninaObject);

    /**
     * Supprime le Media.
     *
     * @param \Nina\Contracts\Entities\NinaObject $ninaObject
     *
     * @return boolean
     */
    public function delete(NinaObject $ninaObject);

    /**
     * Supprime les Media où leur `parent_id` est égal à `$parentId`.
     *
     * @param $parentId
     *
     * @return boolean
     */
    public function deleteWhereParentId($parentId);

    /**
     * Récupère le Media ayant pour id `$id`.
     *
     * @param $id
     *
     * @return \Nina\Contracts\Entities\NinaObject
     */
    public function findById($id);

    /**
     * Récupère le Media ayant pour id `$id`. Si aucun résultat, lance une FileNotFoundException.
     *
     * @param $id
     *
     * @return \Nina\Contracts\Entities\NinaObject
     * @throws \Nina\Exceptions\FileNotFoundException
     */
    public function findByIdOrFail($id);

    /**
     * Récupère des Media en fonction de leur `parent_id`.
     *
     * @param $parentId
     *
     * @return \Nina\Contracts\Entities\NinaObject[]
     */
    public function findByParentId($parentId);

    /**
     * Récupère des Media en fonction de leur `parent_id` et `label`.
     *
     * @param mixed $parentId
     * @param mixed $label
     *
     * @return \Nina\Contracts\Entities\NinaObject[]
     */
    public function findByParentIdAndLabel($parentId, $label);
}
