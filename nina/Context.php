<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * ninaCMS context object.
 *
 * @author Philippe Boireaud <philippe.boireaud@gmail.com>
 */
namespace Nina;

class Context
{
    private $context;

    private $token;

    private $invalid_token;

    private $no_cookie;

    private $user_id;

    private $user_groups = [];

    private $is_root;

    private $edit_mode;

    private $locale;

    private $vars = [];

    private $messages = [];

    public function __construct()
    {
        // TODO: faudrait vraiment retravailler sur ça..
        //        if (!$this->readCookie()) {
        $this->generateCookie();
        //        }

        $this->saveCookie();
    }

    public function generateCookie()
    {
        $this->setLocale(NINA_LOCALE);
        $this->setUser('1');
        $this->setGroups(['test']);
        //        $this->setRoot(false);
        $this->setEditMode(true);
        $this->token = null;
    }

    public function setUser($user_id)
    {
        $this->user_id = $user_id;
    }

    public function setGroups($groups)
    {
        $this->user_groups = (array)$groups;
    }

    public function setEditMode($enabled)
    {
        $this->edit_mode = $enabled;
    }

    public function saveCookie()
    {
        $flow = [
            'user_id' => $this->user_id,
            'user_groups' => $this->user_groups,
            'is_root' => $this->is_root,
            'edit_mode' => $this->edit_mode,
            'lng' => $this->locale,
            'vars' => $this->vars,
            'token' => self::getToken(),
            'expire' => time() + NINA_COOKIE_TIMEOUT,
        ];

        return setcookie(
            'NinaCMS_' . NINA_APP_ID,
            json_encode($flow),
            time() + NINA_COOKIE_TIMEOUT,
            NINA_COOKIE_PATH,
            null/*$_SERVER['HTTP_HOST']*/,
            0
        );
    }

    private function getToken()
    {
        if (! $this->token) {
            $this->token = sha1(
                uniqid()
                . $this->user_id
                . implode('', $this->user_groups)
                . $this->is_root . NINA_PATH . NINA_APP_ID
            );
        }

        return $this->token;
    }

    public function readCookie()
    {
        $cookie = isset($_COOKIE['NinaCMS_' . NINA_APP_ID]) ? $_COOKIE['NinaCMS_' . NINA_APP_ID] : null;

        $flow = json_decode($cookie, true);
        if ($flow == null || $flow['expire'] < time()) {
            $this->no_cookie = true;

            return false;
        }
        $this->user_id = $flow['user_id'];
        $this->user_groups = (array)$flow['user_groups'];
        $this->is_root = $flow['is_root'];
        $this->edit_mode = $flow['edit_mode'];
        $this->locale = $flow['locale'];
        $this->vars = (array)$flow['vars'];
        if ($flow['token'] != self::getToken()) {
            $this->invalid_token = true;
        }
    }

    public function getContext()
    {
        return $this->context;
    }

    public function isCookie()
    {
        return ! $this->no_cookie;
    }

    public function isValidToken()
    {
        return ! $this->invalid_token;
    }

    public function getUser()
    {
        return $this->user_id;
    }

    public function getGroups()
    {
        return (array)$this->user_groups;
    }

    public function addGroup($group)
    {
        if (! in_array($group, $this->user_groups)) {
            $this->user_groups[] = $group;
        }
    }

    public function removeGroup($group)
    {
        unset($this->user_groups[$group]);
    }

    public function setRoot($root)
    {
        $this->is_root = $root;
    }

    public function isRoot()
    {
        return $this->is_root;
    }

    public function enableEditMode()
    {
        $this->setEditMode(true);
    }

    public function disableEditMode()
    {
        $this->setEditMode(false);
    }

    public function isEditModeEnabled()
    {
        return $this->edit_mode;
    }

    public function getLocale()
    {
        return ($this->locale ? $this->locale : NINA_LOCALE);
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    public function setVar($k, $v)
    {
        $this->vars[$k] = $v;
    }

    public function getVar($k)
    {
        return $this->vars[$k];
    }
}
