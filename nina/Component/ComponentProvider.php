<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Component;

use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Exceptions\ComponentAlreadyRegisteredException;
use Nina\Exceptions\ComponentNotFoundInProviderException;

/**
 * Class ComponentProvider
 *
 * @package Nina\Component
 */
class ComponentProvider
{
    /**
     * @var array
     */
    private $components = [];

    /**
     * @var ComponentRepository
     */
    private $componentsRepository;

    /**
     * ComponentProvider constructor.
     *
     * @param ComponentRepository $componentRepository
     */
    public function __construct(ComponentRepository $componentRepository)
    {
        $this->componentsRepository = $componentRepository;
        $this->findAndRegisterComponents();
    }

    /**
     * Cherche et enregistre tous les composants situés dans NINA_COMPONENTS_PATH.
     */
    private function findAndRegisterComponents()
    {
        $componentsFiles = NINA_COMPONENTS_PATH . DS . '**' . DS . '*Component.php';

        foreach (glob($componentsFiles) as $componentFile) {
            require_once $componentFile;
            $componentFqcn = str_replace(NINA_APP_PATH, 'App', $componentFile);
            $componentFqcn = str_replace('.php', '', $componentFqcn);
            $componentFqcn = str_replace('/', '\\', $componentFqcn);
            $this->register($componentFqcn);
        }
    }

    /**
     * Enregistre le fqcn du composant in-memory.
     *
     * @param string|array $componentFqcn Fully Qualified Class Name du composant.
     *
     * @throws ComponentAlreadyRegisteredException
     */
    private function register($componentFqcn)
    {
        $namespaces = explode('\\', $componentFqcn);
        $componentName = end($namespaces);

        if (isset($this->components[$componentName])) {
            throw new ComponentAlreadyRegisteredException($componentName);
        }

        $this->components[$componentName] = $componentFqcn;
    }

    /**
     * Retourne tous les composants stockés in-memory.
     *
     * @return array
     */
    public function all()
    {
        return $this->components;
    }

    /**
     * Récupère le fqcn du composant stocké in-memory.
     *
     * @param $componentName
     *
     * @return \Nina\Component\BaseComponent
     * @throws ComponentNotFoundInProviderException
     */
    public function get($componentName)
    {
        if (! isset($this->components[$componentName])) {
            throw new ComponentNotFoundInProviderException($componentName);
        }

        return $this->components[$componentName];
    }

    /**
     * Récupère un composant en BDD.
     *
     * @param int $id
     *
     * @return BaseComponent
     * @throws \Exception
     */
    public function loadFromDatabase($id)
    {
        $componentModel = $this->componentsRepository->findByIdOrFail($id);
        $component = $this->get($componentModel->getClass() . 'Component');

        return new $component($componentModel, false);
    }
}
