<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Component;

use App\ComponentGroup;
use Illuminate\Container\Container;
use Nina\Context;
use Nina\Contracts\Entities\NinaObject;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Field\BaseField;
use Nina\Traits\PropertiesTrait;
use Nina\Traits\SetupPropertiesTrait;
use stdClass;

/**
 * Class BaseComponent
 *
 * @package Nina\Component
 */
class BaseComponent
{
    use PropertiesTrait;
    use SetupPropertiesTrait;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var ComponentRepository
     */
    protected $componentRepository;

    /**
     * @var NinaObject
     */
    protected $model;

    /**
     * @var BaseComponent[]
     */
    protected $children = [];

    /**
     * Nom de classe du composant, avec le suffixe 'Component'.
     */
    protected $class;

    /**
     * Nom de classe du composant, sans le suffixe 'Component'.
     */
    protected $shortClass;

    /**
     * @var BaseField[][]
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $fieldsRules = [];

    /**
     * Règles de validation pour les champs FieldFile.
     *
     * @var array
     */
    protected $fieldsFileRules = [];

    /**
     * @var array State que le component peut utiliser pour s'isoler de l'entité "Component" de la base de données.
     */
    protected $state = [];

    /**
     * @var array Les components pouvant contenir ce composant.
     */
    protected $contains = [];

    /**
     * @var string Chemin vers une icône.
     */
    protected $icon;

    /**
     * @var string Nom "humanisé".
     */
    protected $name;

    /**
     * @var string|null Chemin du template à utiliser pour le visiteur.
     */
    protected $template = null;

    /**
     * @var array|null Fichier(s) CSS à utiliser pour le visiteur.
     */
    protected $styles = [];

    /**
     * @var array|null Fichier(s) JS à utiliser pour le visiteur.
     */
    protected $scripts = [];

    /**
     * @var array|null Fichier(s) CSS à utiliser pour l'administrateur.
     */
    protected $adminStyles = [];

    /**
     * @var array|null Fichier(s) JS à utiliser pour l'administrateur.
     */
    protected $adminScripts = [];

    protected $absolutePath;

    protected $relativePath;

    /**
     * @var string Le groupe auquel le composant appartient
     */
    protected $group = ComponentGroup::MISCELLANEOUS;

    /**
     * BaseComponent constructor.
     *
     * @param NinaObject $componentModel
     * @param bool       $afterConstruct
     */
    public function __construct(NinaObject $componentModel = null, $afterConstruct = true)
    {
        $this->container = Container::getInstance();
        $this->componentRepository = $this->container->make('component_repository');
        $this->model = $componentModel;

        if ($this->model !== null) {
            $this->addCommonProperties();
            $this->prepareProperties();
            $this->model->setComponent($this);
        }

        $this->reflect();

        if ($afterConstruct) {
            $this->afterConstruct();
        }
    }

    public function afterConstruct()
    {
    }

    private function reflect()
    {
        $reflector = new \ReflectionClass($this);

        $this->absolutePath = dirname($reflector->getFileName());
        $this->relativePath = str_replace(NINA_ROOT_PATH, '', $this->absolutePath);

        $this->class = $reflector->getShortName();
        $this->shortClass = preg_replace('/(.*)Component$/', '$1', $this->class);
    }

    /**
     * Appelée lors d'un `clone [...]`
     */
    public function __clone()
    {
        $this->model = $this->componentRepository->replicate($this->model, ['parent_id']);
    }

    public function canSave()
    {
        /** @var Context $context */
        $context = $this->container->make('context');

        if ($context->isRoot()) {
            return true;
        }

        $rights = $this->model->getRights();

        if (substr($rights, 2, 1) >= '6') {
            return true;
        }

        $user = $this->model->getUserId();

        if ($user == $context->getUser()) {
            return true;
        }

        $group = $this->model->getGroupId();

        if (in_array($group, $context->getGroups()) && substr($rights, 1, 1) >= '6') {
            return true;
        }

        return false;
    }

    /**
     * @param bool $check
     */
    public function save($check = true)
    {
        if ($check && $this->canSave()) {
            $this->componentRepository->save($this->model);

            foreach ($this->children as $order => $child) {
                $child->save($check);
            }
        }
    }

    /**
     * @param callable $beforeDelete Callback appelé à chaque suppression. Prend l'instance de ce composant comme
     *                               paramètre.
     * @param bool     $check
     */
    public function delete(callable $beforeDelete, $check = true)
    {
        if ($check && $this->canSave()) {
            foreach ($this->children as $child) {
                $child->delete($beforeDelete, $check);
            }

            $beforeDelete($this);
            $this->componentRepository->delete($this->model);
        }
    }

    public function move($parent_id, $order)
    {
        $this->model->setParentId($parent_id);
        $this->model->setOrder($order);
        $this->save();
    }

    /**
     * Duplique un composant et ses enfants, tout en assignant un nouveau root_id, parent_id, et order.
     *
     * @param callable    $afterDuplicate
     * @param number|null $new_root_id   Si null, alors n'est pas assigné
     * @param number|null $new_parent_id Si null, alors n'est pas assigné
     * @param number|null $new_order     Si null, alors n'est pas assigné
     *
     * @return BaseComponent
     */
    public function duplicate(callable $afterDuplicate, $new_root_id = null, $new_parent_id = null, $new_order = null)
    {
        $component = clone $this;

        if (!is_null($new_root_id)) {
            $component->model->setRootId($new_root_id);
        }

        if (!is_null($new_parent_id)) {
            $component->model->setParentId($new_parent_id);
        }

        if (!is_null($new_order)) {
            $component->model->setOrder($new_order);
        }

        $this->componentRepository->save($component->model);
        $afterDuplicate($component);

        foreach ($component->children as $order => $child) {
            $component->children[$order] = $child->duplicate(
                $afterDuplicate,
                $new_root_id,
                $component->model->getId()
            );
        }

        return $component;
    }

    /**
     * @return NinaObject
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param BaseComponent $child
     */
    public function addChild(BaseComponent $child)
    {
        $model = $child->getModel();
        $this->children[$model->getOrder() . '_' . $model->getId()] = $child;
    }

    /**
     * Charge les enfants du composant de manière récursive.
     */
    public function loadChildrenRecursively()
    {
        /** @var ComponentProvider $componentProvider */
        $componentProvider = $this->container->make('component_provider');

        $children = $this->componentRepository->getDirectChildren($this->model->getId());

        if ($children === null) {
            return;
        }

        foreach ($children as $child) {
            /** @var NinaObject $child */
            $childComponent = $componentProvider->get($child->getClass() . 'Component');
            /** @var BaseComponent $childInstance */
            $childInstance = new $childComponent($child);
            $childInstance->loadChildrenRecursively();
            $this->addChild($childInstance);
        }

        ksort($this->children);
    }

    public function prepareForRender()
    {
        $flow = new StdClass();
        $flow->class = $this->getClass();
        $flow->shortClass = $this->getShortClass();
        $flow->model = $this->model->toArray();
        $flow->properties = ['tag' => 'div', 'attrClass' => []];
        $flow->medias = $this->getMedias();
        $flow->fields = $this->getFields();
        $flow->flattenFields = [];
        $flow->fieldsValues = ($this->model ? $this->model->getFieldsValues() : []);
        $flow->state = $this->getState();
        $flow->template = $this->getTemplate();

        // Les propriétés
        $this->prepareProperties();
        $this->handleProperties($flow->properties);

        // Enfants
        $flow->children = [];

        foreach ($this->children as $key => $child) {
            $flow->children[$key] = $child->prepareForRender();
        }

        // Fields "aplatis"
        array_walk_recursive(
            $flow->fields,
            function ($field) use ($flow) {
                if ($field instanceof BaseField && !is_null($field->id)) {
                    $flow->flattenFields[$field->id] = $field;
                }
            }
        );

        return $flow;
    }

    /**
     * @param array $params
     *
     * @return string
     * @internal param array $param
     *
     */
    public function render($params = [])
    {
        return render($this->getTemplate(), [
            'ninaObject' => $this->prepareForRender(),
        ], $params);
    }

    public function canContains($componentShortClass)
    {
        return in_array($componentShortClass, $this->contains);
    }

    /**
     * Fusionne les tableaux des resources (styles, scripts ...) en un seul tableau.
     * Si le mode d'édition (admin) n'est pas activé, alors les resources admin ne sont pas mergées.
     *
     * @param string $type 'styles' ou' scripts'
     * @param array  $publicResources
     * @param array  $adminResources
     *
     * @return array
     */
    private function mergePublicAndAdminResources($type, array $publicResources, array $adminResources)
    {
        $resources = [];

        foreach ($publicResources as $publicResource) {
            $resources[] = '/' . $type . '/' . $publicResource;
        }

        if ($this->container->make('context')->isEditModeEnabled()) {
            foreach ($adminResources as $adminResource) {
                $resources[] = '/admin/' . $type . '/' . $adminResource;
            }
        }

        return $resources;
    }

    /**
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * @return mixed
     */
    public function getRelativePath()
    {
        return $this->relativePath;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getShortClass()
    {
        return $this->shortClass;
    }

    public function getMedias()
    {
        $medias = [];

        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $this->container->make('medias_repository');
        $fieldsValues = $this->model->getFieldsValues();

        foreach (get($fieldsValues['__reflection']['fields'], []) as $fieldId => $field) {
            if (!fieldCanContainsMedias($field)) {
                continue;
            }

            foreach (get($fieldsValues[$fieldId], []) as $mediaId) {
                $media = $mediaRepository->findByIdOrFail($mediaId);
                $medias[$fieldId][] = extractNinaObjectData($media);
            }
        }

        return $medias;
    }

    /**
     * @return BaseField[][]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getFieldsRules()
    {
        return $this->fieldsRules;
    }

    /**
     * @return array
     */
    public function getFieldsFileRules()
    {
        return $this->fieldsFileRules;
    }

    /**
     * @return BaseComponent[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getContains()
    {
        return $this->contains;
    }

    /**
     * @return array
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param bool $assets
     *
     * @return array
     */
    public function getMetadata($assets = true)
    {
        $metadata = [
            'name' => $this->getName(),
            'group' => $this->getGroup(),
            'icon' => $this->getIcon(),
            'contains' => $this->getContains(),
        ];

        if ($assets) {
            $metadata['assets'] = [
                'template' => $this->getTemplate(),
                'scripts' => $this->getScripts(),
                'styles' => $this->getStyles(),
            ];
        }

        return $metadata;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        if (is_null($this->template)) {
            $file = $this->class . '.html.twig';
            $template = $this->getRelativePath() . '/templates/' . $file;

            $this->template = $template;

            // Si on est en mode admin et que le template Admin existe, alors on l'utilise.
            // Sinon, on garde le template "public".
            if ($this->container->make('context')->isEditModeEnabled()) {
                $adminTemplate = $this->getRelativePath() . '/admin/templates/' . $file;
                if (file_exists(NINA_ROOT_PATH . $adminTemplate)) {
                    $this->template = $adminTemplate;
                }
            }
        }

        return $this->template;
    }

    /**
     * @return array|string
     */
    public function getStyles()
    {
        return $this->mergePublicAndAdminResources('styles', $this->styles, $this->adminStyles);
    }

    /**
     * @return array|string
     */
    public function getScripts()
    {
        return $this->mergePublicAndAdminResources('scripts', $this->scripts, $this->adminScripts);
    }

    /**
     * {@inheritdoc}
     */
    protected function getIdForProperty()
    {
        return 'component';
    }

    protected function getProperties()
    {
        return $this->properties;
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareProperties()
    {
        $fieldsValues = $this->getModel()->getFieldsValues();

        foreach ($this->getProperties() as $property) {
            $property->value = get($fieldsValues[$property->id], $property->defaultValue);
        }
    }
}
