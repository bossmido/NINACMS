<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Component;

use Nina\Field\BaseField;

/**
 * Class ComponentReflector
 *
 * @package Nina\Component
 */
class ComponentReflector
{
    public static function reflect(BaseComponent $component)
    {
        $component->afterConstruct();

        $reflectionResult = [
            'fields' => self::reflectFields($component),
        ];

        return $reflectionResult;
    }

    private static function reflectFields(BaseComponent $component)
    {
        $fields = [];

        array_walk_recursive($component->getFields(), function ($field) use (&$fields) {
            if (! ($field instanceof BaseField)) {
                return;
            }

            if ($field->id === null) {
                return;
            }

            $fields[$field->id] = [
                'class' => $field->class,
            ];
        });

        return $fields;
    }
}
