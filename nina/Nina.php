<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina;

use Nina\Contracts\TemplateManager;

/**
 * Class Nina
 *
 * @package Nina
 */
class Nina
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var TemplateManager
     */
    private $templateManager;

    /**
     * Nina constructor.
     *
     * @param Context         $context
     * @param TemplateManager $templateManager
     */
    public function __construct(Context $context, TemplateManager $templateManager)
    {
        $this->context = $context;
        $this->templateManager = $templateManager;
    }
}
