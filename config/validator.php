<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/*
 |--------------------------------------------------------------------------
 | Configuration du Validator
 |--------------------------------------------------------------------------
 | https://laravel.com/docs/5.4/container
 |
 | Pour récupérer les services, on fera appel à `$container->make('...')`,
 | où `$container = \Illuminate\Container\Container::getInstance()`.
 */

use Illuminate\Validation\Validator;
use Nina\Component\ComponentProvider;
use Nina\Exceptions\ComponentNotFoundInProviderException;

return [
    // Règles de validations customisées
    'custom_validation_rules' => [

        // Permet de vérifier le type mime d'un fichier, en utilisant les « file_type_aliases ».
        'nina_mimes' => function ($attribute, $value, $parameters, Validator $validator) use ($container) {
            $fileTypeAliases = $container->make('config')->get('nina.file_type_aliases');
            $mimes = [];

            foreach ($parameters as $parameter) {
                if (isset($fileTypeAliases[$parameter])) {
                    $mimes = array_merge($mimes, $fileTypeAliases[$parameter]);
                } else {
                    $mimes[] = $parameter;
                }
            }

            /** @var \Kocal\Validator\Validator $v */
            $v = $container->make('validator');
            $v->setRules([$attribute => 'mimes:' . implode(',', $mimes)]);
            $v->validate([$attribute => $value], [], ['file' => 'Fichier']);

            return $v->passes();
        },

        'nina_component_exists' => function ($attribute, $value, $parameters, Validator $validator) use ($container) {
            /** @var ComponentProvider $componentProvider */
            $componentProvider = $container->make('component_provider');

            try {
                $componentProvider->get($value . 'Component');
            } catch (ComponentNotFoundInProviderException $e) {
                return false;
            }

            return true;
        },
    ],
];
