<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/*
 |--------------------------------------------------------------------------
 | Configuration du Container
 |--------------------------------------------------------------------------
 | NinaCMS utilise des composants venant du framework Laravel.
 | Pour fonctionner correctement, ces composants ont souvent besoin de
 | récupérer une configuration auprès du Container.
 */

return [
    /**
     * ------------------------------------------------------------------------
     * Configuration des systèmes de fichiers
     * ------------------------------------------------------------------------
     *
     * @see https://github.com/laravel/laravel/blob/master/config/filesystems.php
     */

    // Le disque par défaut sera le disque local
    'filesystems.default' => 'local',

    // Disque local
    'filesystems.disks.local' => [
        'driver' => 'local',
        'root' => NINA_PUBLIC_PATH,
    ],

    // Disque pour la médiathèque Nina
    'filesystems.disks.mediatheque_nina' => [
        // 'local' est le seul driver à être supporté pour la médiathèque pour l'instant,
        // car j'ai besoin de certaines méthodes pour copier et déplacer un dossier.
        'driver' => 'local',
        'root' => NINA_PUBLIC_PATH . '/mediatheque',
    ],

    // Disque FTP (non testé)
    'filesystems.disks.ftp' => [
        'driver' => 'ftp',
        'host' => NINA_FTP_HOST,
        'username' => NINA_FTP_USERNAME,
        'password' => NINA_FTP_PASSWORD,
    ],

    /**
     * ------------------------------------------------------------------------
     * Configuration du cache
     * ------------------------------------------------------------------------
     *
     * @see https://github.com/laravel/laravel/blob/master/config/cache.php
     */

    // Par défaut, on utilisera le store 'file'
    'cache.default' => 'file',

    // Définition du store 'file'
    'cache.stores.file' => [
        'driver' => 'file',
        'path' => NINA_CACHE_PATH,
    ],
];
