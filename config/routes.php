<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/*
 |--------------------------------------------------------------------------
 | Configuration des routes
 |--------------------------------------------------------------------------
 | https://laravel.com/docs/5.4/routing
 */

/** @var \Illuminate\Routing\Router $router */

$router->get('/', function () {
    return 'NinaCMS';
});

$router->get('/{root_id}', 'ComponentsController@renderCollection')
    ->where('root_id', '[0-9]+');

$router->get('/admin/', 'PagesController@adminIndex');

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'objects'], function () use ($router) {
        $router->get('/collection', 'ComponentsController@getCollection');
        $router->get('/metadata', 'ComponentsController@getAllMetadata');
        $router->get('/render', 'ComponentsController@render');
        $router->get('/form_edition', 'ComponentsController@renderEditionForm');

        $router->post('/save_fields_values', 'ComponentsController@saveFieldsValues');
        $router->post('/save_styles', 'ComponentsController@saveStyles');
        $router->post('/change_orders', 'ComponentsController@changeOrders');

        $router->post('/create', 'ComponentsController@create');
        $router->post('/move', 'ComponentsController@move');
        $router->post('/duplicate', 'ComponentsController@duplicate');
        $router->post('/delete', 'ComponentsController@delete');
        $router->post('/paste/before', 'ComponentsController@pasteBefore');
        $router->post('/paste/after', 'ComponentsController@pasteAfter');
    });

    $router->group(['prefix' => 'library'], function () use ($router) {
        $router->get('/', 'LibraryController@getLibraryId');
        $router->get('{id}/components', 'LibraryController@getComponents');
        $router->get('/componentsGroupsTree', 'LibraryController@getComponentsGroupsTree');
    });

    $router->group(['prefix' => 'upload'], function () use ($router) {
        $router->post('/file', 'UploadController@file');
        $router->post('/file_for_component', 'UploadController@fileForComponent');
    });

    $router->group(['prefix' => 'mediatheques', 'namespace' => 'Mediatheque'], function () use ($router) {
        $router->get('/', 'MediathequeManagerController@index');

        /*
         * TODO
         * Au lieu de dupliquer toutes les routes pour chaque type de médiathèque,
         * ne serait-il pas plus intelligent d'intercepter toutes les requêtes sur
         * le MediathequeController, et de les dispatcher vers les controllers adéquat ?
         */
        $router->group(['prefix' => 'nina'], function () use ($router) {
            $router->get('/', 'NinaMediathequeController@readMediasContainer');

            $router->post('medias_containers/create', 'NinaMediathequeController@createMediasContainer');
            $router->post('medias_containers/move', 'NinaMediathequeController@moveMediaContainers');
            $router->post('medias_containers/rename', 'NinaMediathequeController@renameMediasContainer');
            $router->post('medias_containers/copy', 'NinaMediathequeController@copyMediaContainers');
            $router->post('medias_containers/delete', 'NinaMediathequeController@deleteMediasContainer');

            $router->post('medias/copy', 'NinaMediathequeController@copyMedia');
            $router->post('medias/move', 'NinaMediathequeController@moveMedia');
            $router->post('medias/rename', 'NinaMediathequeController@renameMedia');
            $router->post('medias/delete', 'NinaMediathequeController@deleteMedia');

            $router->post('images/crop', 'NinaMediathequeController@cropImage');
        });

        $router->group(['prefix' => 'wordpress'], function () use ($router) {
            $router->get('/', 'WordPressMediathequeController@readMediasContainer');

            $router->post('medias_containers/create', 'WordPressMediathequeController@createMediasContainer');
            $router->post('medias_containers/move', 'WordPressMediathequeController@moveMediaContainers');
            $router->post('medias_containers/rename', 'WordPressMediathequeController@renameMediasContainer');
            $router->post('medias_containers/copy', 'WordPressMediathequeController@copyMediaContainers');
            $router->post('medias_containers/delete', 'WordPressMediathequeController@deleteMediasContainer');

            $router->post('medias/copy', 'WordPressMediathequeController@copyMedia');
            $router->post('medias/move', 'WordPressMediathequeController@moveMedia');
            $router->post('medias/rename', 'WordPressMediathequeController@renameMedia');
            $router->post('medias/delete', 'WordPressMediathequeController@deleteMedia');

            $router->post('images/crop', 'NinaMediathequeController@cropImage');
        });

        $router->group(['prefix' => 'joomla'], function () use ($router) {
            $router->get('/', 'JoomlaMediathequeController@readMediasContainer');

            $router->post('medias_containers/create', 'JoomlaMediathequeController@createMediasContainer');
            $router->post('medias_containers/move', 'JoomlaMediathequeController@moveMediaContainers');
            $router->post('medias_containers/rename', 'JoomlaMediathequeController@renameMediasContainer');
            $router->post('medias_containers/copy', 'JoomlaMediathequeController@copyMediaContainers');
            $router->post('medias_containers/delete', 'JoomlaMediathequeController@deleteMediasContainer');

            $router->post('medias/copy', 'JoomlaMediathequeController@copyMedia');
            $router->post('medias/move', 'JoomlaMediathequeController@moveMedia');
            $router->post('medias/rename', 'JoomlaMediathequeController@renameMedia');
            $router->post('medias/delete', 'JoomlaMediathequeController@deleteMedia');

            $router->post('images/crop', 'NinaMediathequeController@cropImage');
        });
    });
});
