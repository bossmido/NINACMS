<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/*
 |--------------------------------------------------------------------------
 | Configuration des alias de types de fichiers
 |--------------------------------------------------------------------------
 | Ces alias sont utilisés par la règle de validation « nina_mimes »,
 | pendant l'écriture des règles de validations pour « FieldFile ».
 */

return [
    /**
     * Les images
     */
    'image' => [
        'jpg',
        'jpeg',
        'png',
        'gif',
    ],

    /**
     * Fichiers de traitement de texte (Office & Open Document Format)
     */
    'document_text' => [
        'odt',
        'odm',
        'ott',
        'oth',
        'doc',
        'docx',
    ],

    /**
     * Fichiers de présentation (Office & Open Document Format)
     */
    'document_presentation' => [
        'odp',
        'otp',
        'pptx',
        'ppt',
    ],

    /**
     * Composition de 2 alias grâce à l'utilisation du symbole « @ ».
     * L'alias « document » sera composé des valeurs de « document_text» et de « document_presentation ».
     */
    'document' => [
        '@document_text',
        '@document_presentation',
    ],

];
