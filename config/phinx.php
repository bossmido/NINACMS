<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

require_once __DIR__ . '/config.php';

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/../database/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/../database/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => NINA_ENVIRONMENT,
        'prod' => [
            'adapter' => 'mysql',
            'host' => NINA_DB_HOST,
            'name' => NINA_DB_NAME . '_prod',
            'user' => NINA_DB_USER,
            'pass' => NINA_DB_PASSWORD,
            'port' => 3306,
            'charset' => 'utf8',
        ],
        'dev' => [
            'adapter' => 'mysql',
            'host' => NINA_DB_HOST,
            'name' => NINA_DB_NAME . '_dev',
            'user' => NINA_DB_USER,
            'pass' => NINA_DB_PASSWORD,
            'port' => 3306,
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => NINA_DB_HOST,
            'name' => NINA_DB_NAME . '_testing',
            'user' => NINA_DB_USER,
            'pass' => NINA_DB_PASSWORD,
            'port' => 3306,
            'charset' => 'utf8',
        ],
    ],
    'version_order' => 'creation',
];
