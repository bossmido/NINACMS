<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

require __DIR__ . '/../bootstrap/bootstrap.php';

use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('spec')
    ->exclude('public')
    ->exclude('ressources')
    ->exclude('vendor')
    ->in(['app', 'nina']);

return new Sami($iterator, [
    'title' => 'NinaCMS',
    'build_dir' => NINA_ROOT_PATH . '/docs-api/ninacms-php',
    'cache_dir' => NINA_CACHE_PATH . '/docs-api/ninacms-php',
    'default_opened_level' => 1,
]);
