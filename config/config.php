<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

// Lit le fichier de configuration « sensible »
$dotenv = new \Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

if (!defined('DS')) {
    /**
     * Raccourci vers la constante `DIRECTORY_SEPARATOR`.
     *
     * @see http://php.net/manual/fr/dir.constants.php
     */
    define('DS', DIRECTORY_SEPARATOR);
}

/*
 |--------------------------------------------------------------------------
 | Configuration de l'Application
 |--------------------------------------------------------------------------
 */

/**
 * L'environnement où se trouve Nina.
 * Peut prendre les version « prod », « dev » et « testing ».
 */
define('NINA_ENVIRONMENT', env('NINA_ENVIRONMENT', 'prod'));

/**
 * Numéro de version
 */
define('NINA_VERSION', '0.1.0');

/**
 * Locale à utiliser
 */
define('NINA_LOCALE', 'fr');

/**
 * Locale à utiliser si les traductions pour NINA_LOCALE n'existent pas
 */
define('NINA_LOCALE_FALLBACK', 'fr');

/**
 * Identifiant de l'application
 */
define('NINA_APP_ID', env('NINA_APP_ID'));

/**
 * Un Token
 */
define('NINA_TOKEN_KEY', env('NINA_TOKEN_KEY'));

/*
 |--------------------------------------------------------------------------
 | Configuration des chemins
 |--------------------------------------------------------------------------
 */

/**
 * Le chemin web menant vers le point d'entrée de Nina.
 * Toutes les URLs générées par NinaCMS seront préfixés par la valeur de NINA_BASE_WEB_PATH.
 *
 * Cette constante peut être désactivée, ou vide.
 */

define('NINA_BASE_WEB_PATH', env('NINA_BASE_WEB_PATH'));

/**
 * Le chemin web menant vers les ressources contenues dans la médiathèque Nina.
 * Peut contenu un chemin relatif, ou une adresse URL.
 *
 * Attention, cette constante n'a rien à voir avec la configuration des systèmes de
 * fichiers (config/container.php) !
 * Ici nous configurons l'adresse pour accéder aux ressources.
 * En revanche, le chemin pour stocker des ressources est différent !
 */
define('NINA_MEDIATHEQUE_URL', '/mediatheque');
//define('NINA_MEDIATHEQUE_URL', 'https://cdn.example.com/foo/bar/.../');

/**
 * Le chemin du dossier racine.
 */
define('NINA_ROOT_PATH', dirname(__DIR__));

/**
 * Le chemin du dossier contenant la partie « Système » de Nina.
 */
define('NINA_PATH', NINA_ROOT_PATH . DS . 'nina');

/**
 * Le chemin du dossier pour stocker le cache.
 */
define('NINA_CACHE_PATH', NINA_ROOT_PATH . DS . 'cache');

/**
 * Le chemin du dossier contenant les ressources accessible publiquement, ainsi que le point d'entrée.
 */
define('NINA_PUBLIC_PATH', NINA_ROOT_PATH . DS . 'public');

/**
 * Le chemin du dossier contenant la partie « Application » de Nina.
 */
define('NINA_APP_PATH', NINA_ROOT_PATH . DS . 'app');

/**
 * Le chemin du dossier contenant les composants (PHP, Twig, SCSS, JS...).
 */
define('NINA_COMPONENTS_PATH', NINA_APP_PATH . DS . 'Components');

/**
 * Le chemin du dossier contenant les ressources non accessibles publiquement.
 */
define('NINA_RESOURCES_PATH', NINA_ROOT_PATH . DS . 'resources');

/**
 * Chemin vers les fichiers de traduction de Nina.
 */
define('NINA_LOCALES_PATH', NINA_RESOURCES_PATH . DS . 'locales');

/**
 * Chemin vers les pages.
 */
define('NINA_PAGES_PATH', NINA_RESOURCES_PATH . DS . 'pages');

/**
 * Chemin vers les templates Twig.
 */
define('NINA_TEMPLATES_PATH', NINA_RESOURCES_PATH . DS . 'templates');

/*
 |--------------------------------------------------------------------------
 | Configuration de la base de données
 |--------------------------------------------------------------------------
 */

/**
 * Type de base de données (mysql, sqlite, pgsql, sqlsrv)
 */
define('NINA_DB_TYPE', env('NINA_DB_TYPE', 'mysql'));

/**
 * Préfixe pour le nom des tables
 */
define('NINA_DB_PREFIX', env('NINA_DB_PREFIX'));

/**
 * Hôte de la base de données
 */
define('NINA_DB_HOST', env('NINA_DB_HOST', 'localhost'));

/**
 * Nom de la base de données
 */
define('NINA_DB_NAME', env('NINA_DB_NAME'));

/**
 * Utilisateur de la base de données
 */
define('NINA_DB_USER', env('NINA_DB_USER'));

/**
 * Mot de passe de la base de données
 */
define('NINA_DB_PASSWORD', env('NINA_DB_PASSWORD'));

/*
 |--------------------------------------------------------------------------
 | Configuration du FTP (pas testé)
 |--------------------------------------------------------------------------
 */

/**
 * Adresse du serveur FTP
 */
define('NINA_FTP_HOST', env('NINA_FTP_HOST'));

/**
 * Port du serveur (défaut: 21)
 */
define('NINA_FTP_PORT', env('NINA_FTP_PORT', 21));

/**
 * Utilisateur pour se connecter au serveur FTP
 */
define('NINA_FTP_USERNAME', env('NINA_FTP_USERNAME'));

/**
 * Mot de passe de l'utilisateur
 */
define('NINA_FTP_PASSWORD', env('NINA_FTP_PASSWORD'));

/*
 |--------------------------------------------------------------------------
 | Configuration des Cookies (pas testé)
 |--------------------------------------------------------------------------
 */

define('NINA_COOKIE_TIMEOUT', 600);
define('NINA_COOKIE_PATH', '/');

/*
 |--------------------------------------------------------------------------
 | Autre / Misc
 |--------------------------------------------------------------------------
 */

/**
 * Défini la timezone à utiliser.
 *
 * @see http://php.net/manual/fr/function.date-default-timezone-set.php
 * @see http://php.net/manual/fr/timezones.php
 */
define('NINA_TIMEZONE', 'Europe/Paris');

/**
 * Permet d'activer ou non la library Tidy.
 *
 * Nécessite l'extension PHP « tidy ».
 *
 * @see http://tidy.sourceforge.net/docs/quickref.html
 * @see http://php.net/manual/fr/book.tidy.php
 */
define('NINA_USE_TIDY', true);

/**
 * Driver à utiliser pour la libraire Intervention.
 * Peut prendre la valeur 'imagick' ou 'gd'.
 *
 * @see http://image.intervention.io/
 */
define('NINA_INTERVENTION_IMAGE_DRIVER', 'gd');
