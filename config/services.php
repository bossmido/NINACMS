<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/*
 |--------------------------------------------------------------------------
 | Configuration des services pour le Container
 |--------------------------------------------------------------------------
 | https://laravel.com/docs/5.4/container
 |
 | Pour récupérer les services, on fera appel à `$container->make('...')`,
 | où `$container = \Illuminate\Container\Container::getInstance()`.
 */

use App\Repositories\DatabaseComponentRepository;
use App\Repositories\DatabaseLibraryRepository;
use App\Repositories\DatabaseMediaRepository;
use App\Repositories\DatabaseMediasContainerRepository;
use Nina\Collection;
use Nina\Component\ComponentProvider;
use Nina\Context;
use Nina\Contracts\Repositories\ComponentRepository;
use Nina\Contracts\Repositories\LibraryRepository;
use Nina\Contracts\Repositories\MediaRepository;
use Nina\Contracts\Repositories\MediasContainerRepository;
use Nina\Nina;
use Nina\Services\ComponentService;
use Nina\Template\Twig\TwigTemplateManager;

return [
    /**
     * Appellera $container->bind($k, $v);
     *
     * @see https://laravel.com/docs/5.4/container#binding
     */
    'bind' => [

    ],
    /**
     * Permet de binder des singletons.
     * Appellera $container->singleton($k, $v);
     */
    'singleton' => [
        // Repositories
        'component_repository' => DatabaseComponentRepository::class,
        'library_repository' => DatabaseLibraryRepository::class,
        'medias_containers_repository' => DatabaseMediasContainerRepository::class,
        'medias_repository' => DatabaseMediaRepository::class,

        ComponentRepository::class => function ($app) {
            return $app->make('component_repository');
        },
        LibraryRepository::class => function ($app) {
            return $app->make('library_repository');
        },
        MediasContainerRepository::class => function ($app) {
            return $app->make('medias_containers_repository');
        },
        MediaRepository::class => function ($app) {
            return $app->make('medias_repository');
        },

        // Services
        'component_service' => ComponentService::class,

        // Autre
        'collection' => Collection::class,
        'component_provider' => ComponentProvider::class,
        'context' => Context::class,

        'template_manager' => function ($app) {
            return new TwigTemplateManager($app->make('context'));
        },

        'nina' => function ($app) {
            return new Nina(
                $app->make('context'),
                $app->make('template_manager')
            );
        },

        'translator' => function ($app) {
            $fileLoader = new \Illuminate\Translation\FileLoader($app->make('files'), NINA_LOCALES_PATH);
            $translator = new \Illuminate\Translation\Translator($fileLoader, NINA_LOCALE);
            $translator->setFallback(NINA_LOCALE_FALLBACK);

            return $translator;
        },

        'validator' => function ($app) {
            $config = $app->make('config')->get('nina.validator');
            $validator = new \Kocal\Validator\Validator([], NINA_LOCALE);

            foreach ($config['custom_validation_rules'] as $customValidationRule => $closure) {
                $validator->extend($customValidationRule, $closure);
            }

            return $validator;
        },

        /**
         * Singletons pour les composants de Laravel
         */

        'files' => \Illuminate\Filesystem\Filesystem::class,

        'cache' => function ($app) {
            return new \Illuminate\Cache\CacheManager($app);
        },

        \Illuminate\Contracts\Filesystem\Factory::class => function ($app) {
            return new \Illuminate\Filesystem\FilesystemManager($app);
        },

        \Illuminate\Contracts\Routing\UrlGenerator::class => function ($app) {
            $urlGenerator = new \Illuminate\Routing\UrlGenerator(
                $app->make(\Illuminate\Routing\RouteCollection::class),
                $app->make(\Illuminate\Http\Request::class)
            );

            $urlGenerator->formatPathUsing(
                function ($path) {
                    $prefix = NINA_BASE_WEB_PATH;
                    $prefix = trim($prefix, '/');
                    $path = trim($path, '/');

                    return "/$prefix/$path";
                }
            );

            return $urlGenerator;
        },
    ],
];
