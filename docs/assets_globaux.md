# Assets globaux

## Script global

Le script global est situé dans `ressources/assets/js/nina.js`.




## Style global

La feuille de styles globale est située dans `ressources/assets/sass/nina.css`.



## Compilation des assets globaux

### Pour la production

```
$ npm run assets
```

### Pour le développement

```
$ npm run assets:dev
```

### Pour le développement + observations des modifications

```
$ npm run assets:watch
```
