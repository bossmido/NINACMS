# Assets des composants

Les assets des composants sont les assets liés à un composant. Ils se trouves dans les dossiers suivants :

- `app/Components/<Nom du composant>/styles` : styles SCSS pour le public
- `app/Components/<Nom du composant>/scripts` : scripts JS pour le public
- `app/Components/<Nom du composant>/images` : images pour le public
- `app/Components/<Nom du composant>/admin/styles` : styles SCSS  pour l'administration
- `app/Components/<Nom du composant>/admin/scripts` : scripts JS pour l'administration
- `app/Components/<Nom du composant>/admin/images` : images pour l'administration
