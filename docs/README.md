# Documentation de NinaCMS

Il est recommandé de lire la documentation dans un navigateur web. 

Pour cela, lancez la commande suivante et ouvrez l'adresse URL annoncée (généralement http://localhost:4000).

```bash
npm run docs:serve
```
