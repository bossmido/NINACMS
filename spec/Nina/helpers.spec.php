<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Nina\Exceptions\FilenameCantBeEmptyException;
use Nina\Exceptions\FilenameCantBeOnlyComposedWithSpacesOrDotsException;
use Nina\Exceptions\FilenameContainsIllegalCharacterException;

require_once __DIR__ . '/../../bootstrap/bootstrap.php';

describe('helpers', function () {
    describe('autoCastArrayValuesRecursively()', function () {
        it('should correctly cast', function () {
            $actualArray = [
                true,
                false,
                'true',
                'false',
                123,
                '456',
                "12302.18765168465",
                "41.666666666666664%",
                "25%",
                1.2,
                'abcdef',
                '12px',
                '1abc10',
                'test' => [
                    'foo' => 'false',
                    'bar' => true,
                ],
            ];

            $expectedArray = [
                true,
                false,
                true,
                false,
                123,
                456,
                12302.18765168465,
                "41.666666666666664%",
                "25%",
                1.2,
                'abcdef',
                '12px',
                '1abc10',
                'test' => [
                    'foo' => false,
                    'bar' => true,
                ],
            ];

            function check($value, $expectedValue)
            {
                if (is_array($value)) {
                    foreach ($value as $k => $v) {
                        check($v, $expectedValue[$k]);
                    }
                } else {
                    expect($value)->toBe($expectedValue);
                }
            }

            foreach (autoCastArrayValuesRecursively($actualArray) as $k => $v) {
                check($v, $expectedArray[$k]);
            }
        });
    });

    describe('checkIfFilenameIsValid', function () {
        it('should validate', function () {
            expect(function () {
                checkIfFilenameIsValid('filename');
            })->not->toThrow();
        });

        it('should not validate empty filename', function () {
            expect(function () {
                checkIfFilenameIsValid('');
            })->toThrow(new FilenameCantBeEmptyException());
        });

        it('should not validate filename with only spaces/dots', function () {
            expect(function () {
                checkIfFilenameIsValid('      ');
            })->toThrow(new FilenameCantBeOnlyComposedWithSpacesOrDotsException());

            expect(function () {
                checkIfFilenameIsValid('...');
            })->toThrow(new FilenameCantBeOnlyComposedWithSpacesOrDotsException());

            expect(function () {
                checkIfFilenameIsValid('  . .  .. foo');
            })->not->toThrow();
        });

        it('should not validate filename with not allowed chars', function () {
            expect(function () {
                checkIfFilenameIsValid('file:name');
            })->toThrow(new FilenameContainsIllegalCharacterException(':'));

            expect(function () {
                checkIfFilenameIsValid('file/name');
            })->toThrow(new FilenameContainsIllegalCharacterException('/'));
        });
    });

    describe('generateUniqueFilename', function () {
        it('should return original filename', function () {
            $originalFilename = 'filename';
            $othersExistingFilename = [];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($originalFilename);
        });

        it('should still return original filename', function () {
            $originalFilename = 'filename';
            $othersExistingFilename = [
                'foo',
                'bar',
            ];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($originalFilename);
        });

        it('should return a new filename', function () {
            $originalFilename = 'filename';
            $expectedFilename = 'filename (1)';
            $othersExistingFilename = [
                'filename',
                'foo',
                'bar',
            ];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($expectedFilename);
        });

        it('should still return a new filename', function () {
            $originalFilename = 'filename';
            $expectedFilename = 'filename (3)';
            $othersExistingFilename = [
                'filename',
                'filename (1)',
                'filename (2)',
                'foo',
                'bar',
            ];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($expectedFilename);
        });

        it('should supports a filename with an extension', function () {
            $originalFilename = 'filename.jpg';
            $expectedFilename = 'filename (3).jpg';
            $othersExistingFilename = [
                'filename.jpg',
                'filename (1).jpg',
                'filename (2).jpg',
                'foo',
                'bar',
            ];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($expectedFilename);
        });

        it('should supports complex filename', function () {
            $originalFilename = 'Mon super fichier qui a un nom peut-être un peu trop long.jpg';
            $expectedFilename = 'Mon super fichier qui a un nom peut-être un peu trop long (4).jpg';
            $othersExistingFilename = [
                'Mon super fichier qui a un nom peut-être un peu trop long.jpg',
                'Mon super fichier qui a un nom peut-être un peu trop long (1).jpg',
                'Mon super fichier qui a un nom peut-être un peu trop long (2).jpg',
                'Mon super fichier qui a un nom peut-être un peu trop long (3).jpg',
                'foo',
                'bar',
            ];

            $filename = generateUniqueFileName($originalFilename, $othersExistingFilename);

            expect($filename)->toBe($expectedFilename);
        });
    });
});
