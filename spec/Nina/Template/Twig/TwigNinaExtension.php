<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use Nina\Template\Twig\TwigNinaExtension;

require_once __DIR__ . '/../../../../bootstrap/bootstrap.php';

describe('TwigNinaFilters', function () {
    it("tests filter ninaColWidthPercToBootstrapCol", function () {
        $twigNinaFilters = new TwigNinaExtension();

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('100%'))->toBe('col-md-12');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('92.66666667%'))->toBe('col-md-12');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('91.66666667%'))->toBe('col-md-11');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('84.33333333%'))->toBe('col-md-11');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('83.33333333%'))->toBe('col-md-10');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('76%'))->toBe('col-md-10');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('75%'))->toBe('col-md-9');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('67.66666667%'))->toBe('col-md-9');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('66.66666667%'))->toBe('col-md-8');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('59.33333333%'))->toBe('col-md-8');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('58.33333333%'))->toBe('col-md-7');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('51%'))->toBe('col-md-7');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('50%'))->toBe('col-md-6');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('43.66666667%'))->toBe('col-md-6');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('41.66666667%'))->toBe('col-md-5');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('34.33333333%'))->toBe('col-md-5');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('33.33333333%'))->toBe('col-md-4');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('26%'))->toBe('col-md-4');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('25%'))->toBe('col-md-3');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('17.66666667%'))->toBe('col-md-3');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('16.66666667%'))->toBe('col-md-2');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('9.33333333%'))->toBe('col-md-2');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('8.33333333%'))->toBe('col-md-1');

        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('5%'))->toBe('col-md-1');
        expect($twigNinaFilters->ninaColWidthPercToBootstrapCol('1%'))->toBe('col-md-1');
    });
});
