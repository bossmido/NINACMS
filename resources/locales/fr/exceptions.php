<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

return [
    CantCopyFolderInItSelfException::class => "Il n'est pas possible de copier le dossier dans lui-même.",
    CantCopyUploadsFolderException::class => "Il n'est pas possible de copier le dossier des envois.",
    CantCreateFolderException::class => "Impossible de créer le dossier.",
    CantDeleteUploadsFolderException::class => "Il n'est pas possible de supprimer le dossier des envois.",
    CantMoveFolderInItSelfException::class => "Il n'est pas possible de déplacer le dossier dans lui-même.",
    CantMoveUploadsFolderException::class => "Il n'est pas possible de déplacer le dossier des envois.",
    CantRenameFileException::class => "Impossible de renommer le fichier.",
    CantRenameFolderException::class => "Impossible de renommer le dossier.",
    ComponentAlreadyRegisteredException::class => "Le composant « :componentName » a déjà été enregistré.",
    ComponentCanNotContainsComponentException::class => "Le composant « :parentComponentName » ne peut pas contenir le composant « :childComponentName ».",
    ComponentNotFoundException::class => "Impossible de récupérer le composant « :id ».",
    ComponentNotFoundInProviderException::class => "Le composant « :componentName » n'a pas été trouvé dans le fournisseur.",
    CopyFileException::class => "Une erreur est survenue pendant la copie du fichier.",
    CopyFolderException::class => "Une erreur est survenue pendant la copie du dossier.",
    DeleteFileException::class => "Une erreur est survenue pendant la suppression du fichier.",
    DeleteFolderException::class => "Une erreur est survenue pendant la suppression du dossier.",
    FieldNotFoundInComponentException::class => "Impossible de trouver le Field « :fieldName » dans le composant « :componentName ».",
    FileIsNotAllowedToBeUploadedException::class => "Le fichier « :filename » n'est pas autorisé à être uploadé.",
    FileNotFoundException::class => "Impossible de récupérer le fichier « :id ».",
    FilenameCantBeEmptyException::class => "Le nom du dossier ne peut être vide.",
    FilenameCantBeOnlyComposedWithSpacesOrDotsException::class => "Le nom du dossier ne peut être composé uniquement d'espaces ou de points.",
    FilenameContainsIllegalCharacterException::class => "Le nom du dossier contient un caractère interdit « :notAllowedChar ».",
    FilenameIsAlreadyTaken::class => "Ce nom est déjà utilisé par un autre fichier.",
    FolderNameIsAlreadyTaken::class => "Ce nom est déjà utilisé par un autre dossier.",
    FolderNotFoundException::class => "Impossible de récupérer le dossier « :id ».",
    FolderNotFoundAtLibraryRootException::class => "Impossible de récupérer le premier dossier de la librairie.",
    LibraryNotFoundException::class => "Impossible de récupérer la librairie ayant pour racine « :rootId ».",
    MoveFileException::class => "Une erreur est survenue pendant le déplacement du fichier.",
    MoveFolderException::class => "Une erreur est survenue pendant le déplacement du dossier.",
    UploadFileException::class => "Une erreur est survenue pendant l'envoie du fichier « :filename ».",
    UploadFolderNotFoundException::class => "Impossible de récupérer le dossier des envois.",
    ValidationException::class => "Une erreur est survenue pendant la validation des données."
];
