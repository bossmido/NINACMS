<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

use App\ComponentGroup;

return [
    ComponentGroup::ESSENTIALS => 'Essentials',
    ComponentGroup::MISCELLANEOUS => 'Miscellaneous',
    ComponentGroup::DISPLAY => 'Display',
    ComponentGroup::DISPLAY_COLUMNS => 'Colonnes',
    ComponentGroup::DISPLAY_COLUMNS_TEST => 'Colonnes (test)',
    ComponentGroup::ARTICLES => 'Articles',
];
