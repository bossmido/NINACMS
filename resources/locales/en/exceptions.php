<?php
/**
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

namespace Nina\Exceptions;

return [
    CantCopyFolderInItSelfException::class => "You can not copy a folder in itself.",
    CantCopyUploadsFolderException::class => "You can not copy the uploads folder.",
    CantCreateFolderException::class => "Unable to create the folder.",
    CantDeleteUploadsFolderException::class => "You can not delete the uploads folder.",
    CantMoveFolderInItSelfException::class => "You can not move a folder in itself.",
    CantMoveUploadsFolderException::class => "You can not move the uploads folder.",
    CantRenameFileException::class => "Unable to rename the file.",
    CantRenameFolderException::class => "Unable to rename the folder.",
    ComponentAlreadyRegisteredException::class => "The component « :componentName » has already been registered.",
    ComponentCanNotContainsComponentException::class => "The component « :parentComponentName » can not contains the component « :childComponentName ».",
    ComponentNotFoundException::class => "Unable to retrieve the component « :id ».",
    ComponentNotFoundInProviderException::class => "Unable to retrieve the component « :componentName » to the provider.",
    CopyFileException::class => "An error occurred while copying the file.",
    CopyFolderException::class => "An error occurred while copying the folder.",
    DeleteFileException::class => "An error occurred while deleting the file.",
    DeleteFolderException::class => "An error occurred while deleting the folder.",
    FieldNotFoundInComponentException::class => "Unable to find the Field « :fieldName » in the component « :componentName ».",
    FileNotFoundException::class => "Unable to retrieve the file « :id ».",
    FileIsNotAllowedToBeUploadedException::class => "The file « :filename » is not allowed to be uploaded.",
    FilenameCantBeEmptyException::class => "The folder name can not be empty.",
    FilenameCantBeOnlyComposedWithSpacesOrDotsException::class => "The folder name can not be only composed with spaces or dots.",
    FilenameContainsIllegalCharacterException::class => "The folder name contains a not allowed character « :notAllowedChar ».",
    FilenameIsAlreadyTaken::class => "This name is already used by another file.",
    FolderNameIsAlreadyTaken::class => "This name is already used by another folder.",
    FolderNotFoundException::class => "Unable to retrieve the folder « :id ».",
    FolderNotFoundAtLibraryRootException::class => "Unable to retrieve the first folder of the library.",
    LibraryNotFoundException::class => "Unable to retrieve the library which has for root « :rootId ».",
    MoveFileException::class => "An error occurred while moving the file.",
    MoveFolderException::class => "An error occurred while moving the folder.",
    UploadFileException::class => "An error occurred while sending the file « :filename ».",
    UploadFolderNotFoundException::class => "Unable to retrieve the uploads folder.",
    ValidationException::class => "An error occurred while validating data."
];
