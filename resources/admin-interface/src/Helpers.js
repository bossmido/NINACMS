/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * @module Helpers
 */

/**
 * Extrait et retourne l'identifiant du composant passé en paramètre
 * @param {jQuery|HTMLElement} $object - Composant
 * @return {Number}
 */
function extractObjectId ($object) {
  $object = $object instanceof jQuery ? $object.get(0) : $object

  const id = $object.getAttribute('id')
  const match = id.match(/(\d+)$/)

  if (!match) {
    throw new Error("Impossible de récupérer l'id du composant.", $object)
  }

  return parseInt(match[1], 10)
}

/**
 * Extrait et retourne la classe du composant passé en paramètre
 * @param {jQuery|HTMLElement} $object - Composant
 * @return {string}
 */
function extractObjectClass ($object) {
  $object = $object instanceof jQuery ? $object.get(0) : $object
  const id = $object.getAttribute('id')
  const match = id.match(/^Nina_(.*)_\d+$/)

  if (!match) {
    throw new Error('Impossible de récupérer la classe du composant.', $object)
  }

  return match[1]
}

/**
 * Appelle la fonction `extractObjectId` sur les éléments (JQuery)
 * passés en paramètre.
 * @param $$objects
 * @return {Array.<Number>}
 */
function extractObjectsId ($$objects) {
  return $$objects
    .map((_, $$el) => extractObjectId($$el))
    .get()
}

/**
 * Calcule le nouvel ordre des éléments (JQuery) passés en paramètres,
 * tout en prenant compte d'un ordre initial.
 * @param $$objects
 * @param {number} [initialOrder=0]
 */
function computeObjectsOrder ($$objects, initialOrder = 0) {
  return $$objects
    .map(index => initialOrder + index)
    .get()
}

/**
 * Wrapper vers les deux méthodes extractObjectsId et computeObjectsOrder.
 * @param $$objects
 * @param initialOrder
 * @return {Array.<Array, Array>}
 */
function getIdAndNewOrderForObjects ($$objects, initialOrder = 0) {
  return [
    extractObjectsId($$objects),
    computeObjectsOrder($$objects, initialOrder)
  ]
}

/**
 * Swap the elements in an array at indexes x and y.
 *
 * @param {Array} array
 * @param {Number} x - L'index du premier élément à échanger
 * @param {Number} y - L'index du second élément à échanger.
 * @return {Array}
 * @see https://gist.github.com/eerohele/5195815
 */
function swapArrayElements (array, x, y) {
  if (array.length === 1) {
    return array
  }

  array.splice(y, 1, array.splice(x, 1, array[y])[0])

  return array
}

export {
  extractObjectId,
  extractObjectClass,
  extractObjectsId,
  computeObjectsOrder,
  getIdAndNewOrderForObjects,
  swapArrayElements
}
