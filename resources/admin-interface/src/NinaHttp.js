/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

import Vue from 'vue'
import store from './store'
import * as Mutation from './store/mutation-types'

const Base64 = require('js-base64').Base64

/**
 * @exports Http
 */
const Http = {
  Objects: {},
  Library: {},
  Mediatheque: {}
}

/**
 * @exports Http/Objects
 */
const Objects = {
  /**
   * Récupère la collection associée à l'app en cours.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getCollection (callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'objects/collection', {}, callback, finallyCallback)
  },

  /**
   * Récupère les méta-données des composants, au format JSON.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getMetadata (callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'objects/metadata', {}, callback, finallyCallback)
  },

  /**
   * Récupère le formulaire d'édition de composant auprès du serveur.
   * @param {number} id - L'id classe composant.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getFormEdition (id, callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'objects/form_edition', {id}, callback, finallyCallback)
  },

  /**
   * Crée un composant auprès du serveur.
   * @param {Object} data
   * @param {Number} data.data.parent_id
   * @param {String} data.data.class
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   */
  create (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/create', data, callback, finallyCallback)
  },

  /**
   * Retourne le rendu du composant `data.id`
   * @param {Object} data
   * @param {Number} data.id
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   */
  render (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'objects/render', data, callback, finallyCallback)
  },

  /**
   * Sauvegarde les styles d'un objet.
   * @param {Object} data
   * @param {Array} data.data
   * @param {Number} data.data.*.id
   * @param {Object} data.data.*.styles
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  saveStyles (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/save_styles', data, callback, finallyCallback)
  },

  /**
   * Sauvegarde les valeurs des fields.
   * @param {String} serializedForm - Contient l'id du composant et ses fieldsValues.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  saveFieldsValues (serializedForm, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/save_fields_values', serializedForm, callback, finallyCallback)
  },

  /**
   * Calcule le nouvel ordre des objects correspondant aux ids
   * @param {Object}   data
   * @param {Array}    data.data
   * @param {Number}   data.data.*.id
   * @param {Number}   data.data.*.order
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  changeOrders (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/change_orders', data, callback, finallyCallback)
  },

  /**
   * Déplace un objet.
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} data.parent_id
   * @param {Number} data.order
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  move (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/move', data, callback, finallyCallback)
  },

  /**
   * Duplique un objet, tout en lui assignant un nouvel parent_id et order.
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} data.parent_id
   * @param {Number} data.order
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  duplicate (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/duplicate', data, callback, finallyCallback)
  },

  /**
   * Supprime un objet.
   * @param {Number} id
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  delete (id, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/delete', {id}, callback, finallyCallback)
  },

  /**
   * Colle un objet avant un autre
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.before_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  pasteBefore (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/paste/before', data, callback, finallyCallback)
  },

  /**
   * Colle un objet après un autre
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.after_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  pasteAfter (data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', 'objects/paste/after', data, callback, finallyCallback)
  }
}

/**
 * @exports Http/Library
 */
const Library = {

  /**
   * Retourne l'ID de la Library associée à l'Application.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getId (callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'library', {}, callback, finallyCallback)
  },

  /**
   * Retourne les composants contenus dans la Library.
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getComponents (callback, finallyCallback = Http.after) {
    const libraryId = store.getters.library.id
    return Http.makeRequest('get', `library/${libraryId}/components`, {}, callback, finallyCallback)
  },

  /**
   * Retourne l'arbre des groupes des composants
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getComponentsGroupsTree (callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', `library/componentsGroupsTree`, {}, callback, finallyCallback)
  }
}

/**
 * @exports Http/Mediatheque
 */
const Mediatheque = {
  /**
   * Retourne les médiathèques disponibles
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  getAvailables (callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', 'mediatheques', {}, callback, finallyCallback)
  },

  /**
   * Retourne les Medias & Medias Containers ayant pour parent_id le `data.id`.
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  readMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('get', `mediatheques/${scope}`, data, callback, finallyCallback)
  },

  /**
   * Crée un Medias container dans le Medias container ayant pour id `data.parent_id`.
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.parent_id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  createMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias_containers/create`, data, callback, finallyCallback)
  },

  /**
   * Renomme un Medias container ayant pour id `data.id`.
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.parent_id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  renameMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias_containers/rename`, data, callback, finallyCallback)
  },

  /**
   * Déplace le medias container `data.id` dans le medias container `data.destination_id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  moveMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias_containers/move`, data, callback, finallyCallback)
  },

  /**
   * Copie le medias container `data.id` dans le medias container `data.destination_id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} data.destination_id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  copyMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias_containers/copy`, data, callback, finallyCallback)
  },

  /**
   * Supprimer un medias container ayant pour id `data.id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  deleteMediasContainer (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias_containers/delete`, data, callback, finallyCallback)
  },

  /**
   * Déplace le media `data.id` dans le medias container `data.destination_id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  moveMedia (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias/move`, data, callback, finallyCallback)
  },

  /**
   * Renomme un Media ayant pour id `data.id`.
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {String} data.newName
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  renameMedia (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias/rename`, data, callback, finallyCallback)
  },

  /**
   * Copie le media `data.id` dans le medias container `data.destination_id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} data.destination_id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  copyMedia (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias/copy`, data, callback, finallyCallback)
  },

  /**
   * Supprimer un media ayant pour id `data.id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.id
   * @param {Number} [data.library_id]
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  deleteMedia (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/medias/delete`, data, callback, finallyCallback)
  },

  /**
   * Supprimer un media ayant pour id `data.id`
   * @param {string} scope - 'nina', 'wordpress', 'joomla'
   * @param {Object} data
   * @param {Number} data.image_id
   * @param {Object} data.crop
   * @param {Function} callback
   * @param {Function} [finallyCallback]
   * @return {jqXHR}
   */
  cropImage (scope, data, callback, finallyCallback = Http.after) {
    return Http.makeRequest('post', `mediatheques/${scope}/images/crop`, data, callback, finallyCallback)
  }
}

/**
 * @param {String} method
 * @param {String} url
 * @param data
 * @param callback
 * @param finallyCallback
 */
Http.makeRequest = function (method, url, data, callback, finallyCallback) {
  return $.ajax(url, {
    method,
    data,
    beforeSend: Http.before
  })
    .done((data, textStatus, jqXHR) => Http.resolve(data, textStatus, jqXHR, callback))
    .fail(Http.reject)
    .always(finallyCallback)
}

/**
 * Regarde si Nina a envoyé une réponse JSON qui contient
 * le couple clé/valeur success/true.
 * Si oui, callback est exécutée, sinon finallyCallback est exécutée.
 * @param {Object}   data
 * @param {String}   textStatus
 * @param {Object}   jqXHR
 * @param {Function} callback
 */
Http.resolve = function (data, textStatus, jqXHR, callback) {
  callback(data)
}

/**
 * Est exécutée lors d'une erreur Http.
 * @param {jqXHR} jqXHR
 * @param textStatus
 * @param errorThrown
 */
Http.reject = function (jqXHR, textStatus, errorThrown) {
  const response = jqXHR.responseJSON
  const vm = new Vue()

  Http.after()

  vm.$toasted.error(response.error.message, {
    duration: 5000,
    position: 'bottom-right'
  })

  console.group(`Une erreur s'est produite. Code d'erreur :`)
  console.log(Base64.encode(JSON.stringify({response, textStatus, errorThrown})))
  console.groupEnd()

  throw new Error(response.error.message)
}

/**
 * Exécutée avant chaque requête AJAX
 */
Http.before = function () {
  store.commit(Mutation.HTTP_OPERATION_IS_PENDING)
  store.commit(Mutation.MOUSE_CURSOR, 'progress')
}

/**
 * Exécutée après chaque requête AJAX
 */
Http.after = function () {
  store.commit(Mutation.RESET_MOUSE_CURSOR)
  store.commit(Mutation.HTTP_OPERATION_IS_FINISHED)
}

Http.Objects = Objects
Http.Library = Library
Http.Mediatheque = Mediatheque

export default Http
