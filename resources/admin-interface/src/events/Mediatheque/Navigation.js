/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * @exports Events/Mediatheque/Navigation
 */
export default {
  /**
   * GO_TO
   */
  GO_TO: 'MEDIATHEQUE-NAVIGATION-GO_TO',
  /**
   * GO_TO_PREVIOUS
   */
  GO_TO_PREVIOUS: 'MEDIATHEQUE-NAVIGATION-GO_TO_PREVIOUS',
  /**
   * GO_TO_NEXT
   */
  GO_TO_NEXT: 'MEDIATHEQUE-NAVIGATION-GO_TO_NEXT',
  /**
   * GO_TO_PARENT
   */
  GO_TO_PARENT: 'MEDIATHEQUE-NAVIGATION-GO_TO_PARENT',
  /**
   * GO_TO_ROOT
   */
  GO_TO_ROOT: 'MEDIATHEQUE-NAVIGATION-GO_TO_ROOT',
  /**
   * REFRESH
   */
  REFRESH: 'MEDIATHEQUE-NAVIGATION-REFRESH'
}
