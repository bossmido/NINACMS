/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * @exports Events/Mediatheque/Media
 */
export default {
  /**
   * RENAME
   */
  RENAME: 'MEDIATHEQUE-MEDIA-RENAME',
  /**
   * DELETE
   */
  DELETE: 'MEDIATHEQUE-MEDIA-DELETE',
  /**
   * COPY_IN_CLIPBOARD
   */
  COPY_IN_CLIPBOARD: 'MEDIATHEQUE-MEDIA-COPY_IN_CLIPBOARD',
  /**
   * COPY
   */
  COPY: 'MEDIATHEQUE-MEDIA-COPY',
  /**
   * CUT_IN_CLIPBOARD
   */
  CUT_IN_CLIPBOARD: 'MEDIATHEQUE-CUT_MEDIA_IN_CLIPBOARD',
  /**
   * MOVE
   */
  MOVE: 'MEDIATHEQUE-MEDIA-MOVE'
}
