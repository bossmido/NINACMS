/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * @exports Events/Mediatheque/MediasContainer
 */
export default {
  /**
   * CREATE
   */
  CREATE: 'MEDIATHEQUE-MEDIAS-CONTAINER-CREATE',
  /**
   * RENAME
   */
  RENAME: 'MEDIATHEQUE-MEDIAS-CONTAINER-RENAME',
  /**
   * DELETE
   */
  DELETE: 'MEDIATHEQUE-MEDIAS-CONTAINER-DELETE',
  /**
   * COPY_IN_CLIPBOARD
   */
  COPY_IN_CLIPBOARD: 'MEDIATHEQUE-MEDIAS-CONTAINER-COPY_IN_CLIPBOARD',
  /**
   * COPY
   */
  COPY: 'MEDIATHEQUE-MEDIAS-CONTAINER-COPY',
  /**
   * CUT_IN_CLIPBOARD
   */
  CUT_IN_CLIPBOARD: 'MEDIATHEQUE-MEDIAS-CONTAINER-CUT_IN_CLIPBOARD',
  /**
   * MOVE
   */
  MOVE: 'MEDIATHEQUE-MEDIAS-CONTAINER-MOVE'
}
