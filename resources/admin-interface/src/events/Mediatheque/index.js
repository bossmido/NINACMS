/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

/**
 * @exports Events/Mediatheque
 */
export default {
  /**
   * @see module:Events/Mediatheque/Dialogs
   */
  Dialogs: require('./Dialogs').default,
  /**
   * @see module:Events/Mediatheque/Media
   */
  Media: require('./Media').default,
  /**
   * @see module:Events/Mediatheque/MediasContainer
   */
  MediasContainer: require('./MediasContainer').default,
  /**
   * @see module:Events/Mediatheque/Navigation
   */
  Navigation: require('./Navigation').default
}
