/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

import Vue from 'vue'

import Bus from '../bus'
import Events from '../events'
import Http from '../NinaHttp'

import * as Helper from './../Helpers'
import * as Mutation from '../store/mutation-types'

import ModalComponent from './Modal.vue'

const Modal = Vue.extend(ModalComponent)

class ManagerOnUpdate {
  /**
   * @param {Object} componentManager - Instance de BlocManager ou ObjectManager
   * @param {HTMLElement} $component
   * @param {ManagerOnUpdate~requestCallback} callback
   */
  constructor (componentManager, $component, callback = this.afterSaveFieldsValues) {
    this.componentManager = componentManager
    this.$component = $component
    this.componentId = Helper.extractObjectId(this.$component)
    this.componentAttrId = this.$component.getAttribute('id')
    this.callback = callback
    this.init()
  }

  init () {
    Http.Objects.getFormEdition(this.componentId, json => {
      if (json.error) {
        return
      }

      /**
       * Le formulaire d'édition
       * @type {jQuery}
       */
      this.$$form = null

      this.modal = this.createModal(json.html)
    })
  }

  /**
   * Crée la fenêtre modale.
   * @param {String} body
   */
  createModal (body) {
    const that = this

    const modal = new Modal({
      data: {
        body,
        zIndex: 70,
        title: `Edition du composant « ${this.componentAttrId} »`,
        buttons: {
          'Annuler et fermer': () => modal.close(),
          'Modifier': () => {
            /**
             * TODO:
             * Cliquer sur le bouton submit permet de forcer la vérification HTML5 du formulaire.
             * Cependant, une erreur peut se produire lorsqu'un champ qui n'est pas valide, se trouve dans
             * un onglet qui est caché (ex: An invalid form control with name='test_link' is not focusable.).
             *
             * Pour cela, il faudrait implémenter un système de validation plus poussé que celui d'HTML5.
             *
             * TODO: Implémenter une vérification côté serveur
             */
            that.$$form.find('[name="updateObject"]').click()
          }
        },
        onClose () {
          OnUpdateTinyMce.remove()
          OnUpdateVueJs.remove()
          modal.$destroy()

          that.componentManager.$store.commit(Mutation.MEDIATHEQUE_EXPLORER_CANCEL_MEDIAS_SELECTIONS)
          that.componentManager.$store.commit(Mutation.MEDIATHEQUE_EXPLORER_REMOVE_FILTERS)
          that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_COMPONENT, null)
          that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_COMPONENT_FORM, null)
          that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_TAB, null)
        },
        afterMount () {
          that.$$form = $(this.$el).find('#NinaEditionForm')

          that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_COMPONENT, that.$component)
          that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_COMPONENT_FORM, that.$$form.get(0))

          function handleTabActivation (tab, panel) {
            that.componentManager.$store.commit(Mutation.OBJECT_MANAGER_SET_CURRENT_TAB, tab.attr('data-nina-tab'))
            const $$file = panel.find(':file:first')

            if ($$file.length === 1) {
              const fieldId = $$file.attr('id')

              Bus.$emit(Events.ASK_FOR_ENABLE_MEDIAS_SELECTION, fieldId)
              Bus.$emit(Events.ASK_FOR_MEDIATHEQUES_FILTERS, fieldId)
              Bus.$emit(Events.FIELD_WILL_BE_VISIBLE, fieldId, that)
              that.componentManager.$store.commit(Mutation.MEDIATHEQUE_EXPLORER_SET_CURRENT_FIELD_FOR_MEDIAS_SELECTION, fieldId)
            } else {
              that.componentManager.$store.commit(Mutation.MEDIATHEQUE_EXPLORER_CANCEL_MEDIAS_SELECTIONS)
              that.componentManager.$store.commit(Mutation.MEDIATHEQUE_EXPLORER_REMOVE_FILTERS)
            }
          }

          // Création des tabs
          that.$$form.find('#tabs').tabs({
            create (e, ui) {
              handleTabActivation(ui.tab, ui.panel)
            },
            activate (e, ui) {
              handleTabActivation(ui.newTab, ui.newPanel)
            }
          })

          // Gestion des plugins
          OnUpdateTinyMce.init(that)
          OnUpdateVueJs.init(that)

          // Gestion des modales
          that.handleDialogs()

          that.$$form.on('submit', e => {
            return that.onSubmit(e)
          })
        }
      }
    }).$mount()

    return modal
  }

  /**
   * Exécutée dès l'envoie du formulaire d'édition
   * @param {Event} e
   */
  onSubmit (e) {
    e.preventDefault()

    this.saveComponent(this.$$form.serialize())
  }

  /**
   * Sauvegarde les fieldsValues auprès du serveur.
   * @param serializedForm
   */
  saveComponent (serializedForm) {
    Http.Objects.saveFieldsValues(serializedForm, (json) => {
      this.callback(this, json)
    })
  }

  /**
   * @callback ManagerOnUpdate~requestCallback
   * @param {ManagerOnUpdate} managerOnUpdate
   * @param {Object} json
   */
  afterSaveFieldsValues (managerOnUpdate, json) {
    if (json.error) {
      return
    }

    this.$component.outerHTML = json.html
    this.componentManager.$toasted.success('Le composant a bien été modifié !')
    Bus.$emit(Events.OBJECTS_ARE_DIRTY)

    this.modal.close()
  }

  /**
   *
   */
  handleDialogs () {
    const that = this

    this.$$form.find('[data-open-next-element-in-dialog]').each(function () {
      const $$this = $(this)
      const $$dialog = $$this.next()
      const $$parent = $$this.parents('.form-group')

      // Évite de relancer la construction d'une dialog, .dialog('instance') ne fonctionne pas bizarrement
      if ($$dialog.hasClass('ui-dialog')) {
        return
      }

      $$dialog.dialog({
        appendTo: $$parent, // pour rester dans le formulaire, et ainsi sauvegarder les propriétés
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: false,
        title: 'Édition des propriétés',
        position: {my: 'center top', at: 'center top', of: that.$$form}
      })

      $$this.on('click', function (e) {
        e.preventDefault()
        $$dialog.dialog('open')
      })
    })
  }
}

const OnUpdateTinyMce = {
  /**
   * Initialise FieldRichTextTinyMce du formulaire passé en paramètre.
   * @param {ManagerOnUpdate} that
   */
  init (that) {
    that.$$form.find('[data-nina-tinymce]').each(function () {
      const $this = $(this)

      tinymce.init({
        selector: '#' + $this.attr('id'),
        theme: 'modern',
        schema: 'html5',
        plugins: 'link',
        language: 'fr_FR',
        toolbar: 'undo redo | styleselect | bold italic underline | alignleft aligncenter alignright | link',
        //                    statusbar: false,
        menubar: false,
        setup: function (editor) {
          editor.on('change', editor.save)
        }
      })
    })
  },

  /**
   * Supprime les instances TinyMCE contenu dans la page.
   * (Sinon le initTinyMceOnForm() ne fonctionnera plus)
   */
  remove () {
    tinymce.editors
      .filter(editor => /^form_/.test(editor.id))
      .forEach(editor => editor.remove())
  }
}

const OnUpdateVueJs = {
  instances: [],

  /**
   * @param {ManagerOnUpdate} that
   */
  init (that) {
    that.$$form.find('[data-nina-run-vuejs]').each((_, $el) => {
      const vm = new Vue({
        el: $el,
        store: that.componentManager.$store,
        toasted: that.componentManager.$toasted
      })

      this.instances.push(vm)
    })
  },

  remove () {
    this.instances.forEach(instance => {
      instance.$destroy()
    })
  }
}

export default ManagerOnUpdate
