/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

const notAllowedChars = ['/', '\\', '<', '>', '"', '|', '*', '?', ':']

/**
 * Valide un nom de fichier.
 * @param {String} fileName
 * @param {Array.<Object>} existingFiles
 * @param {Object.<String, String>} errorsMessages
 * @return {{valid: boolean, errorMessage: string}}
 */
export default function validateFileName (fileName, existingFiles, errorsMessages) {
  'use strict'

  let valid = true
  let errorMessage = ''

  // Si le nom est vide
  if (fileName.length === 0) {
    valid = false
    errorMessage = errorsMessages['isEmpty']
  }

  // Si le nom est uniquement composé d'espaces, ou de points
  if (/^\s+$/.test(fileName) || /^[.]+$/.test(fileName)) {
    valid = false
    errorMessage = errorsMessages['isOnlyComposedWithSpacesOrDots']
  }

  // Si le nom contient un caractère non autorisé
  valid && notAllowedChars.forEach(c => {
    if (fileName.includes(c)) {
      valid = false
      errorMessage = errorsMessages['isContainingNotAllowedChars'].replace('{char}', c)
    }
  })

  // Si le nom est déjà utilisé
  valid && existingFiles.forEach(mc => {
    if (mc.label === fileName) {
      valid = false
      errorMessage = errorsMessages['isAlreadyExisting']
    }
  })

  return {
    valid,
    errorMessage
  }
}
