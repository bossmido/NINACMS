/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

import Vue from 'vue'
import App from './App.vue'

import Vuex from 'vuex'
import Toasted from 'vue-toasted'
import Popper from 'vue-popperjs'

import Cursor from './components/Cursor.vue'
import Library from './components/Library.vue'
import LibraryComponentsExplorer from './components/Library/ComponentsExplorer.vue'
import LibraryMediasExplorer from './components/Library/MediasExplorer.vue'
import LibraryCmsTools from './components/Library/CmsTools.vue'
import FieldLinks from './fields/FieldLinks.vue'
import FieldFile from './fields/FieldFile.vue'
import FieldImage from './fields/FieldImage.vue'

import store from './store'

// Configuration

Vue.use(Vuex)

Vue.use(Toasted, {
  duration: 1000,
  position: 'bottom-right'
})

Vue.component('NinaCursor', Cursor)
Vue.component('Popper', Popper)
Vue.component('Library', Library)
Vue.component('LibraryComponentsExplorer', LibraryComponentsExplorer)
Vue.component('LibraryMediasExplorer', LibraryMediasExplorer)
Vue.component('LibraryCmsTools', LibraryCmsTools)
Vue.component('FieldLinks', FieldLinks)
Vue.component('FieldFile', FieldFile)
Vue.component('FieldImage', FieldImage)

// jQuery

global.$ = global.jQuery = require('jquery')
require('jquery-ui/themes/base/all.css')
require('jquery-ui/ui/widgets/sortable')
require('jquery-ui/ui/widgets/tabs')
require('jquery-ui/ui/widgets/dialog')
require('jquery-ui/ui/widgets/draggable')
require('jquery-contextmenu')

require('blueimp-file-upload')
require('cropper')

// Misc
require('babel-polyfill')
require('../../../public/assets/vendor/bootstrap/js/bootstrap')

Array.prototype.unique = Array.prototype.unique || function () {
  return this.reduce((acc, x) => acc.includes(x) ? acc : acc.concat(x), [])
}

// Go

new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
