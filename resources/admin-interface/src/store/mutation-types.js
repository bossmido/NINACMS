/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

// Global
export const OPTIONS = 'OPTIONS'
export const COMPONENTS_METADATA = 'COMPONENTS_METADATA'
export const COMPONENTS_GROUPS_TREE = 'COMPONENTS_GROUPS_TREE'

export const COPY_COMPONENT = 'COPY_COMPONENT'
export const RESET_COMPONENT_TO_COPY = 'RESET_COMPONENT_TO_COPY'

export const MOUSE_CURSOR = 'MOUSE_CURSOR'
export const RESET_MOUSE_CURSOR = 'RESET_MOUSE_CURSOR'

export const SHOW_GRIDS = 'SHOW_GRIDS'
export const HIDE_GRIDS = 'HIDE_GRIDS'

export const HTTP_OPERATION_IS_PENDING = 'HTTP_OPERATION_IS_PENDING'
export const HTTP_OPERATION_IS_FINISHED = 'HTTP_OPERATION_IS_FINISHED'

export const WAITING_USER_TO_INSERT_A_COMPONENT = 'WAITING_USER_TO_INSERT_A_COMPONENT'
export const USER_HAS_INSERTED_A_COMPONENT = 'USER_HAS_INSERTED_A_COMPONENT'
export const USER_HAS_NOT_INSERTED_A_COMPONENT = 'USER_HAS_NOT_INSERTED_A_COMPONENT'

export const USER_HAS_STARTED_TO_DRAG_COMPONENT = 'USER_HAS_STARTED_TO_DRAG_COMPONENT'
export const USER_HAS_STOPPED_TO_DRAG_COMPONENT = 'USER_HAS_STOPPED_TO_DRAG_COMPONENT'

// ObjectManager
export const OBJECT_MANAGER_SET_CURRENT_COMPONENT_FORM = 'OBJECT_MANAGER_SET_CURRENT_COMPONENT_FORM'
export const OBJECT_MANAGER_SET_CURRENT_COMPONENT = 'OBJECT_MANAGER_SET_CURRENT_COMPONENT'
export const OBJECT_MANAGER_SET_CURRENT_TAB = 'OBJECT_MANAGER_SET_CURRENT_TAB'

// Library
export const LIBRARY_ID = 'LIBRARY_ID'
export const LIBRARY_COMPONENTS = 'SET_LIBRARY_COMPONENTS'

// Explorateur de composants
export const COMPONENTS_EXPLORER_SET_CURRENT_GROUP = 'COMPONENTS_EXPLORER_SET_CURRENT_GROUP'
export const COMPONENTS_EXPLORER_SET_FILTERS = 'COMPONENTS_EXPLORER_SET_FILTERS'
export const COMPONENTS_EXPLORER_PUSH_FILTER = 'COMPONENTS_EXPLORER_PUSH_FILTER'
export const COMPONENTS_EXPLORER_REMOVE_FILTERS = 'COMPONENTS_EXPLORER_REMOVE_FILTERS'

// Mediathèques
export const MEDIATHEQUE_INIT = 'MEDIATHEQUE_INIT'
export const MEDIATHEQUE_SET_CURRENT = 'MEDIATHEQUE_SET_CURRENT'

// Médiatheque (explorer)
export const MEDIATHEQUE_EXPLORER_CLEAR_FILES = 'MEDIATHEQUE_EXPLORER_CLEAR_FILES'
export const MEDIATHEQUE_EXPLORER_SET_FILES = 'MEDIATHEQUE_EXPLORER_SET_FILES'

export const MEDIATHEQUE_EXPLORER_SET_CURRENT_MEDIAS_CONTAINER = 'MEDIATHEQUE_EXPLORER_SET_CURRENT_MEDIAS_CONTAINER'
// export const MEDIATHEQUE_EXPLORER_PUSH_MEDIAS_CONTAINER = 'MEDIATHEQUE_EXPLORER_PUSH_MEDIAS_CONTAINER';
// export const MEDIATHEQUE_EXPLORER_PUSH_MEDIA = 'MEDIATHEQUE_EXPLORER_PUSH_MEDIA';
export const MEDIATHEQUE_EXPLORER_PUSH_MEDIA_TO_SELECTION = 'MEDIATHEQUE_EXPLORER_PUSH_MEDIA_TO_SELECTION'
// export const MEDIATHEQUE_EXPLORER_REMOVE_MEDIAS_CONTAINER = 'MEDIATHEQUE_EXPLORER_REMOVE_MEDIAS_CONTAINER';
// export const MEDIATHEQUE_EXPLORER_REMOVE_MEDIA = 'MEDIATHEQUE_EXPLORER_REMOVE_MEDIA';
export const MEDIATHEQUE_EXPLORER_REMOVE_MEDIA_FROM_SELECTION = 'MEDIATHEQUE_EXPLORER_REMOVE_MEDIA_FROM_SELECTION'

export const MEDIATHEQUE_EXPLORER_SET_FILTERS = 'MEDIATHEQUE_EXPLORER_SET_FILTERS'
export const MEDIATHEQUE_EXPLORER_REMOVE_FILTERS = 'MEDIATHEQUE_EXPLORER_REMOVE_FILTERS'
export const MEDIATHEQUE_EXPLORER_FILTER_MEDIAS = 'MEDIATHEQUE_EXPLORER_FILTER_MEDIAS'

export const MEDIATHEQUE_EXPLORER_UPDATE_CURRENT_ID = 'MEDIATHEQUE_EXPLORER_UPDATE_CURRENT_ID'
export const MEDIATHEQUE_EXPLORER_PUSH_PREV_ID = 'MEDIATHEQUE_EXPLORER_PUSH_PREV_ID'
export const MEDIATHEQUE_EXPLORER_PUSH_NEXT_ID = 'MEDIATHEQUE_EXPLORER_PUSH_NEXT_ID'
export const MEDIATHEQUE_EXPLORER_REMOVE_LAST_PREV_ID = 'MEDIATHEQUE_EXPLORER_REMOVE_LAST_PREV_ID'
export const MEDIATHEQUE_EXPLORER_REMOVE_LAST_NEXT_ID = 'MEDIATHEQUE_EXPLORER_REMOVE_LAST_NEXT_ID'

export const MEDIATHEQUE_EXPLORER_PREPARE_MEDIAS_SELECTION = 'MEDIATHEQUE_EXPLORER_PREPARE_MEDIAS_SELECTION'
export const MEDIATHEQUE_EXPLORER_SET_CURRENT_FIELD_FOR_MEDIAS_SELECTION = 'MEDIATHEQUE_EXPLORER_SET_CURRENT_FIELD_FOR_MEDIAS_SELECTION'
export const MEDIATHEQUE_EXPLORER_CANCEL_MEDIAS_SELECTIONS = 'MEDIATHEQUE_EXPLORER_CANCEL_MEDIAS_SELECTIONS'

// Médiathèque (clipboard)
export const MEDIATHEQUE_CLIPBOARD_FILL = 'MEDIATHEQUE_CLIPBOARD_FILL'
export const MEDIATHEQUE_CLIPBOARD_RESET = 'MEDIATHEQUE_CLIPBOARD_RESET'
