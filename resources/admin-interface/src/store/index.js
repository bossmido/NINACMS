/*
 * This file is part of NinaCMS.
 *
 * Copyright (c) 2017.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 */

import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'

Vue.use(Vuex)

const state = {
  'options': {},
  'object_manager': {
    'current_component_form': null,
    'current_component': null,
    'current_tab': null
  },
  'library': {
    'id': null,
    'components': [],
    'componentsFilters': [
      // Exemple de filtre
      // (components) => components.filter(component => component.class.startsWith('Article'))
    ],
    'currentComponentGroup': {}
  },
  'mediatheques': {},
  'currentMediatheque': null,
  'componentsMetadata': {},
  'componentsGroupsTree': [],
  'componentToCopy': null,
  'gridsAreVisible': true,
  'cursor': 'auto',
  'httpOperationIsPending': false,
  'waitingUserToInsertAComponent': false,
  'userIsDraggingAComponent': false
}

const getters = {
  'options': (state) => state.options,
  'object_manager': (state) => state.object_manager,
  'library': (state) => state.library,
  'currentComponentGroup': (state) => state.library.currentComponentGroup,
  'mediatheques': (state) => state.mediatheques,
  'mediatheque': (state) => state.mediatheques[state.currentMediatheque],
  'componentsMetadata': (state) => state.componentsMetadata,
  'componentsGroupsTree': (state) => state.componentsGroupsTree,
  'componentToCopy': (state) => state.componentToCopy,
  'gridsAreVisible': (state) => state.gridsAreVisible,
  'cursor': (state) => state.cursor,
  'httpOperationIsPending': (state) => state.httpOperationIsPending,
  'waitingUserToInsertAComponent': (state) => state.waitingUserToInsertAComponent,
  'userIsDraggingAComponent': (state) => state.userIsDraggingAComponent
}

export default new Vuex.Store({
  state,
  getters,
  mutations: {
    [types.OPTIONS] (state, options) {
      state.options = options
    },

    [types.COMPONENTS_METADATA] (state, componentsMetadata) {
      state.componentsMetadata = componentsMetadata
    },

    [types.COMPONENTS_GROUPS_TREE] (state, componentsGroupsTree) {
      state.componentsGroupsTree = componentsGroupsTree
    },

    [types.COPY_COMPONENT] (state, payload) {
      state.componentToCopy = payload
    },

    [types.RESET_COMPONENT_TO_COPY] (state) {
      state.componentToCopy = null
    },

    [types.MOUSE_CURSOR] (state, cursor) {
      state.cursor = cursor
    },

    [types.RESET_MOUSE_CURSOR] (state) {
      state.cursor = 'auto'
    },

    [types.SHOW_GRIDS] (state) {
      state.gridsAreVisible = true
    },

    [types.HIDE_GRIDS] (state) {
      state.gridsAreVisible = false
    },

    [types.HTTP_OPERATION_IS_PENDING] (state) {
      state.httpOperationIsPending = true
    },

    [types.HTTP_OPERATION_IS_FINISHED] (state) {
      state.httpOperationIsPending = false
    },

    [types.WAITING_USER_TO_INSERT_A_COMPONENT] (state) {
      state.waitingUserToInsertAComponent = true
    },

    [types.USER_HAS_INSERTED_A_COMPONENT] (state) {
      state.waitingUserToInsertAComponent = false
    },

    [types.USER_HAS_NOT_INSERTED_A_COMPONENT] (state) {
      state.waitingUserToInsertAComponent = false
    },

    [types.USER_HAS_STARTED_TO_DRAG_COMPONENT] (state) {
      state.userIsDraggingAComponent = true
    },

    [types.USER_HAS_STOPPED_TO_DRAG_COMPONENT] (state) {
      state.userIsDraggingAComponent = false
    },

    /* -------------
     * ObjectManager
     * ------------- */
    [types.OBJECT_MANAGER_SET_CURRENT_COMPONENT_FORM] (state, $$form) {
      Vue.set(state.object_manager, 'current_component_form', $$form)
    },

    [types.OBJECT_MANAGER_SET_CURRENT_COMPONENT] (state, component) {
      Vue.set(state.object_manager, 'current_component', component)
    },

    [types.OBJECT_MANAGER_SET_CURRENT_TAB] (state, tab) {
      Vue.set(state.object_manager, 'current_tab', tab)
    },

    /* -------
     * Library
     * ------- */
    [types.LIBRARY_ID] (state, id) {
      Vue.set(state.library, 'id', id)
    },

    [types.LIBRARY_COMPONENTS] (state, components) {
      Vue.set(state.library, 'components', components)
    },

    /* -------------------------
     * Explorateur de composants
     * ------------------------- */
    [types.COMPONENTS_EXPLORER_SET_FILTERS] (state, filters) {
      Vue.set(state.library, 'componentsFilters', filters)
    },

    [types.COMPONENTS_EXPLORER_PUSH_FILTER] (state, filter) {
      state.library.componentsFilters.push(filter)
    },

    [types.COMPONENTS_EXPLORER_REMOVE_FILTERS] (state) {
      Vue.set(state.library, 'componentsFilters', [])
    },

    [types.COMPONENTS_EXPLORER_SET_CURRENT_GROUP] (state, group) {
      Vue.set(state.library, 'currentComponentGroup', group)
    },

    /* -----------------------------------
     * Médiatheque
     * ----------------------------------- */
    [types.MEDIATHEQUE_INIT] (state, mediatheque) {
      Vue.set(state.mediatheques, mediatheque.name, mediatheque)
    },

    [types.MEDIATHEQUE_SET_CURRENT] (state, mediatheque) {
      Vue.set(state, 'currentMediatheque', mediatheque)
    },

    /* -----------------------------------
     * Médiatheque - explorateur de médias
     * ----------------------------------- */
    [types.MEDIATHEQUE_EXPLORER_SET_FILTERS] (state, filters) {
      Object.keys(state.mediatheques).forEach(mediathequeName => {
        state.mediatheques[mediathequeName].explorer.filters = filters
      })
    },

    [types.MEDIATHEQUE_EXPLORER_REMOVE_FILTERS] (state) {
      Object.keys(state.mediatheques).forEach(mediathequeName => {
        state.mediatheques[mediathequeName].explorer.filters = []
      })
    },

    [types.MEDIATHEQUE_EXPLORER_CLEAR_FILES] (state) {
      const mediatheque = getters.mediatheque(state)

      Object.assign(mediatheque.explorer.files, {
        medias: [],
        medias_containers: [],
        original_medias: [],
        original_medias_containers: []
      })
    },

    [types.MEDIATHEQUE_EXPLORER_SET_FILES] (state, payload) {
      const mediatheque = getters.mediatheque(state)

      Object.assign(mediatheque.explorer.files, {
        medias: [...payload.medias],
        medias_containers: [...payload.medias_containers],
        original_medias: [...payload.medias],
        original_medias_containers: [...payload.medias_containers]
      })
    },

    [types.MEDIATHEQUE_EXPLORER_SET_CURRENT_MEDIAS_CONTAINER] (state, currentMediasContainer) {
      const mediatheque = getters.mediatheque(state)

      Vue.set(mediatheque.explorer, 'current_medias_container', currentMediasContainer)
    },

    [types.MEDIATHEQUE_EXPLORER_FILTER_MEDIAS] (state) {
      const mediatheque = getters.mediatheque(state)
      const files = mediatheque.explorer.files

      // faire un [].reduce ?
      let medias = files.original_medias

      mediatheque.explorer.filters.forEach(filter => {
        medias = medias.filter(filter)
      })

      files.medias = medias
    },

    [types.MEDIATHEQUE_EXPLORER_PUSH_MEDIA_TO_SELECTION] (state, media) {
      const mediatheque = getters.mediatheque(state)

      if (mediatheque.explorer.medias_selection !== null) {
        const selectedMediaIndex = mediatheque.explorer.medias_selection.medias.findIndex(m => m.id === media.id)

        console.log({
          media,
          selectedMediaIndex
        })

        if (selectedMediaIndex === -1) {
          mediatheque.explorer.medias_selection.medias.push(media)
        }
      }
    },

    [types.MEDIATHEQUE_EXPLORER_REMOVE_MEDIA_FROM_SELECTION] (state, mediaId) {
      const mediatheque = getters.mediatheque(state)

      if (mediatheque.explorer.medias_selection !== null) {
        const selectedMediaIndex = mediatheque.explorer.medias_selection.medias.findIndex(m => m.id === mediaId)

        if (selectedMediaIndex > -1) {
          mediatheque.explorer.medias_selection.medias.splice(selectedMediaIndex, 1)
        }
      }
    },

    [types.MEDIATHEQUE_EXPLORER_UPDATE_CURRENT_ID] (state, id) {
      const mediatheque = getters.mediatheque(state)
      mediatheque.explorer.current_id = id
    },

    [types.MEDIATHEQUE_EXPLORER_PUSH_PREV_ID] (state, id) {
      const mediatheque = getters.mediatheque(state)
      mediatheque.explorer.prev_ids.push(id)
    },

    [types.MEDIATHEQUE_EXPLORER_PUSH_NEXT_ID] (state, id) {
      const mediatheque = getters.mediatheque(state)
      mediatheque.explorer.next_ids.push(id)
    },

    [types.MEDIATHEQUE_EXPLORER_REMOVE_LAST_PREV_ID] (state) {
      const mediatheque = getters.mediatheque(state)

      mediatheque.explorer.prev_ids.pop()
    },

    [types.MEDIATHEQUE_EXPLORER_REMOVE_LAST_NEXT_ID] (state) {
      const mediatheque = getters.mediatheque(state)

      mediatheque.explorer.next_ids.pop()
    },

    [types.MEDIATHEQUE_CLIPBOARD_FILL] (state, {scope, action, file, fileType}) {
      const clipboard = state.mediatheques[scope].clipboard
      clipboard.empty = false
      clipboard.action = action
      clipboard.file = Object.assign({}, file)
      clipboard.file_type = fileType
    },

    [types.MEDIATHEQUE_CLIPBOARD_RESET] (state, {scope}) {
      const clipboard = state.mediatheques[scope].clipboard
      clipboard.empty = true
      clipboard.action = null
      clipboard.file = {}
      clipboard.file_type = null
    },

    [types.MEDIATHEQUE_EXPLORER_PREPARE_MEDIAS_SELECTION] (state, {fieldId, medias, infinite, max}) {
      Object.keys(state.mediatheques).forEach(name => {
        const mediatheque = state.mediatheques[name]

        Vue.set(mediatheque.explorer.medias_selections, fieldId, {
          medias,
          infinite,
          max
        })
      })
    },

    [types.MEDIATHEQUE_EXPLORER_SET_CURRENT_FIELD_FOR_MEDIAS_SELECTION] (state, fieldId) {
      Object.keys(state.mediatheques).forEach(name => {
        const mediatheque = state.mediatheques[name]

        mediatheque.explorer.medias_selection = mediatheque.explorer.medias_selections[fieldId]
      })
    },

    [types.MEDIATHEQUE_EXPLORER_CANCEL_MEDIAS_SELECTIONS] (state) {
      Object.keys(state.mediatheques).forEach(name => {
        const mediatheque = state.mediatheques[name]

        mediatheque.explorer.medias_selection = null
        mediatheque.explorer.medias_selections = {}
      })
    }
  }
})
