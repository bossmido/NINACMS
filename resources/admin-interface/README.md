# API de l'Interface d'Administration de NinaCMS

## Pré-requis

- [Node.js](https://nodejs.org/) >= 6.11
- npm >= 3.10 ou [yarn](https://yarnpkg.com/lang/en/) >= 0.19 

(yarn est un gestionnaire de paquets comme npm, mais il a l'avantage d'être plus rapide pour l'installation des paquets.)

## Installation

Si le script `deploy.sh` n'a pas encore été utilisé, il est nécessaire d'installer les dépendances Node.js du projet :

```bash
npm install
# ou
yarn
```

## Construction de l'interface

### Pour la production

Optimise et minifie le code JavaScript et CSS généré :

```bash
npm run build
# ou
yarn run build
```

### Pendant le développement

Aucune minification de code, mais les messages d'erreurs JavaScript sont plus simples à comprendre : 

```bash
npm run build:dev
# ou
yarn run build:dev
```

Observe les changements effectués dans le dossier `src` et lance à chaque fois la commande `npm run build:dev` :

```bash
npm run watch
# ou
yarn run watch
```

## Configuration

Le projet utilise webpack. Dans le cadre de l'application, webpack permet :

- De modulariser chaque ressource, telle que le JavaScript, le CSS, les images...
- De séparer l'application dans 4 fichiers principaux :`manifest.js`, `vendor.js`, `main.js` et `style.css`,
- D'extraire tout le CSS dans le fichier `style.css`, 
- D'extraire les librairies JavaScript tierces (jQuery, jQueryUI, Vue, etc...) dans le fichier `vendor.js`, 
- D'utiliser les fichiers `.vue`, des composant VueJS qui permettent l'écriture de code HTML/JS/CSS dans un seul fichier,
- D'utiliser des dépendances Node.js et qui seront elles aussi empaquetées avec l'application,
- D'utiliser la version ES6 de JavaScript, qui sera ensuite transpilée en ES5 grâce à Babel pour
- D'utiliser les langages SCSS, Sass, ...
- De minifier ou non le code JavaScript et CSS généré, 
- etc ...

Pour sa configuration, consultez la [documentation de webpack](https://webpack.js.org/configuration/).
