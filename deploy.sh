#!/usr/bin/env bash

set -eu

PROGRAM=$(basename "$0")
ROOT=$(pwd)

PHP=$(which php)
COMPOSER="php composer.phar"
NPM=$(which npm)

display_help () {
    echo -e "
$PROGRAM -- Script de déploiement pour le projet NinaCMS
Prototype:
    $PROGRAM [OPTIONS]
OPTIONS:
    --help, -h            \tAffiche l'aide
    --all                 \tLance --php --admin --components-assets --composer-cache --spec

    --clean               \tClean le projet
    --php                 \tDéploie le côté PHP du projet
    --admin               \tDéploie l'administration VueJS du projet
    --assets              \tGénère les assets
    --composer-cache      \tGénère le cache de Composer
    --cache               \tNettoie le dossier cache
    --docs                \tGénère la documentation d'API de Nina
    --fix-permissions     \tFixe les permissions des dossiers cache, public/upload et public/mediatheque,
    --spec                \tLance les specs
"
    exit
}

die () {
    echo "$@" >&2
    exit 1
}

[[ $# -eq 0 ]] && display_help
[[ -x ${PHP} ]] || die "PHP n'est pas installé."
[[ -f "composer.phar" ]] || die "Composer n'est pas installé."
[[ -x ${NPM} ]] || die "Npm n'est pas installé."

run () {
    echo -e "\e[1m=> Exécution de «" $@ "» \e[0m"
    $@
}

step_clean () {
    run cd "${ROOT}"
    run rm -fr \
        vendor/ \
        node_modules/ \
        public/assets/components \
        public/assets/admin-interface \
        resources/admin-interface/node_modules
}

step_php () {
    run cd "${ROOT}"
    run ${COMPOSER} install
    run "vendor/bin/phinx migrate -c config/phinx.php"
    run "vendor/bin/phinx seed:run -c config/phinx.php"
}

step_admin () {
    run cd "${ROOT}/resources/admin-interface"
    run ${NPM} install
    run ${NPM} run build
}

step_assets () {
    run cd "${ROOT}"
    run ${NPM} install
    run rm -fr public/assets/components
    run npm run assets
    run npm run components
}

step_composer_cache () {
    run cd "${ROOT}"
    run ${COMPOSER} dump-autoload -o
}

step_cache () {
    run cd "${ROOT}"
    run rm -fr cache
    run mkdir cache
    step_fix_permissions
}

step_docs () {
  run cd "${ROOT}"
  run composer run docs-api
  run npm run docs-api
  run npm run docs:install
  run npm run docs
}

step_fix_permissions () {
    # http://symfony.com/doc/current/setup/file_permissions.html#using-acl-on-a-system-that-supports-setfacl-linux-bsd
    HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

    run sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX cache
    run sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX cache

    run sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX public/mediatheque
    run sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX public/mediatheque
}

step_spec () {
    run cd "${ROOT}"
    run ./vendor/bin/kahlan -ff --reporter=verbose
}

step_run_all () {
    step_php
    step_assets
    step_admin
    step_composer_cache
    step_docs
    step_spec
}

for ARG in "$@"
do
    case "${ARG}" in
        "--help"|"-h") display_help ;;
        "--all") step_run_all ;;
        "--clean") step_clean ;;
        "--php") step_php ;;
        "--admin") step_admin ;;
        "--assets") step_assets ;;
        "--composer-cache") step_composer_cache ;;
        "--docs") step_docs ;;
        "--cache") step_cache ;;
        "--fix-permissions") step_fix_permissions ;;
        "--spec") step_spec ;;
    esac
done
